//******************************************************************************
// file: I2C.cc
// desc: I2C Interface
// auth: 05-AUG-2004 R. Spiwoks
// modf: 06-SEP-2004 R. Spiwoks
//******************************************************************************

// to enable debugging
//#define DEBUG

#include <I2C/I2C.h>
#include "RCDUtilities/RCDUtilities.h"

#include <iostream>
#include <iomanip>
#include <sstream>

using namespace LVL1;

//------------------------------------------------------------------------------

const std::string I2C::MODULE_NAME[MODULE_NUMBER] =
  { "UNKNOWN", "CTPCORE", "CTPIN", "MICTP", "CTPCOREPLUS", "ALTI" };

//------------------------------------------------------------------------------

I2C::I2C(MODULE_TYPE mod, RCD::VMEMasterMap* vmm, const unsigned int bus, const unsigned int d) : I2CInterface(d) {

    // user externally created VMEbus master mapping
    m_vmm = vmm;

    switch (mod) {
    case CTP_CORE:
        m_base = 0x000c4460;
        break;

    case CTP_IN:
        m_base = 0x00280020;
        break;

    case MICTP:
        m_base = 0x00000600;
        break;

    case CTPCOREPLUS:
        if(bus >= BUS_NUMBER) {
            CERR("I2C::I2C: ERROR - bus number %d not in allowed range [0..%d]",bus,BUS_NUMBER-1);
            return;
        }
        m_base = 0x00200200 + bus*ADDR_CTPCOREPLUS_OFFSET;
        break;

    case ALTI:
        m_base = 0x00009080;
        break;

    default:
        CERR("wrong module type \"%d\"",(int)mod);
        return;
    }

    // address offsets
    m_i2c_prescale_low  = m_base + ADDR_PRESCALE_LOW;
    m_i2c_prescale_high = m_base + ADDR_PRESCALE_HIGH;
    m_i2c_control       = m_base + ADDR_CONTROL;
    m_i2c_transmit_data = m_base + ADDR_TRANSMIT_DATA;
    m_i2c_transmit_addr = m_base + ADDR_TRANSMIT_ADDR;
    m_i2c_receive       = m_base + ADDR_RECEIVE;
    m_i2c_command       = m_base + ADDR_COMMAND;
    m_i2c_status        = m_base + ADDR_STATUS;

    // report
    std::printf("I2C::I2C: INFO - opened I2C of type \"%s\" at offset 0x%08x\n",MODULE_NAME[mod].c_str(),m_base);
}

//------------------------------------------------------------------------------

I2C::I2C()

  : I2CInterface(0), m_base(0), m_i2c_prescale_low(0), m_i2c_prescale_high(0), m_i2c_control(0), m_i2c_transmit_data(0), m_i2c_transmit_addr(0),
    m_i2c_receive(0), m_i2c_command(0), m_i2c_status(0) {

    // nothing to be done
}

//------------------------------------------------------------------------------

I2C::~I2C() {

    // nothing to be done
}

//------------------------------------------------------------------------------

int I2C::PrescaleLow_Read(unsigned int& data) {

    return(m_vmm->ReadSafe(m_i2c_prescale_low, &data));
}

//------------------------------------------------------------------------------

int I2C::PrescaleLow_Write(unsigned int data) {

    return(m_vmm->WriteSafe(m_i2c_prescale_low, data));
}

//------------------------------------------------------------------------------

int I2C::PrescaleHigh_Read(unsigned int& data) {

    return(m_vmm->ReadSafe(m_i2c_prescale_high, &data));
}

//------------------------------------------------------------------------------

int I2C::PrescaleHigh_Write(unsigned int data) {

    return(m_vmm->WriteSafe(m_i2c_prescale_high, data));
}

//------------------------------------------------------------------------------

int I2C::Control_Read(I2C_CONTROL_BITSTRING& bitset) {

    unsigned int data;

    if((m_status = m_vmm->ReadSafe(m_i2c_control, &data)) == 0) {
        std::vector<unsigned int> outp;
        outp.push_back(data);
        bitset = (RCD::BitSet)(outp);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int I2C::Control_Write(I2C_CONTROL_BITSTRING& bitset) {

    std::vector<unsigned int> outp = bitset.number();
    unsigned int data = outp[0];

    return(m_vmm->WriteSafe(m_i2c_control, data));
}

//------------------------------------------------------------------------------

int I2C::TransmitData_Read(unsigned int& data) {

#ifdef DEBUG
    int rtnv = m_vmm->ReadSafe(m_i2c_transmit_data, &data);
    CDBG(">>>>>>>>>>>>>>>> - RECV = 0x%08x",data);
    return(rtnv);
#else
    return(m_vmm->ReadSafe(m_i2c_transmit_data, &data));
#endif
}

//------------------------------------------------------------------------------

int I2C::TransmitData_Write(unsigned int data) {

    CDBG(">>>>>>>>>>>>>>>> - DATA = 0x%08x",data);
    return(m_vmm->WriteSafe(m_i2c_transmit_data, data));
}

//------------------------------------------------------------------------------

int I2C::TransmitAddr_Read(I2C_TRANSMITADDR_BITSTRING& bitset) {

    unsigned int data;

    if((m_status = m_vmm->ReadSafe(m_i2c_transmit_addr, &data)) == 0) {
        std::vector<unsigned int> outp;
        outp.push_back(data);
        bitset = (RCD::BitSet)(outp);
    }
    return(m_status);
}

//------------------------------------------------------------------------------

int I2C::TransmitAddr_Write(I2C_TRANSMITADDR_BITSTRING& bitset) {

    std::vector<unsigned int> outp = bitset.number();
    unsigned int data = outp[0];

    CDBG(">>>>>>>>>>>>>>>> - ADDR = 0x%08x (\"%s\")",data,printAddress(data).c_str());
    return(m_vmm->WriteSafe(m_i2c_transmit_addr, data));
}

//------------------------------------------------------------------------------

int I2C::Receive_Read(unsigned int& data) {

#ifdef DEBUG
    int rtnv = m_vmm->ReadSafe(m_i2c_receive, &data);
    CDBG(">>>>>>>>>>>>>>>> - RECV = 0x%08x",data);
    return(rtnv);
#else
    return(m_vmm->ReadSafe(m_i2c_receive, &data));
#endif
}

//------------------------------------------------------------------------------

int I2C::Receive_Write(unsigned int data) {

    return(m_vmm->WriteSafe(m_i2c_receive, data));
}

//------------------------------------------------------------------------------

int I2C::Command_Read(I2C_COMMAND_BITSTRING& bitset) {

    unsigned int data;

    if((m_status = m_vmm->ReadSafe(m_i2c_command, &data)) == 0) {
        std::vector<unsigned int> outp;
        outp.push_back(data);
        bitset = (RCD::BitSet)(outp);
    }
    return(m_status);
}

//------------------------------------------------------------------------------

int I2C::Command_Write(I2C_COMMAND_BITSTRING& bitset) {

    std::vector<unsigned int> outp = bitset.number();
    unsigned int data = outp[0];

    CDBG(">>>>>>>>>>>>>>>> - CMND = 0x%08x (\"%s\")",data,printCommand(data).c_str());
    return(m_vmm->WriteSafe(m_i2c_command, data));
}

//------------------------------------------------------------------------------

int I2C::Status_Read(I2C_STATUS_BITSTRING& bitset) {

    unsigned int data;

    if((m_status = m_vmm->ReadSafe(m_i2c_status, &data)) == 0) {
        CDBG(">>>>>>>>>>>>>>>> - STAT = 0x%08x (\"%s\")",data,printStatus(data).c_str());
        std::vector<unsigned int> outp;
        outp.push_back(data);
        bitset = (RCD::BitSet)(outp);
    }
    return(m_status);
}

//------------------------------------------------------------------------------

int I2C::Status_Write(I2C_STATUS_BITSTRING& bitset) {

    std::vector<unsigned int> outp = bitset.number();
    unsigned int data = outp[0];

    return(m_vmm->WriteSafe(m_i2c_status, data));
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

std::string I2C::printAddress(const unsigned int addr) {

    char s[STRING_LENGTH];

    std::sprintf(s,"0x%02x%s",(addr>>ADDR_SHFT)&ADDR_MASK,addr&READ_MASK?"R":"W");

    return(std::string(s));
}

//------------------------------------------------------------------------------

std::string I2C::printCommand(const unsigned int cmmd) {

    std::ostringstream buf;
    bool flag(false);

    if(cmmd & CMMD_STA_MASK) {
        buf << "START";
        flag = true;
    }
    if(cmmd & CMMD_RD_MASK) {
        buf << (flag?"|":"") << "READ";
        flag = true;
    }
    if(cmmd & CMMD_WR_MASK) {
        buf << (flag?"|":"") << "WRITE";
        flag = true;
    }
    if(!(cmmd & CMMD_ACK_MASK)) {
        buf << (flag?"|":"") << "ACK";
        flag = true;
    }
    if(cmmd & CMMD_STO_MASK) {
        buf << (flag?"|":"") << "STOP";
    }

    return(buf.str());
}

//------------------------------------------------------------------------------

std::string I2C::printStatus(const unsigned int stat) {

    std::ostringstream buf;
    bool flag(false);

    if(stat & STAT_IRQ_MASK) {
        buf << "INTERRUPT";
        flag = true;
    }
    if(stat & STAT_BSY_MASK) {
        buf << (flag?"|":"") << "BUSY";
        flag = true;
    }
    buf << (flag?"|":"") << ((stat & STAT_TIP_MASK)?"PROGRESS":"COMPLETE");
    flag = true;
    if(!(stat & STAT_ACK_MASK)) {
        buf << (flag?"|":"") << "RxACK";
    }

    return(buf.str());
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int I2C::Prescale(unsigned int prsc) {

    // select clock speed
    if((m_status = PrescaleLow_Write(prsc&PRSC_LL_MASK)) != SUCCESS) {
        CERR("writing prescale low value","");
        return(m_status);
    }
    if((m_status = PrescaleHigh_Write(((prsc>>PRSC_LL_SHIFT)&PRSC_LL_MASK))) != SUCCESS) {
        CERR("writing prescale high value","");
        return(m_status);
    }
    CDBG("writing prescale value 0x%04x (%d)",prsc&PRSC_MASK,prsc&PRSC_MASK);

    return(m_status);
}

//------------------------------------------------------------------------------

int I2C::PrescaleRead(unsigned int& prsc) {

    unsigned int data;

    // select clock speed
    if((m_status = PrescaleLow_Read(data)) != SUCCESS) {
        CERR("reading prescale low value","");
        return(m_status);
    }
    prsc = data&PRSC_LL_MASK;
    if((m_status = PrescaleHigh_Read((data))) != SUCCESS) {
        CERR("reading prescale high value","");
        return(m_status);
    }
    prsc |= (data&PRSC_LL_MASK)<<PRSC_LL_SHIFT;
    CDBG("reading prescale value 0x%04x (%d)",prsc&PRSC_MASK,prsc&PRSC_MASK);

    return(m_status);
}

//------------------------------------------------------------------------------

int I2C::Enable() {

    I2C_CONTROL_BITSTRING ctrl;

    // enable I2C interface
    ctrl.Enable("ENABLED");
    if((m_status = Control_Write(ctrl)) != SUCCESS) {
        CERR("enabling I2C","");
        return(m_status);
    }
    CDBG("writing control value 0x%08x (\"%s\")",ctrl.number()[0],ctrl.string().c_str());

    return(m_status);
}

//------------------------------------------------------------------------------

int I2C::Disable() {

    I2C_CONTROL_BITSTRING ctrl;

    // disable I2C interface
    ctrl.Enable("DISABLED");
    if((m_status = Control_Write(ctrl)) != SUCCESS) {
        CERR("disabling I2C","");
        return(m_status);
    }
    CDBG("writing control value %08x (\"%s\")%s",ctrl.number()[0],ctrl.string().c_str());

    return(m_status);
}

//------------------------------------------------------------------------------

int I2C::EnableRead(bool& ena) {

    I2C_CONTROL_BITSTRING ctrl;

    // read I2C enable
    if((m_status = Control_Read(ctrl)) != SUCCESS) {
        CERR("reading I2C control","");
        return(m_status);
    }
    ena = ctrl.Enable() == "ENABLED";

    return(m_status);
}

//------------------------------------------------------------------------------

int I2C::EnableWrite(const bool ena) {

    I2C_CONTROL_BITSTRING ctrl;

    // write I2C enable
    ctrl.Enable(ena?"ENABLED":"DISABLED");
    if((m_status = Control_Write(ctrl)) != SUCCESS) {
        CERR("writing I2C control","");
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int I2C::WaitTransfer(const bool stop) {

    unsigned int itim(0);

    // read status and wait for transfer completed
    while(true) {
        CDBG("reading I2C status, loop = %d:",itim);
        if((m_status = Status_Read(m_stat)) != SUCCESS) {
            CERR("reading I2C status","");
            return(m_status);
        }
        if(m_stat.Transfer() == "COMPLETED") {
             if(!stop && (m_stat.RxACK() != "RECEIVED")) return(FAILURE);
             return(SUCCESS);
        }
        if((++itim) >= STAT_TIMEOUT) {
            CERR("timeout waiting for transfer to complete","");
            return(TIMEOUT);
        }
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int I2C::WriteAddress(const unsigned int addr, const bool read) {

    unsigned int udat;

    // write address
    udat = (addr&ADDR_MASK)<<ADDR_SHFT;
    if(read) udat |= READ_MASK;
    CDBG(">>>>>>>>>>>>>>>> - ADDR = 0x%08x (\"%s\")",udat,printAddress(udat).c_str());
    if((m_status = m_vmm->WriteSafe(m_i2c_transmit_addr,udat)) != SUCCESS) {
        CERR("writing transmit address","");
        return(m_status);
    }

    // write command: START|WRITE
    m_cmd.STA("ON");
    m_cmd.STO("OFF");
    m_cmd.RD("OFF");
    m_cmd.WR("ON");
    m_cmd.ACK("OFF");
    if((m_status = Command_Write(m_cmd)) != SUCCESS) {
        CERR("writing start command","");
        return(m_status);
    }

    // wait for transfer to complete
    if((m_status = WaitTransfer(false)) != SUCCESS) {
        if(m_status == FAILURE) {
            CERR("waiting for device to acknowledge","");
            return(m_status);
        }
        else {
            CERR("waiting for transfer to complete","");
            return(m_status);
        }
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int I2C::WriteData(const unsigned int data, const bool stop) {

    unsigned int udat;

    // write data
    udat = data&DATA_MASK;
    if ((m_status = TransmitData_Write(udat)) != SUCCESS) {
      CERR("writing transmit data","");
      return(m_status);
    }

    // write command: STOP|WRITE(|ACK)
    m_cmd.STA("OFF");
    m_cmd.STO(stop?"ON":"OFF");
    m_cmd.RD("OFF");
    m_cmd.WR("ON");
    m_cmd.ACK("OFF");
    if((m_status = Command_Write(m_cmd)) != SUCCESS) {
        CERR("writing stop command","");
        return(m_status);
    }

    // wait for transfer to complete
    if((m_status = WaitTransfer(stop)) != SUCCESS) {
        if(m_status == FAILURE) {
            CERR("waiting for device to acknowledge","");
            return(m_status);
        }
        else {
            CERR("waiting for transfer to complete","");
            return(m_status);
        }
    }
    return(m_status);
}

//------------------------------------------------------------------------------

int I2C::ReadData(unsigned int& data, const bool stop) {

    // write command: STOP|READ(|ACK)
    m_cmd.STA("OFF");
    m_cmd.STO(stop?"ON":"OFF");
    m_cmd.RD("ON");
    m_cmd.WR("OFF");
    m_cmd.ACK(stop?"ON":"OFF");
    if((m_status = Command_Write(m_cmd)) != SUCCESS) {
        CERR("writing stop command","");
        return(m_status);
    }

    // wait for transfer to complete
    if((m_status = WaitTransfer(stop)) != SUCCESS) {
        if(m_status == FAILURE) {
            CERR("waiting for device to acknowledge","");
            return(m_status);
        }
        else {
            CERR("waiting for transfer to complete","");
            return(m_status);
        }
    }

    // read data
    if((m_status = TransmitData_Read(data)) != SUCCESS) {
        CERR("reading transmit data","");
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int I2C::Write(unsigned int addr, unsigned int data) {

    // write address
    if((m_status = WriteAddress(addr,false)) != SUCCESS) {
        CERR("writing address 0x%02x",addr);
        return(m_status);
    }

    // write data
    if ((m_status = WriteData(data&DATA_MASK,true)) != SUCCESS) {
        CERR("writing data","");
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int I2C::Read(unsigned int addr, unsigned int& data) {

    // write address
    if((m_status = WriteAddress(addr,true)) != SUCCESS) {
        CERR("writing address 0x%02x",addr);
        return(m_status);
    }

    // read data
    if ((m_status = ReadData(data,true)) != SUCCESS) {
        CERR("reading data","");
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------
// READ I2C BYTE
//------------------------------------------------------------------------------

int I2C::DataRead(const unsigned int addr, uint8_t& data) {

    CDBG("R%s: addr = 0x%02x",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::BYTE].c_str(),addr);

    unsigned int datx;
    if((m_status = DataRead(addr,datx)) != SUCCESS) return(m_status);
    data = datx & 0xff;

    return(m_status);
}

//------------------------------------------------------------------------------
// READ I2C DATA
//------------------------------------------------------------------------------

int I2C::DataRead(const unsigned int addr, const unsigned int offs, uint8_t& data) {

    CDBG("R%s: addr = 0x%02x, offs = 0x%02x",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::DATA].c_str(),addr,offs);

    unsigned int datx[1], numb(1);
    if((m_status = DataRead(addr,offs,numb,datx)) != SUCCESS)return(m_status);
    data = datx[0] & 0xff;

    return(m_status);
}

//------------------------------------------------------------------------------
// READ I2C WORD
//------------------------------------------------------------------------------

int I2C::DataRead(const unsigned int addr, const unsigned int offs, uint16_t& data) {

    CDBG("R%s: addr = 0x%02x, offs = 0x%02x",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::WORD].c_str(),addr,offs);

    unsigned int datx[2], numb(2);
    if((m_status = DataRead(addr,offs,numb,datx)) != SUCCESS) return(m_status);
    data = ((datx[0] & 0xff) << 8) | (datx[1] & 0xff); // big-endian

    return(m_status);
}

//------------------------------------------------------------------------------
// READ I2C IBLK (as unsigned int)
//------------------------------------------------------------------------------

int I2C::DataRead(const unsigned int addr, const unsigned int offs, unsigned int& numb, unsigned int data[]) {

    CDBG("R%s: addr = 0x%02x, offs = 0x%02x, numb = %d",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::IBLK].c_str(),addr,offs,numb);

    if(offs >= DATA_SIZE) {
        CERR("byte offset %d not in allowed range [0..%d]",offs,DATA_SIZE-1);
        return(FAILURE);
    }
    if((offs + numb) > DATA_SIZE) {
        CERR("number of bytes %d not in allowed range [0..%d]",numb,DATA_SIZE-offs);
        return(FAILURE);
    }

    // write address
    if((m_status = WriteAddress(addr,false)) != SUCCESS) {
        CERR("writing address 0x%02x - FIRST",addr);
        return(m_status);
    }

    // write offset
    if((m_status = WriteData(offs,false)) != SUCCESS) {
        CERR("writing data offset 0x%02x",offs);
        return(m_status);
    }

    // write address
    if((m_status = WriteAddress(addr,true)) != SUCCESS) {
        CERR("writing address 0x%02x - SECOND",addr);
        return(m_status);
    }

    // read data
    for(unsigned int i=0; i<numb; i++) {
        if ((m_status = ReadData(data[i],i==(numb-1))) != SUCCESS) {
            CERR("reading data byte %d",i);
            return(m_status);
        }
    }

    return(m_status);
}

//------------------------------------------------------------------------------
// READ I2C dispatcher function
//------------------------------------------------------------------------------

int I2C::DataRead(const unsigned int addr, const unsigned int offs, unsigned int& numb, uint8_t data[], const TRANSFER_TYPE ttyp) {

    CDBG("R%s: addr = 0x%02x, offs = 0x%02x, numb = %d",TRANSFER_TYPE_NAME[ttyp].c_str(),addr,offs,numb);

    switch(ttyp) {
        case BYTE: {
                uint8_t offx;
                if((m_status = DataRead(addr,offx)) != SUCCESS) return(m_status);
                data[0] = offx;
                numb = 1;
            }
            break;
        case DATA: {
                uint8_t datc;
                if((m_status = DataRead(addr,offs,datc)) != SUCCESS) return(m_status);
                data[0] = datc;
                numb = 1;
            }
            break;
        case WORD: {
                uint16_t dats;
                if((m_status = DataRead(addr,offs,dats)) != SUCCESS) return(m_status);
                data[0] = (dats >> 8) & 0xff;
                data[1] = dats & 0xff;
                numb = 2;
            }
            break;
        case IBLK: {
                unsigned int datx[MAX_BLOCK];
                if((m_status = DataRead(addr,offs,numb,datx)) != SUCCESS) return(m_status);
                for(unsigned int i=0; i<numb; i++) data[i] = datx[i];
            }
            break;
        default:
            CERR("I2C READ(%s) not implemented",TRANSFER_TYPE_NAME[ttyp].c_str());
            return(m_status = WRONGPAR);
            break;
    }

    return(m_status);
}

//------------------------------------------------------------------------------
// WRITE I2C BYTE
//------------------------------------------------------------------------------

int I2C::DataWrite(const unsigned int addr, uint8_t data) {

    CDBG("W%s: addr = 0x%02x, data = 0x%02x",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::BYTE].c_str(),addr,data);

    unsigned int datx(data);
    m_status = DataWrite(addr,datx);

    return(m_status);
}

//------------------------------------------------------------------------------
// WRITE I2C DATA
//------------------------------------------------------------------------------

int I2C::DataWrite(const unsigned int addr, const unsigned int offs, const uint8_t data) {

    CDBG("W%s: addr = 0x%02x, offs = 0x%02x, data = 0x%02x",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::DATA].c_str(),addr,offs,data);

    unsigned int datx(data);
    if((m_status = DataWrite(addr,offs,datx)) != SUCCESS) return(m_status);

    return(m_status);
}

//------------------------------------------------------------------------------
// WRITE I2C WORD
//------------------------------------------------------------------------------

int I2C::DataWrite(const unsigned int addr, const unsigned int offs, const uint16_t data) {

    CDBG("W%s: addr = 0x%02x, offs = 0x%02x, data = 0x%04x",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::WORD].c_str(),addr,offs,data);

    unsigned int datx[2];
    datx[0] = data >> 8;
    datx[1] = data & 0xff;
    if((m_status = DataWrite(addr,offs,2,datx)) != SUCCESS) return(m_status);

    return(m_status);
}

//------------------------------------------------------------------------------
// WRITE I2C DATA (as unsigned int)
//------------------------------------------------------------------------------

int I2C::DataWrite(const unsigned int addr, const unsigned int offs, const unsigned int data) {

    CDBG("W%s: addr = 0x%02x, offs = 0x%02x, data = 0x%02x",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::DATA].c_str(),addr,offs,data);

    if(offs >= DATA_SIZE) {
        CERR("byte offset %d not in allowed range [0..%d]",offs,DATA_SIZE-1);
        return(FAILURE);
    }

    // write address
    if((m_status = WriteAddress(addr,false)) != SUCCESS) {
        CERR("writing address 0x%02x - FIRST",addr);
        return(m_status);
    }

    // write offset
    if((m_status = WriteData(offs,false)) != SUCCESS) {
        CERR("writing data offset 0x%02x",offs);
        return(m_status);
    }

    // write data
    if ((m_status = WriteData(data,true)) != SUCCESS) {
        CERR("writing data byte #0","");
        return(m_status);
    }

    return(m_status);
}

//------------------------------------------------------------------------------
// WRITE I2C IBLK (as unsigned int)
//------------------------------------------------------------------------------

int I2C::DataWrite(const unsigned int addr, const unsigned int offs, const unsigned int numb, const unsigned int data[]) {

    CDBG("W%s: addr = 0x%02x, offs = 0x%02x, numb = %d",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::IBLK].c_str(),addr,offs,numb);

    if(offs >= DATA_SIZE) {
        CERR("byte offset %d not in allowed range [0..%d]",offs,DATA_SIZE-1);
        return(FAILURE);
    }
    if((offs + numb) > DATA_SIZE) {
        CERR("number of bytes %d not in allowed range [0..%d]",numb,DATA_SIZE-offs);
        return(FAILURE);
    }

    // write address
    if((m_status = WriteAddress(addr,false)) != SUCCESS) {
        CERR("writing address 0x%02x - FIRST",addr);
        return(m_status);
    }

    // write offset
    if((m_status = WriteData(offs,false)) != SUCCESS) {
        CERR("writing data offset 0x%02x",offs);
        return(m_status);
    }

    // write data
    for(unsigned int i=0; i<numb; i++) {
        if ((m_status = WriteData(data[i],i==(numb-1))) != SUCCESS) {
            CERR("writing data byte #%d",i);
            return(m_status);
        }
    }

    return(m_status);
}

//------------------------------------------------------------------------------
// WRITE I2C dispatcher function
//------------------------------------------------------------------------------

int I2C::DataWrite(const unsigned int addr, const unsigned int offs, unsigned int& numb, const uint8_t data[], const TRANSFER_TYPE ttyp) {

    CDBG("W%s: addr = 0x%02x, off = 0x%02x, numb = %d",TRANSFER_TYPE_NAME[ttyp].c_str(),addr,offs,numb);

    switch(ttyp) {
        case BYTE: {
                uint8_t datx(offs);
                if((m_status = DataWrite(addr,datx)) != SUCCESS) return(m_status);
                numb = 1;
            }
            break;
        case DATA: {
                uint8_t datx(data[0]);
                if((m_status = DataWrite(addr,offs,datx)) != SUCCESS) return(m_status);
                numb = 1;
            }
            break;
        case WORD: {
                uint16_t datx((data[0]<<8) | data[1]);
                if((m_status = DataWrite(addr,offs,datx)) != SUCCESS) return(m_status);
            }
            break;
        case IBLK: {
                unsigned int datx[MAX_BLOCK];
                for(unsigned int i=0; i<numb; i++) datx[i] = data[i];
                if((m_status = DataWrite(addr,offs,numb,datx)) != SUCCESS) return(m_status);
            }
            break;
        default:
            CERR("I2C WRITE(%s) not implemented",TRANSFER_TYPE_NAME[ttyp].c_str());
            return(m_status = WRONGPAR);
            break;
    }

    return(m_status);
}
