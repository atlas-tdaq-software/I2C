//******************************************************************************
// file: I2CCommon.cc
// desc: pure status class for I2C common parameters
// auth: 29-JAN-2019 R. Spiwoks, copied from MuctpiCommon.cc
//******************************************************************************

#include "I2C/I2CCommon.h"
#include "RCDUtilities/RCDUtilities.h"

#include <sstream>
#include <cstdint>

using namespace LVL1;

//------------------------------------------------------------------------------

std::string I2CCommon::leftTrimString(const std::string& s) {

    std::string::size_type idx(0), siz(s.size());

    while ((idx < siz) && (std::isspace(s[idx]))) {
        idx++;
    }
    CDBG("str = \"%s\", siz = %d, idx = %d",s.c_str(),siz,idx);

    return(s.substr(idx,siz-idx));
}

//------------------------------------------------------------------------------

std::string I2CCommon::rightTrimString(const std::string& s) {

    std::string::size_type idx(s.size());

    while ((idx > 0) && (std::isspace(s[idx-1]))) {
        idx--;
    }
    CDBG("str = \"%s\", siz = %d, idx = %d",s.c_str(),s.size(),idx);

    return(s.substr(0,idx));
}

//------------------------------------------------------------------------------

std::string I2CCommon::trimString(const std::string& s) {

    return(rightTrimString(leftTrimString(s)));
}

//------------------------------------------------------------------------------

std::string I2CCommon::printableString(const std::string& s) {

    std::ostringstream buf;

    for(unsigned int i=0; i<s.size(); i++) {
        if((uint8_t)s[i] < 32) buf << "?";
        else if ((uint8_t)s[i] >= 127 ) buf << ".";
        else buf << s[i];
    }

    return(buf.str());
}

//------------------------------------------------------------------------------

int I2CCommon::splitString(const std::string& str, const std::string& del, std::vector<std::string>& rslt) {

    rslt.clear();

    if(str.empty()) return(0);

    std::string::size_type idx0(0), idx1(0);

    while(true) {
        idx1 = str.find(del,idx0);
        if(idx1 != std::string::npos) {
            rslt.push_back(str.substr(idx0,idx1-idx0));
        }
        else {
            rslt.push_back(str.substr(idx0));
            break;
        }
        idx0 = idx1+1;
    }

    return(rslt.size());
}
