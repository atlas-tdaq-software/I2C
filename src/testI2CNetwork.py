#/usr/bin/env python

# export PYTHONPATH=/usr/lib

# import I2Cnetwork module
import I2CNetwork

# create I2CDevice on /dev/i2c-0
i2c = I2CNetwork.I2CDevice(0,False,0)

# create I2CDevice on /dev/i2c-1
# i2c = I2CNetwork.I2CDevice(1,False,0)

# create I2CNetwork
net = I2CNetwork.I2CNetwork()

# use I2CDevice as BUS0 of I2CNetwork
net.addI2CBus("BUS0",i2c)

# initialise BUS0 as MUCTPI0
net.initI2CBus(0,I2CNetwork.I2CNetwork.BUS_MUCTPI0,0)

# initialise BUS0 as MUCTPI0
#net.initI2CBus(0,I2CNetwork.I2CNetwork.BUS_MUCTPI1,0)

# read I2CQuantities from BUS0
net.dumpI2CQuantity(0,0,53)
net.dumpI2CQuantity(0,0,54)

