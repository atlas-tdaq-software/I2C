//******************************************************************************
// file: I2C_BITSTRING.cc
// desc: I2C Interface - Bit Strings
// auth: 05/08/04, Ralf SPIWOKS
//******************************************************************************

// $Id: I2C_BITSTRING.cc,v 1.3 2007/04/13 09:14:04 berge Exp $

#include <I2C/I2C_BITSTRING.h>
#include "RCDUtilities/RCDUtilities.h"

#include <iostream>
#include <iomanip>

using namespace LVL1;

//------------------------------------------------------------------------------

I2C_CONTROL_BITSTRING::I2C_CONTROL_BITSTRING() : RCD::BitSet(32) {

    // nothing to do
}

//------------------------------------------------------------------------------

I2C_CONTROL_BITSTRING::~I2C_CONTROL_BITSTRING() {

    // nothing to do
}

//------------------------------------------------------------------------------

I2C_CONTROL_BITSTRING& I2C_CONTROL_BITSTRING::operator=(const RCD::BitSet& bs) {

    if(this != &bs) {
        m_number = bs.number();
    }
    return(*this);
}

//------------------------------------------------------------------------------

std::string I2C_CONTROL_BITSTRING::Enable() const {

    std::string  str;

    if(((*this) & MASK_ENABLE) == VALUE_ENABLE_DISABLED) {
        str = "DISABLED";
     }
    else if(((*this) & MASK_ENABLE) == VALUE_ENABLE_ENABLED) {
        str = "ENABLED";
     }
    else {
        CERR("finding value for data \"%s\"",((*this) & MASK_ENABLE).string().c_str());
    }

    return(str);
}

//------------------------------------------------------------------------------

void I2C_CONTROL_BITSTRING::Enable(const std::string& value) {

    if(value == "DISABLED") {
        *this = (*this & (~MASK_ENABLE)) | VALUE_ENABLE_DISABLED;
    }
    else if(value == "ENABLED") {
        *this = (*this & (~MASK_ENABLE)) | VALUE_ENABLE_ENABLED;
    }
    else {
        CERR("finding value \"%s\"",value.c_str());
    }
}

//------------------------------------------------------------------------------

std::string I2C_CONTROL_BITSTRING::InterruptEnable() const {

    std::string  str;

    if(((*this) & MASK_INTERRUPTENABLE) == VALUE_INTERRUPTENABLE_DISABLED) {
        str = "DISABLED";
     }
    else if(((*this) & MASK_INTERRUPTENABLE) == VALUE_INTERRUPTENABLE_ENABLED) {
        str = "ENABLED";
     }
    else {
        CERR("finding value for data \"%s\"",((*this) & MASK_INTERRUPTENABLE).string().c_str());
    }

    return(str);
}

//------------------------------------------------------------------------------

void I2C_CONTROL_BITSTRING::InterruptEnable(const std::string& value) {

    if(value == "DISABLED") {
        *this = (*this & (~MASK_INTERRUPTENABLE)) | VALUE_INTERRUPTENABLE_DISABLED;
    }
    else if(value == "ENABLED") {
        *this = (*this & (~MASK_INTERRUPTENABLE)) | VALUE_INTERRUPTENABLE_ENABLED;
    }
    else {
        CERR("finding value \"%s\"",value.c_str());
    }
}


//------------------------------------------------------------------------------

void I2C_CONTROL_BITSTRING::print() {

    std::string str;

    std::cout << std::endl;
    std::cout << "  \"I2C_CONTROL_BITSTRING\": " << string() << std::endl;

    str = "\"" + Enable() + "\"";
    std::cout << "    " << std::setw(28) << std::left << "\"Enable\"" << ": " << std::setw(20) << std::right << str << std::endl;
    str = "\"" + InterruptEnable() + "\"";
    std::cout << "    " << std::setw(28) << std::left << "\"InterruptEnable\"" << ": " << std::setw(20) << std::right << str << std::endl;

}

const RCD::BitSet I2C_CONTROL_BITSTRING::MASK_ENABLE = RCD::BitSet("0x00000080");
const RCD::BitSet I2C_CONTROL_BITSTRING::VALUE_ENABLE_DISABLED = RCD::BitSet("0x00000000");
const RCD::BitSet I2C_CONTROL_BITSTRING::VALUE_ENABLE_ENABLED = RCD::BitSet("0x00000080");

const RCD::BitSet I2C_CONTROL_BITSTRING::MASK_INTERRUPTENABLE = RCD::BitSet("0x00000040");
const RCD::BitSet I2C_CONTROL_BITSTRING::VALUE_INTERRUPTENABLE_DISABLED = RCD::BitSet("0x00000000");
const RCD::BitSet I2C_CONTROL_BITSTRING::VALUE_INTERRUPTENABLE_ENABLED = RCD::BitSet("0x00000040");

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

I2C_TRANSMITADDR_BITSTRING::I2C_TRANSMITADDR_BITSTRING() : RCD::BitSet(32) {

    // nothing to do
}

//------------------------------------------------------------------------------

I2C_TRANSMITADDR_BITSTRING::~I2C_TRANSMITADDR_BITSTRING() {

    // nothing to do
}

//------------------------------------------------------------------------------

I2C_TRANSMITADDR_BITSTRING& I2C_TRANSMITADDR_BITSTRING::operator=(const RCD::BitSet& bs) {

    if(this != &bs) {
        m_number = bs.number();
    }
    return(*this);
}

//------------------------------------------------------------------------------

std::string I2C_TRANSMITADDR_BITSTRING::SlaveAddress() const {

    std::string  str;

    BitSet       obs(m_size);
    int          idx = 0;

    for(size_t i = 0; i < m_size; i++) {
        if(MASK_SLAVEADDRESS[i] == 1) {
            obs[idx] = (*this)[i];
            idx++;
         }
    }
    str = obs.string(true,false);

    return(str);
}

//------------------------------------------------------------------------------

void I2C_TRANSMITADDR_BITSTRING::SlaveAddress(const std::string& value) {

    BitSet       ibs(value);
    int          idx = 0;

    for(size_t i = 0; i < m_size; i++) {
        if(MASK_SLAVEADDRESS[i] == 1) {
            (*this)[i] = ibs[idx];
            idx++;
        }
    }
}

//------------------------------------------------------------------------------

std::string I2C_TRANSMITADDR_BITSTRING::ReadWriteFlag() const {

    std::string  str;

    if(((*this) & MASK_READWRITEFLAG) == VALUE_READWRITEFLAG_READ) {
        str = "READ";
     }
    else if(((*this) & MASK_READWRITEFLAG) == VALUE_READWRITEFLAG_WRITE) {
        str = "WRITE";
     }
    else {
        CERR("finding value for data \"%s\"",((*this) & MASK_READWRITEFLAG).string().c_str());
    }

    return(str);
}

//------------------------------------------------------------------------------

void I2C_TRANSMITADDR_BITSTRING::ReadWriteFlag(const std::string& value) {

    if(value == "READ") {
        *this = (*this & (~MASK_READWRITEFLAG)) | VALUE_READWRITEFLAG_READ;
    }
    else if(value == "WRITE") {
        *this = (*this & (~MASK_READWRITEFLAG)) | VALUE_READWRITEFLAG_WRITE;
    }
    else {
        CERR("finding value \"%s\"",value.c_str());
    }
}


//------------------------------------------------------------------------------

void I2C_TRANSMITADDR_BITSTRING::print() {

    std::string str;

    std::cout << std::endl;
    std::cout << "  \"I2C_TRANSMITADDR_BITSTRING\": " << string() << std::endl;

    str = "\"" + SlaveAddress() + "\"";
    std::cout << "    " << std::setw(28) << std::left << "\"SlaveAddress\"" << ": " << std::setw(20) << std::right << str << std::endl;
    str = "\"" + ReadWriteFlag() + "\"";
    std::cout << "    " << std::setw(28) << std::left << "\"ReadWriteFlag\"" << ": " << std::setw(20) << std::right << str << std::endl;

}

const RCD::BitSet I2C_TRANSMITADDR_BITSTRING::MASK_SLAVEADDRESS = RCD::BitSet("0x000000fe");

const RCD::BitSet I2C_TRANSMITADDR_BITSTRING::MASK_READWRITEFLAG = RCD::BitSet("0x00000001");
const RCD::BitSet I2C_TRANSMITADDR_BITSTRING::VALUE_READWRITEFLAG_READ = RCD::BitSet("0x00000001");
const RCD::BitSet I2C_TRANSMITADDR_BITSTRING::VALUE_READWRITEFLAG_WRITE = RCD::BitSet("0x00000000");

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

I2C_COMMAND_BITSTRING::I2C_COMMAND_BITSTRING() : RCD::BitSet(32) {

    // nothing to do
}

//------------------------------------------------------------------------------

I2C_COMMAND_BITSTRING::~I2C_COMMAND_BITSTRING() {

    // nothing to do
}

//------------------------------------------------------------------------------

I2C_COMMAND_BITSTRING& I2C_COMMAND_BITSTRING::operator=(const RCD::BitSet& bs) {

    if(this != &bs) {
        m_number = bs.number();
    }
    return(*this);
}

//------------------------------------------------------------------------------

std::string I2C_COMMAND_BITSTRING::STA() const {

    std::string  str;

    if(((*this) & MASK_STA) == VALUE_STA_OFF) {
        str = "OFF";
     }
    else if(((*this) & MASK_STA) == VALUE_STA_ON) {
        str = "ON";
     }
    else {
        CERR("finding value for data \"%s\"",((*this) & MASK_STA).string().c_str());
    }

    return(str);
}

//------------------------------------------------------------------------------

void I2C_COMMAND_BITSTRING::STA(const std::string& value) {

    if(value == "OFF") {
        *this = (*this & (~MASK_STA)) | VALUE_STA_OFF;
    }
    else if(value == "ON") {
        *this = (*this & (~MASK_STA)) | VALUE_STA_ON;
    }
    else {
        CERR("finding value \"%s\"",value.c_str());
    }
}

//------------------------------------------------------------------------------

std::string I2C_COMMAND_BITSTRING::STO() const {

    std::string  str;

    if(((*this) & MASK_STO) == VALUE_STO_OFF) {
        str = "OFF";
     }
    else if(((*this) & MASK_STO) == VALUE_STO_ON) {
        str = "ON";
     }
    else {
        CERR("finding value for data \"%s\"",((*this) & MASK_STO).string().c_str());
    }

    return(str);
}

//------------------------------------------------------------------------------

void I2C_COMMAND_BITSTRING::STO(const std::string& value) {

    if(value == "OFF") {
        *this = (*this & (~MASK_STO)) | VALUE_STO_OFF;
    }
    else if(value == "ON") {
        *this = (*this & (~MASK_STO)) | VALUE_STO_ON;
    }
    else {
        CERR("finding value \"%s\"",value.c_str());
    }
}

//------------------------------------------------------------------------------

std::string I2C_COMMAND_BITSTRING::RD() const {

    std::string  str;

    if(((*this) & MASK_RD) == VALUE_RD_OFF) {
        str = "OFF";
     }
    else if(((*this) & MASK_RD) == VALUE_RD_ON) {
        str = "ON";
     }
    else {
        CERR("finding value for data \"%s\"",((*this) & MASK_RD).string().c_str());
    }

    return(str);
}

//------------------------------------------------------------------------------

void I2C_COMMAND_BITSTRING::RD(const std::string& value) {

    if(value == "OFF") {
        *this = (*this & (~MASK_RD)) | VALUE_RD_OFF;
    }
    else if(value == "ON") {
        *this = (*this & (~MASK_RD)) | VALUE_RD_ON;
    }
    else {
        CERR("finding value \"%s\"",value.c_str());
    }
}

//------------------------------------------------------------------------------

std::string I2C_COMMAND_BITSTRING::WR() const {

    std::string  str;

    if(((*this) & MASK_WR) == VALUE_WR_OFF) {
        str = "OFF";
     }
    else if(((*this) & MASK_WR) == VALUE_WR_ON) {
        str = "ON";
     }
    else {
        CERR("finding value for data \"%s\"",((*this) & MASK_WR).string().c_str());
    }

    return(str);
}

//------------------------------------------------------------------------------

void I2C_COMMAND_BITSTRING::WR(const std::string& value) {

    if(value == "OFF") {
        *this = (*this & (~MASK_WR)) | VALUE_WR_OFF;
    }
    else if(value == "ON") {
        *this = (*this & (~MASK_WR)) | VALUE_WR_ON;
    }
    else {
        CERR("finding value \"%s\"",value.c_str());
    }
}

//------------------------------------------------------------------------------

std::string I2C_COMMAND_BITSTRING::ACK() const {

    std::string  str;

    if(((*this) & MASK_ACK) == VALUE_ACK_OFF) {
        str = "OFF";
     }
    else if(((*this) & MASK_ACK) == VALUE_ACK_ON) {
        str = "ON";
     }
    else {
        CERR("finding value for data \"%s\"",((*this) & MASK_ACK).string().c_str());
    }

    return(str);
}

//------------------------------------------------------------------------------

void I2C_COMMAND_BITSTRING::ACK(const std::string& value) {

    if(value == "OFF") {
        *this = (*this & (~MASK_ACK)) | VALUE_ACK_OFF;
    }
    else if(value == "ON") {
        *this = (*this & (~MASK_ACK)) | VALUE_ACK_ON;
    }
    else {
        CERR("finding value \"%s\"",value.c_str());
    }
}

//------------------------------------------------------------------------------

std::string I2C_COMMAND_BITSTRING::IACK() const {

    std::string  str;

    if(((*this) & MASK_IACK) == VALUE_IACK_OFF) {
        str = "OFF";
     }
    else if(((*this) & MASK_IACK) == VALUE_IACK_ON) {
        str = "ON";
     }
    else {
        CERR("finding value for data \"%s\"",((*this) & MASK_IACK).string().c_str());
    }

    return(str);
}

//------------------------------------------------------------------------------

void I2C_COMMAND_BITSTRING::IACK(const std::string& value) {

    if(value == "OFF") {
        *this = (*this & (~MASK_IACK)) | VALUE_IACK_OFF;
    }
    else if(value == "ON") {
        *this = (*this & (~MASK_IACK)) | VALUE_IACK_ON;
    }
    else {
        CERR("finding value \"%s\"",value.c_str());
    }
}


//------------------------------------------------------------------------------

void I2C_COMMAND_BITSTRING::print() {

    std::string str;

    std::cout << std::endl;
    std::cout << "  \"I2C_COMMAND_BITSTRING\": " << string() << std::endl;

    str = "\"" + STA() + "\"";
    std::cout << "    " << std::setw(28) << std::left << "\"STA\"" << ": " << std::setw(20) << std::right << str << std::endl;
    str = "\"" + STO() + "\"";
    std::cout << "    " << std::setw(28) << std::left << "\"STO\"" << ": " << std::setw(20) << std::right << str << std::endl;
    str = "\"" + RD() + "\"";
    std::cout << "    " << std::setw(28) << std::left << "\"RD\"" << ": " << std::setw(20) << std::right << str << std::endl;
    str = "\"" + WR() + "\"";
    std::cout << "    " << std::setw(28) << std::left << "\"WR\"" << ": " << std::setw(20) << std::right << str << std::endl;
    str = "\"" + ACK() + "\"";
    std::cout << "    " << std::setw(28) << std::left << "\"ACK\"" << ": " << std::setw(20) << std::right << str << std::endl;
    str = "\"" + IACK() + "\"";
    std::cout << "    " << std::setw(28) << std::left << "\"IACK\"" << ": " << std::setw(20) << std::right << str << std::endl;

}

const RCD::BitSet I2C_COMMAND_BITSTRING::MASK_STA = RCD::BitSet("0x00000080");
const RCD::BitSet I2C_COMMAND_BITSTRING::VALUE_STA_OFF = RCD::BitSet("0x00000000");
const RCD::BitSet I2C_COMMAND_BITSTRING::VALUE_STA_ON = RCD::BitSet("0x00000080");

const RCD::BitSet I2C_COMMAND_BITSTRING::MASK_STO = RCD::BitSet("0x00000040");
const RCD::BitSet I2C_COMMAND_BITSTRING::VALUE_STO_OFF = RCD::BitSet("0x00000000");
const RCD::BitSet I2C_COMMAND_BITSTRING::VALUE_STO_ON = RCD::BitSet("0x00000040");

const RCD::BitSet I2C_COMMAND_BITSTRING::MASK_RD = RCD::BitSet("0x00000020");
const RCD::BitSet I2C_COMMAND_BITSTRING::VALUE_RD_OFF = RCD::BitSet("0x00000000");
const RCD::BitSet I2C_COMMAND_BITSTRING::VALUE_RD_ON = RCD::BitSet("0x00000020");

const RCD::BitSet I2C_COMMAND_BITSTRING::MASK_WR = RCD::BitSet("0x00000010");
const RCD::BitSet I2C_COMMAND_BITSTRING::VALUE_WR_OFF = RCD::BitSet("0x00000000");
const RCD::BitSet I2C_COMMAND_BITSTRING::VALUE_WR_ON = RCD::BitSet("0x00000010");

const RCD::BitSet I2C_COMMAND_BITSTRING::MASK_ACK = RCD::BitSet("0x00000008");
const RCD::BitSet I2C_COMMAND_BITSTRING::VALUE_ACK_OFF = RCD::BitSet("0x00000000");
const RCD::BitSet I2C_COMMAND_BITSTRING::VALUE_ACK_ON = RCD::BitSet("0x00000008");

const RCD::BitSet I2C_COMMAND_BITSTRING::MASK_IACK = RCD::BitSet("0x00000001");
const RCD::BitSet I2C_COMMAND_BITSTRING::VALUE_IACK_OFF = RCD::BitSet("0x00000000");
const RCD::BitSet I2C_COMMAND_BITSTRING::VALUE_IACK_ON = RCD::BitSet("0x00000001");

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

I2C_STATUS_BITSTRING::I2C_STATUS_BITSTRING() : RCD::BitSet(32) {

    // nothing to do
}

//------------------------------------------------------------------------------

I2C_STATUS_BITSTRING::~I2C_STATUS_BITSTRING() {

    // nothing to do
}

//------------------------------------------------------------------------------

I2C_STATUS_BITSTRING& I2C_STATUS_BITSTRING::operator=(const RCD::BitSet& bs) {

    if(this != &bs) {
        m_number = bs.number();
    }
    return(*this);
}

//------------------------------------------------------------------------------

std::string I2C_STATUS_BITSTRING::RxACK() const {

    std::string  str;

    if(((*this) & MASK_RXACK) == VALUE_RXACK_NONE_RECEIVED) {
        str = "NONE_RECEIVED";
     }
    else if(((*this) & MASK_RXACK) == VALUE_RXACK_RECEIVED) {
        str = "RECEIVED";
     }
    else {
        CERR("finding value for data \"%s\"",((*this) & MASK_RXACK).string().c_str());
    }

    return(str);
}

//------------------------------------------------------------------------------

void I2C_STATUS_BITSTRING::RxACK(const std::string& value) {

    if(value == "NONE_RECEIVED") {
        *this = (*this & (~MASK_RXACK)) | VALUE_RXACK_NONE_RECEIVED;
    }
    else if(value == "RECEIVED") {
        *this = (*this & (~MASK_RXACK)) | VALUE_RXACK_RECEIVED;
    }
    else {
        CERR("finding value \"%s\"",value.c_str());
    }
}

//------------------------------------------------------------------------------

std::string I2C_STATUS_BITSTRING::Busy() const {

    std::string  str;

    if(((*this) & MASK_BUSY) == VALUE_BUSY_START_DETECTED) {
        str = "START_DETECTED";
     }
    else if(((*this) & MASK_BUSY) == VALUE_BUSY_STOP_DETECTED) {
        str = "STOP_DETECTED";
     }
    else {
        CERR("finding value for data \"%s\"",((*this) & MASK_BUSY).string().c_str());
    }

    return(str);
}

//------------------------------------------------------------------------------

void I2C_STATUS_BITSTRING::Busy(const std::string& value) {

    if(value == "START_DETECTED") {
        *this = (*this & (~MASK_BUSY)) | VALUE_BUSY_START_DETECTED;
    }
    else if(value == "STOP_DETECTED") {
        *this = (*this & (~MASK_BUSY)) | VALUE_BUSY_STOP_DETECTED;
    }
    else {
        CERR("finding value \"%s\"",value.c_str());
    }
}

//------------------------------------------------------------------------------

std::string I2C_STATUS_BITSTRING::Transfer() const {

    std::string  str;

    if(((*this) & MASK_TRANSFER) == VALUE_TRANSFER_COMPLETED) {
        str = "COMPLETED";
     }
    else if(((*this) & MASK_TRANSFER) == VALUE_TRANSFER_IN_PROGRESS) {
        str = "IN_PROGRESS";
     }
    else {
        CERR("finding value for data \"%s\"",((*this) & MASK_TRANSFER).string().c_str());
    }

    return(str);
}

//------------------------------------------------------------------------------

void I2C_STATUS_BITSTRING::Transfer(const std::string& value) {

    if(value == "COMPLETED") {
        *this = (*this & (~MASK_TRANSFER)) | VALUE_TRANSFER_COMPLETED;
    }
    else if(value == "IN_PROGRESS") {
        *this = (*this & (~MASK_TRANSFER)) | VALUE_TRANSFER_IN_PROGRESS;
    }
    else {
        CERR("finding value \"%s\"",value.c_str());
    }
}

//------------------------------------------------------------------------------

std::string I2C_STATUS_BITSTRING::Interrupt() const {

    std::string  str;

    if(((*this) & MASK_INTERRUPT) == VALUE_INTERRUPT_OFF) {
        str = "OFF";
     }
    else if(((*this) & MASK_INTERRUPT) == VALUE_INTERRUPT_ON) {
        str = "ON";
     }
    else {
        CERR("finding value for data \"%s\"",((*this) & MASK_INTERRUPT).string().c_str());
    }

    return(str);
}

//------------------------------------------------------------------------------

void I2C_STATUS_BITSTRING::Interrupt(const std::string& value) {

    if(value == "OFF") {
        *this = (*this & (~MASK_INTERRUPT)) | VALUE_INTERRUPT_OFF;
    }
    else if(value == "ON") {
        *this = (*this & (~MASK_INTERRUPT)) | VALUE_INTERRUPT_ON;
    }
    else {
        CERR("finding value \"%s\"",value.c_str());
    }
}


//------------------------------------------------------------------------------

void I2C_STATUS_BITSTRING::print() {

    std::string str;

    std::cout << "  \"I2C_STATUS_BITSTRING\": " << string() << std::endl;

    str = "\"" + RxACK() + "\"";
    std::cout << "    " << std::setw(28) << std::left << "\"RxACK\"" << ": " << std::setw(20) << std::right << str << std::endl;
    str = "\"" + Busy() + "\"";
    std::cout << "    " << std::setw(28) << std::left << "\"Busy\"" << ": " << std::setw(20) << std::right << str << std::endl;
    str = "\"" + Transfer() + "\"";
    std::cout << "    " << std::setw(28) << std::left << "\"Transfer\"" << ": " << std::setw(20) << std::right << str << std::endl;
    str = "\"" + Interrupt() + "\"";
    std::cout << "    " << std::setw(28) << std::left << "\"Interrupt\"" << ": " << std::setw(20) << std::right << str << std::endl;

}

const RCD::BitSet I2C_STATUS_BITSTRING::MASK_RXACK = RCD::BitSet("0x00000080");
const RCD::BitSet I2C_STATUS_BITSTRING::VALUE_RXACK_NONE_RECEIVED = RCD::BitSet("0x00000080");
const RCD::BitSet I2C_STATUS_BITSTRING::VALUE_RXACK_RECEIVED = RCD::BitSet("0x00000000");

const RCD::BitSet I2C_STATUS_BITSTRING::MASK_BUSY = RCD::BitSet("0x00000040");
const RCD::BitSet I2C_STATUS_BITSTRING::VALUE_BUSY_START_DETECTED = RCD::BitSet("0x00000040");
const RCD::BitSet I2C_STATUS_BITSTRING::VALUE_BUSY_STOP_DETECTED = RCD::BitSet("0x00000000");

const RCD::BitSet I2C_STATUS_BITSTRING::MASK_TRANSFER = RCD::BitSet("0x00000002");
const RCD::BitSet I2C_STATUS_BITSTRING::VALUE_TRANSFER_COMPLETED = RCD::BitSet("0x00000000");
const RCD::BitSet I2C_STATUS_BITSTRING::VALUE_TRANSFER_IN_PROGRESS = RCD::BitSet("0x00000002");

const RCD::BitSet I2C_STATUS_BITSTRING::MASK_INTERRUPT = RCD::BitSet("0x00000001");
const RCD::BitSet I2C_STATUS_BITSTRING::VALUE_INTERRUPT_OFF = RCD::BitSet("0x00000000");
const RCD::BitSet I2C_STATUS_BITSTRING::VALUE_INTERRUPT_ON = RCD::BitSet("0x00000001");

//------------------------------------------------------------------------------
