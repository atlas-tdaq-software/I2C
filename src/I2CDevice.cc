//******************************************************************************
// file: I2CDevice.cc
// desc: I2C Interface based on i2c-tools (/dev/i2c-0)
// auth: 30-AUG-2016 R. Spiwoks
// modf: 16-FEB-2017 R. Spiwoks, imported from zynq_i2c
// modf: 11-JUL-2017 R. Spiwoks, re-organization of I2C code
//******************************************************************************

// to enable debugging
//#define DEBUG

#include "I2C/I2CDevice.h"

#ifdef __ZYNQ__
#include "zynq_log/Logging.h"
#ifdef __ZYNQ_EXT_I2C_LIB__
extern "C" {
    #include <linux/i2c-dev.h>
    #include <i2c/smbus.h>
}
typedef __u8 msg_buf_type;
#else
#include <linux/i2c-dev-user.h>
typedef char msg_buf_type;
#endif
using namespace ZYNQ;
#else
#include "RCDUtilities/RCDUtilities.h"
#include "I2C/i2c-dev-future.h"
typedef char msg_buf_type;
#endif

#include <cstdio>
#include <iostream>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <string.h>

using namespace LVL1;

//------------------------------------------------------------------------------

const std::string I2CDevice::PEC_TYPE_NAME[PEC_TYPE_NUMBER] = {
    "NO_PEC", "PEC_DEVICE", "PEC_SOFT"
};

//------------------------------------------------------------------------------

I2CDevice::I2CDevice(const unsigned int bus, const bool f, const PEC_TYPE p, const unsigned int d) : I2CInterface(d), m_bus(bus), m_file(0), m_force(f), m_pmbus(p) {

    char fn[1024];

    // open device file
    sprintf(fn,"/dev/i2c-%d",m_bus);
    m_file = open(fn,O_RDWR);
    if(m_file < 0) {
        CERR("opening I2C at \"%s\"",fn);
        m_status = -1;
    }
    if(m_debug>=1) std::printf("INFO: opened file \"%s\"\n",fn);

    // set packet error checking if necessary
    if(m_pmbus == PEC_DEVICE) {
        if(ioctl(m_file,I2C_PEC,1) < 0) {
            CERR("setting packet error checking: %s",strerror(errno));
            m_status = -1;
        }
    }
}

//------------------------------------------------------------------------------

I2CDevice::~I2CDevice() {

    // reset packet error checking if necessary
    if(m_pmbus == PEC_DEVICE) {
        if(ioctl(m_file,I2C_PEC,0) < 0) {
            CERR("resetting packet error checking: %s",strerror(errno));
            m_status = -1;
        }
    }

    // close device file
    close(m_file);
}

//------------------------------------------------------------------------------
// READ I2C BYTE
//------------------------------------------------------------------------------

int I2CDevice::Read(const unsigned int addr, uint8_t& data) {

    CDBG("R%s: addr = 0x%02x (\"%s\")",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::BYTE].c_str(),addr,PEC_TYPE_NAME[m_pmbus].c_str());

    // set slave address
    if(ioctl(m_file,m_force?I2C_SLAVE_FORCE:I2C_SLAVE,addr) < 0) {
        CERR("setting address to 0x%02x: %s",addr,strerror(errno));
        m_status = -1;
        return(errno);
    }

    int rtnv(0);

    // read byte of data
    if((rtnv = i2c_smbus_read_byte(m_file)) < 0) {
        CERR("reading %s from address 0x%02x: %s",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::BYTE].c_str(),addr,strerror(errno));
        m_status = -1;
        return(errno);
    }
    data = rtnv;
    CDBG("R%s: data = 0x%02x",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::BYTE].c_str(),data);

    return(SUCCESS);
}

//------------------------------------------------------------------------------
// READ I2C DATA
//------------------------------------------------------------------------------

int I2CDevice::Read(const unsigned int addr, const unsigned int offs, uint8_t& data) {

    CDBG("R%s: addr = 0x%02x, offs = 0x%02x (\"%s\")",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::DATA].c_str(),addr,offs,PEC_TYPE_NAME[m_pmbus].c_str());

    // set slave address
    if(ioctl(m_file,m_force?I2C_SLAVE_FORCE:I2C_SLAVE,addr) < 0) {
        CERR("setting address to 0x%02x: %s",addr,strerror(errno));
        m_status = -1;
        return(errno);
    }

    int rtnv(0);

    // read data from offset
    if((rtnv = i2c_smbus_read_byte_data(m_file,offs)) < 0) {
        CERR("reading %s from address 0x%02x, offset 0x%02x: %s",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::DATA].c_str(),addr,offs,strerror(errno));
        m_status = -1;
        return(errno);
    }
    data = rtnv;
    CDBG("R%s: data = 0x%02x",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::DATA].c_str(),data);

    return(SUCCESS);
}

//------------------------------------------------------------------------------
// READ I2C WORD
//------------------------------------------------------------------------------

int I2CDevice::Read(const unsigned int addr, const unsigned int offs, uint16_t& data) {

    CDBG("R%s: addr = 0x%02x, offs = 0x%02x (\"%s\")",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::WORD].c_str(),addr,offs,PEC_TYPE_NAME[m_pmbus].c_str());

    // set slave address
    if(ioctl(m_file,m_force?I2C_SLAVE_FORCE:I2C_SLAVE,addr) < 0) {
        CERR("setting address to 0x%02x: %s",addr,strerror(errno));
        m_status = -1;
        return(errno);
    }

    int rtnv(0);

    // read data from offset
    if((rtnv = i2c_smbus_read_word_data(m_file,offs)) < 0) {
        CERR("reading %s from address 0x%02x, offset 0x%02x: %s",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::WORD].c_str(),addr,offs,strerror(errno));
        m_status = -1;
        return(errno);
    }
    data = rtnv;
    CDBG("R%s: data = 0x%04x",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::WORD].c_str(),data);

    return(SUCCESS);
}

//------------------------------------------------------------------------------
// READ I2C IBLK
//------------------------------------------------------------------------------

int I2CDevice::ReadIBlock(const unsigned int addr, const unsigned int offs, unsigned int& numb, uint8_t data[]) {

    CDBG("R%s: addr = 0x%02x, offs = 0x%02x, numb = %d (\"%s\")",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::IBLK].c_str(),addr,offs,numb,PEC_TYPE_NAME[m_pmbus].c_str());

    // set slave address
    if(ioctl(m_file,m_force?I2C_SLAVE_FORCE:I2C_SLAVE,addr) < 0) {
        CERR("setting address to 0x%02x: %s",addr,strerror(errno));
        m_status = -1;
        return(errno);
    }

    int rtnv(0), xnum(numb), lnum(0);

    // include CRC(PMBUS) if necessary
    if(m_pmbus == PEC_SOFT) xnum++;

    // read block of data (in chunks of 32 bytes maximum)
    while(lnum < (int)xnum) {
        int rst = xnum - lnum; if(rst > 32) rst = 32;
        if((rtnv = i2c_smbus_read_i2c_block_data(m_file,offs+lnum,rst,data+lnum)) <= 0) {
            CERR("reading %s from address 0x%02x, offset 0x%02x, number %d: %s",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::IBLK].c_str(),addr,offs+lnum,rst,strerror(errno));
            m_status = -1;
            return(errno);
        }
        lnum += rtnv;
    }

    // check CRC(PMBUS) if necessary
    if(m_pmbus == PEC_SOFT) {
        unsigned int crc = I2CInterface::crc(addr,offs,data,xnum,0);
        CDBG("R%s: numb = %d, CRC = %d",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::IBLK].c_str(),numb,crc);
        if(crc) {
            CERR("wrong CRC reading %s from address 0x%02x, offset 0x%02x, number %d: CRC = %d",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::IBLK].c_str(),addr,offs,xnum,crc);
            m_status = -1;
            return(-1);
        }
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------
// READ I2C SBLK
//------------------------------------------------------------------------------

int I2CDevice::ReadSBlock(const unsigned int addr, const unsigned int offs, unsigned int& numb, uint8_t data[]) {

    CDBG("R%s: addr = 0x%02x, offs = 0x%02x, numb = %d (\"%s\")",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::SBLK].c_str(),addr,offs,numb,PEC_TYPE_NAME[m_pmbus].c_str());

    // set slave address
    if(ioctl(m_file,m_force?I2C_SLAVE_FORCE:I2C_SLAVE,addr) < 0) {
        CERR("setting address to 0x%02x: %s",addr,strerror(errno));
        m_status = -1;
        return(errno);
    }

    int rtnv(0);

    // read block of data
    if((rtnv = i2c_smbus_read_block_data(m_file,offs,data)) < 0) {
        CERR("reading %s from address 0x%02x, offset 0x%02x, number %d: %s",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::SBLK].c_str(),addr,offs,numb,strerror(errno));
        m_status = -1;
        return(-1);
    }
    numb = rtnv;
    CDBG("R%s: numb = %d",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::SBLK].c_str(),numb);

    return(SUCCESS);
}

//------------------------------------------------------------------------------
// READ I2C PBLK
//------------------------------------------------------------------------------

int I2CDevice::ReadPBlock(const unsigned int addr, const unsigned int offs, unsigned int& numb, uint8_t data[]) {

    CDBG("R%s: addr = 0x%02x, offs = 0x%02x, numb = %d (\"%s\")",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::PBLK].c_str(),addr,offs,numb,PEC_TYPE_NAME[m_pmbus].c_str());

    char off[sizeof(char)*1] = {static_cast<char>(offs)};
    char dat[I2CInterface::MAX_BLOCK];
    int num(I2CInterface::DATA_SIZE);
    i2c_rdwr_ioctl_data cmd;
    i2c_msg msg[2];
    int rtnv(0);

    // include CRC(PMBUS) if necessary
    if(m_pmbus == PEC_SOFT) num++;

    msg[0].addr = static_cast<__u16>(addr);
    msg[0].flags = 0;
    msg[0].len = static_cast<short>(sizeof(unsigned char)*1);
    msg[0].buf = (msg_buf_type*)off;

    msg[1].addr = static_cast<__u16>(addr);
    msg[1].flags = I2C_M_RD;
    msg[1].len = static_cast<short>(sizeof(unsigned char)*num);
    msg[1].buf = (msg_buf_type*)dat;

    cmd.msgs = msg;
    cmd.nmsgs = 2;

    // read block of data
    if((rtnv = ioctl(m_file,I2C_RDWR,&cmd)) < 0) {
        CERR("reading %s from address 0x%02x, offset 0x%02x, number %d: %s",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::PBLK].c_str(),addr,offs,num,strerror(errno));
        m_status = -1;
        return(errno);
    }
    num = dat[0];
#ifdef DEBUG
    CDBG("R%s: rtnv = %d, data[0] = 0x%02x (\"%s\")",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::PBLK].c_str(),rtnv,dat[0],PEC_TYPE_NAME[m_pmbus].c_str());
    printData((uint8_t*)dat,(m_pmbus==PEC_SOFT)?num+2:num+1);
#endif  // DEBUG

    // check CRC(PMBUS) if necessary
    if(m_pmbus == PEC_SOFT) {
        unsigned int crc = I2CInterface::crc(addr,offs,(uint8_t*)dat,num+2,0);
        CDBG("R%s: numb = %d, CRC = %d",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::PBLK].c_str(),num,crc);
        if(crc) {
            CERR("wrong CRC reading %s from address 0x%02x, offset 0x%02x, number %d: CRC = %d",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::PBLK].c_str(),addr,offs,num,crc);
            m_status = -1;
            return(-1);
        }
    }

    // return data
    for(int i=0; i<num; i++) data[i] = dat[i+1];
    numb = num;

    return(SUCCESS);
}

//------------------------------------------------------------------------------
// READ I2C PROC
//------------------------------------------------------------------------------

int I2CDevice::ReadProcessCall(const unsigned int addr, const unsigned int offs, uint16_t& data) {

    CDBG("R%s: addr = 0x%02x, offs = 0x%02x, data = 0x%04x (\"%s\")",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::PROC].c_str(),addr,offs,data,PEC_TYPE_NAME[m_pmbus].c_str());

    // set slave address
    if(ioctl(m_file,m_force?I2C_SLAVE_FORCE:I2C_SLAVE,addr) < 0) {
        CERR("setting address to 0x%02x: %s",addr,strerror(errno));
        m_status = -1;
        return(errno);
    }

    int rtnv(0);

    // read process call
    CDBG("R%s: addr = 0x%2x, offs = %02x, data = 0x%04x",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::PROC].c_str(),addr,offs,data);
    if((rtnv = i2c_smbus_process_call(m_file,offs,data)) < 0) {
        CERR("reading %s from address 0x%02x, offset 0x%02x: %s",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::PROC].c_str(),addr,offs,strerror(errno));
        m_status = -1;
        return(-1);
    }
    data = rtnv;
    CDBG("R%s: data = 0x%04x",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::PROC].c_str(),data);

    return(SUCCESS);
}

//------------------------------------------------------------------------------
// READ I2C BLOCK PROC
//------------------------------------------------------------------------------

int I2CDevice::ReadBlockProcessCall(const unsigned int addr, const unsigned int offs, unsigned int& numb, uint8_t data[]) {

    CDBG("R%s: addr = 0x%02x, offs = 0x%02x, numb = %d (\"%s\")",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::PROC].c_str(),addr,offs,numb,PEC_TYPE_NAME[m_pmbus].c_str());

    // set slave address
    if(ioctl(m_file,m_force?I2C_SLAVE_FORCE:I2C_SLAVE,addr) < 0) {
        CERR("setting address to 0x%02x: %s",addr,strerror(errno));
        m_status = -1;
        return(errno);
    }

    int rtnv(0), xnum(numb);

    // write block - read block process call
    CDBG("R%s: addr = 0x%02x, offs = %02x, numb = %d, data[0] = 0x%02x",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::PROC].c_str(),addr,offs,numb,data[0]);
    if((rtnv = i2c_smbus_block_process_call(m_file,offs,xnum,data)) < 0) {
        CERR("reading BLOCK %s from address 0x%02x, offset 0x%02x, number %d: %s",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::PROC].c_str(),addr,offs,numb,strerror(errno));
        m_status = -1;
        return(-1);
    }
    numb = rtnv;
    CDBG("R%s: numb = %d, data[0] = 0x%02x",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::PROC].c_str(),numb,data[0]);

    return(SUCCESS);
}

//------------------------------------------------------------------------------
// READ I2C SMON
//------------------------------------------------------------------------------

int I2CDevice::ReadSystemMonitor(const unsigned int addr, const unsigned int offs, uint16_t& data) {

    i2c_rdwr_ioctl_data cmd;
    i2c_msg msg[2];
    char wbuf[4];
    char rbuf[2];
    int rtnv(0);

    wbuf[0] = 0x00;                             // data[7..0]  - unsued for read
    wbuf[1] = 0x00;                             // data[15..8] - unsued for read
    wbuf[2] = offs & 0xff;                      // offs[7..0]
    wbuf[3] = 0x04 | ((offs >> 8) & 0x03);      // READ COMMAND | offs[9..8]

    msg[0].addr = static_cast<__u16>(addr);
    msg[0].flags = 0;
    msg[0].len = static_cast<short>(4);
    msg[0].buf = (msg_buf_type*)wbuf;

    msg[1].addr = static_cast<__u16>(addr);
    msg[1].flags = I2C_M_RD;
    msg[1].len = static_cast<short>(2);
    msg[1].buf = (msg_buf_type*)rbuf;

    cmd.msgs = msg;
    cmd.nmsgs = 2;

    CDBG("R%s: wbuf = 0x%02x%02x%02x%02x",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::SMON].c_str(),wbuf[3],wbuf[2],wbuf[1],wbuf[0]);

    // write block - read block of data (system monitor)
    if((rtnv = ioctl(m_file,I2C_RDWR,&cmd)) < 0) {
        CERR("reading %s from address 0x%02x, offset 0x%02x: %s",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::SMON].c_str(),addr,offs,strerror(errno));
    }
    CDBG("R%s: rtnv = %d, rbuf = 0x%02x%02x",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::SMON].c_str(),rtnv,rbuf[1],rbuf[0]);

    data = rbuf[0] | (rbuf[1] << 8);

    return(SUCCESS);
}

//------------------------------------------------------------------------------
// READ I2C dispatcher function
//------------------------------------------------------------------------------

int I2CDevice::DataRead(const unsigned int addr, const unsigned int offs, unsigned int& numb, uint8_t data[], const TRANSFER_TYPE ttyp) {

    CDBG("R%s: addr = 0x%02x, offs = 0x%02x, numb = %d",TRANSFER_TYPE_NAME[ttyp].c_str(),addr,offs,numb);

    switch(ttyp) {
        case BYTE: {
                uint8_t offx;
                if((m_status = Read(addr,offx)) != SUCCESS) return(m_status);
                data[0] = offx;
                numb = 1;
            }
            break;
        case DATA: {
                uint8_t datc;
                if((m_status = Read(addr,offs,datc)) != SUCCESS) return(m_status);
                data[0] = datc;
                numb = 1;
            }
            break;
        case WORD: {
                uint16_t dats;
                if((m_status = Read(addr,offs,dats)) != SUCCESS) return(m_status);
                data[0] = dats & 0xff;
                data[1] = (dats >> 8) & 0xff;
                numb = 2;
            }
            break;
        case IBLK:
            if((m_status = ReadIBlock(addr,offs,numb,data)) != SUCCESS) return(m_status);
            break;
        case SBLK:
            if((m_status = const_cast<I2CDevice*>(this)->ReadSBlock(addr,offs,numb,data)) != SUCCESS) return(m_status);
            break;
        case PBLK:
            if((m_status = const_cast<I2CDevice*>(this)->ReadPBlock(addr,offs,numb,data)) != SUCCESS) return(m_status);
            break;
        case PROC: {
                uint16_t dats(data[0] | (data[1]<<8));
                if((m_status = const_cast<I2CDevice*>(this)->ReadProcessCall(addr,offs,dats)) != SUCCESS) return(m_status);
                data[0] = dats & 0xff;
                data[1] = (dats >> 8) & 0xff;
                numb = 2;
            }
            break;
        case SMON: {
                uint16_t dats;
                if((m_status = const_cast<I2CDevice*>(this)->ReadSystemMonitor(addr,offs,dats)) != SUCCESS) return(m_status);
                numb = 2;
                data[0] = dats & 0xff;
                data[1] = (dats >> 8) & 0xff;
            }
            break;
        default:
            CERR("I2C READ %s not implemented",TRANSFER_TYPE_NAME[ttyp].c_str());
            return(m_status = WRONGPAR);
            break;
    }

    return(m_status);
}

//------------------------------------------------------------------------------
// WRITE I2C BYTE
//------------------------------------------------------------------------------

int I2CDevice::Write(const unsigned int addr, const uint8_t data) {

    CDBG("W%s: addr = 0x%02x, data = 0x%02x (\"%s\")",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::BYTE].c_str(),addr,data,PEC_TYPE_NAME[m_pmbus].c_str());

    // set slave address
    if(ioctl(m_file,m_force?I2C_SLAVE_FORCE:I2C_SLAVE,addr) < 0) {
        CERR("setting address to 0x%02x: %s",addr,strerror(errno));
        m_status = -1;
        return(errno);
    }

    int rtnv(0);

    // write byte of data
    if((rtnv = i2c_smbus_write_byte(m_file,data)) < 0) {
        CERR("writing %s to address 0x%02x: %s",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::BYTE].c_str(),addr,strerror(errno));
        m_status = -1;
        return(errno);
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------
// WRITE I2C DATA
//------------------------------------------------------------------------------

int I2CDevice::Write(const unsigned int addr, const unsigned int offs, const uint8_t data) {

    CDBG("W%s: addr = 0x%02x, offs = 0x%02x, data = 0x%02x (\"%s\")",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::DATA].c_str(),addr,offs,data,PEC_TYPE_NAME[m_pmbus].c_str());

    // set slave address
    if(ioctl(m_file,m_force?I2C_SLAVE_FORCE:I2C_SLAVE,addr) < 0) {
        CERR("setting address to 0x%02x: %s",addr,strerror(errno));
        m_status = -1;
        return(errno);
    }

    int rtnv(0);

    // write data to offset
    if((rtnv = i2c_smbus_write_byte_data(m_file,offs,data)) < 0) {
        CERR("writing %s to address 0x%02x, offset 0x%02x: %s",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::DATA].c_str(),addr,offs,strerror(errno));
        m_status = -1;
        return(errno);
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------
// WRITE I2C WORD
//------------------------------------------------------------------------------

int I2CDevice::Write(const unsigned int addr, const unsigned int offs, const uint16_t data) {

    CDBG("W%s: addr = 0x%02x, offs = 0x%02x, data = 0x%04x (\"%s\")",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::WORD].c_str(),addr,offs,data,PEC_TYPE_NAME[m_pmbus].c_str());

    // set slave address
    if(ioctl(m_file,m_force?I2C_SLAVE_FORCE:I2C_SLAVE,addr) < 0) {
        CERR("setting address to 0x%02x: %s",addr,strerror(errno));
        m_status = -1;
        return(errno);
    }

    int rtnv(0);

    // write data to offset
    if((rtnv = i2c_smbus_write_word_data(m_file,(__u8)offs,(__u16)data)) < 0) {
        CERR("writing %s data to address 0x%02x, offset 0x%02x: %s",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::WORD].c_str(),addr,offs,strerror(errno));
        m_status = -1;
        return(errno);
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------
// WRITE I2C IBLK
//------------------------------------------------------------------------------

int I2CDevice::WriteIBlock(const unsigned int addr, const unsigned int offs, const unsigned int numb, const uint8_t data[]) {

    CDBG("W%s: addr = 0x%02x, offs = 0x%02x, numb = %d (\"%s\")",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::IBLK].c_str(),addr,offs,numb,PEC_TYPE_NAME[m_pmbus].c_str());

    // set slave address
    if(ioctl(m_file,m_force?I2C_SLAVE_FORCE:I2C_SLAVE,addr) < 0) {
        CERR("setting address to 0x%02x: %s",addr,strerror(errno));
        m_status = -1;
        return(errno);
    }

    int rtnv(0), xnum(numb), lnum(0);
    uint8_t* xdat = new uint8_t[numb+1];
    for(unsigned int i=0; i<numb; i++) xdat[i] = data[i];

    // add CRC(PMBUS) if necessary
    if(m_pmbus == PEC_SOFT) {
        unsigned int crc = I2CInterface::crc(addr,offs,data,xnum,0);
        xdat[xnum++] = crc;
    }
#ifdef DEBUG
    CDBG("W%s: numb = %d (\"%s\"):",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::IBLK].c_str(),xnum,PEC_TYPE_NAME[m_pmbus].c_str());
    for(auto i=0; i<xnum; ++i) std::printf("data[%d] = 0x%02x\n",i,xdat[i]);
#endif  // DEBUG

    // write block of data (in chunks of 32 bytes maximum)
    while(lnum < (int)xnum) {
        int rst = xnum - lnum; if(rst > 32) rst = 32;
        if((rtnv = i2c_smbus_write_i2c_block_data(m_file,offs+lnum,rst,xdat+lnum)) <= 0) {
            // 04-MAY-2021, R. Spiwoks, kludge for function returning 0, errno = 0 - change of return value ????
            if(rtnv == 0 and errno == 0) {
                rtnv = rst;
            }
            else {
                CERR("writing %s to address 0x%02x, offset 0x%02x, number %d: %s",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::IBLK].c_str(),addr,offs+lnum,rst,strerror(errno));
                delete [] xdat;
                m_status = -1;
                return(errno);
            }
        }
        lnum += rtnv;
    }

    delete [] xdat;

    return(SUCCESS);
}

//------------------------------------------------------------------------------
// WRITE I2C SBLK
//------------------------------------------------------------------------------

int I2CDevice::WriteSBlock(const unsigned int addr, const unsigned int offs, unsigned int& numb, const uint8_t data[]) {

    CDBG("W%s: addr = 0x%02x, offs = 0x%02x, numb = %d (\"%s\")",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::SBLK].c_str(),addr,numb,data,PEC_TYPE_NAME[m_pmbus].c_str());

    // set slave address
    if(ioctl(m_file,m_force?I2C_SLAVE_FORCE:I2C_SLAVE,addr) < 0) {
        CERR("setting address to 0x%02x: %s",addr,strerror(errno));
        m_status = -1;
        return(errno);
    }

    int rtnv(0), xnum(numb);
    uint8_t* xdat = new uint8_t[numb+1];
    for(unsigned int i=0; i<numb; i++) xdat[i] = data[i];

    // add CRC(PMBUS) if necessary
    if(m_pmbus == PEC_SOFT) {
        unsigned int crc = I2CInterface::crc(addr,offs,data,xnum,0);
        xdat[xnum++] = crc;
    }

    // write block of data
    if((rtnv = i2c_smbus_write_block_data(m_file,offs,xnum,xdat)) <= 0) {
        CERR("writing %s data to address 0x%02x, offset 0x%02x, number %d: %s",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::SBLK].c_str(),addr,offs,xnum,strerror(errno));
        delete [] xdat;
        m_status = -1;
        return(-1);
    }
    numb = rtnv;

    delete [] xdat;

    return(SUCCESS);
}

//------------------------------------------------------------------------------
// WRITE I2C PBLK
//------------------------------------------------------------------------------

int I2CDevice::WritePBlock(const unsigned int addr, const unsigned int offs, const unsigned int numb, const uint8_t data[]) {

    CDBG("W%s: addr = 0x%02x, offs = 0x%02x, numb = %d (\"%s\")",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::PBLK].c_str(),addr,numb,data,PEC_TYPE_NAME[m_pmbus].c_str());

    char off[sizeof(char)*1] = {static_cast<char>(offs)};
    char* dat = new char[I2CInterface::MAX_BLOCK];
    int num(numb);
    i2c_rdwr_ioctl_data cmd;
    i2c_msg msg[2];
    int rtnv(0);

    // fill data
    dat[0] = num;
    for(int i=0; i<num; i++) dat[i+1] = data[i];
    num++;

    // add CRC(PMBUS) if necessary
    if(m_pmbus == PEC_SOFT) {
        unsigned int crc = I2CInterface::crc(addr,offs,data,num,0);
        dat[num] = crc;
        num++;
    }

#ifdef DEBUG
    CDBG("W%s: numb = %d (\"%s\"):",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::PBLK].c_str(),num,PEC_TYPE_NAME[m_pmbus].c_str());
    printData((uint8_t*)dat,num);
#endif  // DEBUG

    msg[0].addr = static_cast<__u16>(addr);
    msg[0].flags = 0;
    msg[0].len = static_cast<short>(sizeof(unsigned char)*1);
    msg[0].buf = (msg_buf_type*)off;

    msg[1].addr = static_cast<__u16>(addr);
    msg[1].flags = 0;
    msg[1].len = static_cast<short>(sizeof(unsigned char)*num);
    msg[1].buf = (msg_buf_type*)dat;

    cmd.msgs = msg;
    cmd.nmsgs = 2;

    // write block of data
    if((rtnv = ioctl(m_file,I2C_RDWR,&cmd)) < 0) {
        CERR("writing %s to address 0x%02x, offset 0x%02x, number %d: %s",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::PBLK].c_str(),addr,offs,num,strerror(errno));
        m_status = -1;
        return(errno);
    }
    CDBG("W%s: rtnv = %d",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::PBLK].c_str(),rtnv);

    return(SUCCESS);
}

//------------------------------------------------------------------------------
// WRITE I2C SMON
//------------------------------------------------------------------------------

int I2CDevice::WriteSystemMonitor(const unsigned int addr, const unsigned int offs, const uint16_t data) {

    CDBG("W%s: addr = 0x%02x, offs = 0x%02x, data = 0x%04x (\"%s\")",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::SMON].c_str(),addr,offs,data,PEC_TYPE_NAME[m_pmbus].c_str());

    i2c_rdwr_ioctl_data cmd;
    i2c_msg msg[1];
    char wbuf[4];
    int rtnv(0);

    wbuf[0] = data & 0xff;                      // data[7..0]
    wbuf[1] = (data >> 8) & 0xff;               // data[15..8]
    wbuf[2] = offs & 0xff;                      // offs[7..0]
    wbuf[3] = 0x08 | ((offs >> 8) & 0x03);      // WRITE COMMAND | offs[9..8]

    msg[0].addr = static_cast<__u16>(addr);
    msg[0].flags = 0;
    msg[0].len = static_cast<short>(4);
    msg[0].buf = (msg_buf_type*)wbuf;

    cmd.msgs = msg;
    cmd.nmsgs = 1;

    CDBG("W%s: wbuf = 0x%02x%02x%02x%02x",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::SMON].c_str(),wbuf[3],wbuf[2],wbuf[1],wbuf[0]);

    // write block - read block (system monitor)
    if((rtnv = ioctl(m_file,I2C_RDWR,&cmd)) < 0) {
        CERR("writing %s to address 0x%02x, offset 0x%02x: %s",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::SMON].c_str(),addr,offs,strerror(errno));
    }
    CDBG("W%s: rtnv = %d",I2CInterface::TRANSFER_TYPE_NAME[I2CInterface::SMON].c_str(),rtnv);

    return(SUCCESS);
}

//------------------------------------------------------------------------------
// WRITE I2C dispatcher function
//------------------------------------------------------------------------------

int I2CDevice::DataWrite(const unsigned int addr, const unsigned int offs, unsigned int& numb, const uint8_t data[], const TRANSFER_TYPE ttyp) {

    CDBG("W%s: addr = 0x%02x, off = 0x%02x, numb = %d",TRANSFER_TYPE_NAME[ttyp].c_str(),addr,offs,numb);

    switch(ttyp) {
        case BYTE: {
                if((m_status = Write(addr,offs)) != SUCCESS) return(m_status);
            }
            break;
        case DATA: {
                uint8_t datc(data[0]);
                if((m_status = Write(addr,offs,datc)) != SUCCESS) return(m_status);
            }
            break;
        case WORD: {
                uint16_t dats(data[0] | (data[1]<<8));
                if((m_status = Write(addr,offs,dats)) != SUCCESS) return(m_status);
            }
            break;
        case IBLK:
            if((m_status = WriteIBlock(addr,offs,numb,data)) != SUCCESS) return(m_status);
            break;
        case SBLK:
            if((m_status = const_cast<I2CDevice*>(this)->WriteSBlock(addr,offs,numb,data)) != SUCCESS) return(m_status);
            break;
        case PBLK:
            if((m_status = const_cast<I2CDevice*>(this)->WritePBlock(addr,offs,numb,data)) != SUCCESS) return(m_status);
            break;
        case PROC: {
                // for WRITE PROC use WRITE WORD
                uint16_t dats(data[0] | (data[1]<<8));
                if((m_status = Write(addr,offs,dats)) != SUCCESS) return(m_status);
            }
            break;
        case SMON: {
                uint16_t dats(data[0] | (data[1]<<8));
                if((m_status = const_cast<I2CDevice*>(this)->WriteSystemMonitor(addr,offs,dats)) != SUCCESS) return(m_status);
            }
            break;
        default:
            CERR("I2C WRITE %s not implemented",TRANSFER_TYPE_NAME[ttyp].c_str());
            return(m_status = WRONGPAR);
            break;
    }

    return(m_status);
}

//------------------------------------------------------------------------------

int I2CDevice::PrintFunctions() {

    unsigned long funcs;

    // read I2Cdevice driver functions
    if(ioctl(m_file,I2C_FUNCS,&funcs) < 0) {
        CERR("reading driver functions: %s",strerror(errno));
        m_status = -1;
        return(errno);
    }

    std::printf("I2C[bus %d] functionality:\n",m_bus);
    if(funcs&I2C_FUNC_I2C)                    std::cout << "  I2C_FUNC_I2C" << std::endl;
    if(funcs&I2C_FUNC_10BIT_ADDR)             std::cout << "  I2C_FUNC_10BIT_ADDR" << std::endl;
    if(funcs&I2C_FUNC_PROTOCOL_MANGLING)      std::cout << "  I2C_FUNC_PROTOCOL_MANGLING" << std::endl;
    if(funcs&I2C_FUNC_SMBUS_PEC)              std::cout << "  I2C_FUNC_SMBUS_PEC" << std::endl;
    if(funcs&I2C_FUNC_SMBUS_BLOCK_PROC_CALL)  std::cout << "  I2C_FUNC_SMBUS_BLOCK_PROC_CALL" << std::endl;
    if(funcs&I2C_FUNC_SMBUS_QUICK)            std::cout << "  I2C_FUNC_SMBUS_QUICK" << std::endl;
    if(funcs&I2C_FUNC_SMBUS_READ_BYTE)        std::cout << "  I2C_FUNC_SMBUS_READ_BYTE" << std::endl;
    if(funcs&I2C_FUNC_SMBUS_WRITE_BYTE)       std::cout << "  I2C_FUNC_SMBUS_WRITE_BYTE" << std::endl;
    if(funcs&I2C_FUNC_SMBUS_READ_BYTE_DATA)   std::cout << "  I2C_FUNC_SMBUS_READ_BYTE_DATA" << std::endl;
    if(funcs&I2C_FUNC_SMBUS_WRITE_BYTE_DATA)  std::cout << "  I2C_FUNC_SMBUS_WRITE_BYTE_DATA" << std::endl;
    if(funcs&I2C_FUNC_SMBUS_READ_WORD_DATA)   std::cout << "  I2C_FUNC_SMBUS_READ_WORD_DATA" << std::endl;
    if(funcs&I2C_FUNC_SMBUS_WRITE_WORD_DATA)  std::cout << "  I2C_FUNC_SMBUS_WRITE_WORD_DATA" << std::endl;
    if(funcs&I2C_FUNC_SMBUS_PROC_CALL)        std::cout << "  I2C_FUNC_SMBUS_PROC_CALL" << std::endl;
    if(funcs&I2C_FUNC_SMBUS_READ_BLOCK_DATA)  std::cout << "  I2C_FUNC_SMBUS_READ_BLOCK_DATA" << std::endl;
    if(funcs&I2C_FUNC_SMBUS_WRITE_BLOCK_DATA) std::cout << "  I2C_FUNC_SMBUS_WRITE_BLOCK_DATA" << std::endl;
    if(funcs&I2C_FUNC_SMBUS_READ_I2C_BLOCK)   std::cout << "  I2C_FUNC_SMBUS_READ_I2C_BLOCK" << std::endl;
    if(funcs&I2C_FUNC_SMBUS_WRITE_I2C_BLOCK)  std::cout << "  I2C_FUNC_SMBUS_WRITE_I2C_BLOCK" << std::endl;

    return(SUCCESS);
}

// -----------------------------------------------------------------------------

void I2CDevice::printData(const uint8_t data[], const unsigned int size) {

    unsigned int i(0);

    for(i=0; i<size; i++) {
        if(i%16 == 0) std::printf("data[%3d] = ",i);
        std::printf(" 0x%02x",data[i]);
        if(i%16 == 15) std::printf("\n");
    }
    if(i%16 != 0) std::printf("\n");
}

// -----------------------------------------------------------------------------

void I2CDevice::lockBus() {
#ifdef I2C_LOCK
    lockf(m_file, F_LOCK, 0); 
#endif
    // do nothing
}

// -----------------------------------------------------------------------------

void I2CDevice::unlockBus() {
#ifdef I2C_LOCK
    lockf(m_file, F_ULOCK, 0); 
#endif
    // do nothing
}
