//*****************************************************************************
// file: I2CNetwork.cc
// desc: class for I2C network
// auth: 26-MAR-2015 R. Spiwoks
// auth: 26-MAR-2015 R. Spiwoks
// modf: 11-JUL-2017 R. Spiwoks, re-organization of I2C code
//*****************************************************************************

// to enable debugging
//#define DEBUG

#include "I2C/I2CNetwork.h"

#ifdef __ZYNQ__
#include "zynq_log/Logging.h"
#include "zynq_util/Utility.h"
#define XUTIL Utility
using namespace ZYNQ;
using namespace LVL1;
#else
#include "I2C/I2CCommon.h"
#include "RCDUtilities/RCDUtilities.h"
#define XUTIL I2CCommon
using namespace LVL1;
#endif

#include <sstream>
#include <cmath>
#include <climits>
#include <I2C/I2CDevice.h>

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

const std::string I2CNetwork::DISPLAY_TYPE_NAME[DISPLAY_TYPE_NUMBER] = {
    "WORD", "FLOAT", "STRING", "MEMORY"
};

const std::string I2CNetwork::BUS_TYPE_NAME[BUS_TYPE_NUMBER] = {
    "LTC2990", "LTC2991", "LTC3880", "LTM4676", "RXMPODLO", "RXMPODH1", "SFP", "SFPEXTD",
    "ZC706", "MUCTPI0", "MUCTPI0_V2V3", "MUCTPI1","ZCU102", "ALTI", "ALTI2", "LTI_DEMO", "LTI_FMC"
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

I2CNetwork::I2CNode::I2CNode() : m_name(), m_bus(0), m_dev(0), m_qty(0) {

    // nothing to be done
}

//------------------------------------------------------------------------------

I2CNetwork::I2CNode::I2CNode(const I2CNode& n) : m_name(n.m_name), m_bus(n.m_bus), m_dev(n.m_dev), m_qty(n.m_qty) {

    // nothing to be done
}

//------------------------------------------------------------------------------

I2CNetwork::I2CNode& I2CNetwork::I2CNode::operator=(const I2CNode& n) {

    if(this != &n) {
        m_name = n.m_name;
        m_bus = n.m_bus;
        m_dev = n.m_dev;
        m_qty = n.m_qty;
    }

    return(*this);
}

//------------------------------------------------------------------------------

I2CNetwork::I2CNode::~I2CNode() {

    // nothing to be done
}

//------------------------------------------------------------------------------

void I2CNetwork::I2CNode::dump() const {

    std::printf("I2C bus/device/quantity \"%s\" = %d/%d/%d\n",m_name.c_str(),m_bus,m_dev,m_qty);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

I2CNetwork::I2CQuantity::I2CQuantity(const I2CInterface::TRANSFER_TYPE ttyp, const std::string& n, const unsigned int o, const unsigned int s)

  : m_transfer_type(ttyp), m_display_type(WORD), m_name(n), m_offset(o), m_number(s), m_readable(true), m_writable(false), m_little_endian(true),
    m_mantissa_signed(false), m_exponent_signed(false), m_exponent_width(0), m_exponent_offset(0), m_convert_factor(1.0), m_convert_offset(0.0), m_convert_mask(0xffffffff) {

    // nothing to be done
}

//------------------------------------------------------------------------------

I2CNetwork::I2CQuantity::~I2CQuantity() {

    // nothing to be done
}

//------------------------------------------------------------------------------

int I2CNetwork::I2CQuantity::readableI2CQuantity(const bool rdb) {

    m_readable = rdb;

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::I2CQuantity::writableI2CQuantity(const bool wtb) {

    m_writable = wtb;

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::I2CQuantity::displayI2CQuantity(const DISPLAY_TYPE dtyp) {

    m_display_type = dtyp;

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::I2CQuantity::endianI2CQuantity(const bool ltl) {

    m_little_endian = ltl;

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::I2CQuantity::signedI2CQuantity(const bool man, const bool exp) {

    m_mantissa_signed = man;
    m_exponent_signed = exp;

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::I2CQuantity::exponentI2CQuantity(const unsigned int wdt, const int off) {

    m_exponent_width = wdt;
    m_exponent_offset = off;

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::I2CQuantity::convertI2CQuantity(const float fct, const float off) {

    m_convert_factor = fct;
    m_convert_offset = off;

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::I2CQuantity::maskI2CQuantity(const unsigned int msk) {

    m_convert_mask = msk;

    return(SUCCESS);
}

//------------------------------------------------------------------------------

unsigned int I2CNetwork::I2CQuantity::bytes2word(const uint8_t data[]) {

    unsigned int word(0), shft(0), indx;

    for(unsigned int i=0; i<m_number; i++) {
        indx = m_little_endian ? (i) : (m_number - 1 - i);
        word |= data[indx] << shft;
        shft += CHAR_BIT;
    }

    return(word);
}

//------------------------------------------------------------------------------

void I2CNetwork::I2CQuantity::word2bytes(const unsigned int word, const unsigned int numb, uint8_t data[]) {

    unsigned int shft(0), indx;

    for(unsigned int i=0; i<m_number; i++) {
        indx = m_little_endian ? (i) : (m_number - 1 - i);
        data[indx] = (word >> shft) & I2CInterface::DATA_MASK;
        shft += CHAR_BIT;
    }
}

//------------------------------------------------------------------------------

float I2CNetwork::I2CQuantity::word2float(const unsigned int data) {

    unsigned int msb(CHAR_BIT*m_number - 1);
    int exponent(0), mantissa;

    // mask data before conversion
    unsigned int datx = data & m_convert_mask;

    // calculate exponent
    if(m_exponent_width) exponent = word2number(word2range(datx,msb-m_exponent_width+1,msb),m_exponent_width-1,m_exponent_signed);

    // calculate mantissa
    mantissa = word2number(word2range(datx,0,msb-m_exponent_width),msb-m_exponent_width,m_mantissa_signed);

    // convert to number
    float number = static_cast<float>(mantissa) * std::pow(2.0,static_cast<float>(exponent+m_exponent_offset)) * m_convert_factor + m_convert_offset;

    return(number);
}

//------------------------------------------------------------------------------

unsigned int I2CNetwork::I2CQuantity::float2word(const float number) {

    double xval, xman;
    int xexp(0);
    unsigned int data, iman, iexp(0), nexp, lman, lexp(0);

    // convert from number
    xval = (number - m_convert_offset) / m_convert_factor;

    // calculate normalized mantissa and exponent
    xman = std::frexp(xval,&xexp);
    xexp -= m_exponent_offset;
    CDBG("number = %15.6f, value = %15.6f, mantissa = %15.6lf, exponent = %d",number,xval,xman,xexp);

    // calculate range of mantissa and exponent
    iman = CHAR_BIT*m_number - m_exponent_width - (m_mantissa_signed?1:0);
    if(m_exponent_width) iexp = m_exponent_width - (m_exponent_signed?1:0);
    CDBG("iman = %d%s, iexp = %d%s",iman,m_mantissa_signed?"S":"U",iexp,m_exponent_signed?"S":"U");

    // calculate de-normalized mantissa and exponent
    nexp = std::min(iman,(unsigned int)xexp+((1U<<iexp) - 1));
    CDBG("nexp = %d",nexp);
    lman = (unsigned int)(std::ldexp(xman,nexp));
    if(m_exponent_width) lexp = (unsigned int)(xexp - nexp);
    CDBG("lman = 0x%08x (%d), lexp = 0x%08x (%d)",lman,lman,lexp,lexp);

    // calculate word
    data = range2word(lman,0,CHAR_BIT*m_number-m_exponent_width-1);
    if(m_exponent_width) data |= range2word(lexp,CHAR_BIT*m_number-m_exponent_width,CHAR_BIT*m_number-1);
    CDBG("data = 0x%08x, revert = %f",data,word2float(data));

    return(data);
}

//------------------------------------------------------------------------------

int I2CNetwork::I2CQuantity::word2number(const unsigned int data, const unsigned int msb, const bool sgn) {

    if(sgn && (word2range(data,msb,msb) == 1)) return(data - (1U << (msb+1)));

    return(data);
}

//------------------------------------------------------------------------------

int I2CNetwork::I2CQuantity::number2word(const unsigned int data, const unsigned int msb, const bool sgn) {

    if(sgn && (word2range(data,msb,msb) == 1)) return(data + (1U << (msb+1)));

    return(data);
}

//------------------------------------------------------------------------------

int I2CNetwork::I2CQuantity::word2range(const unsigned int data, const unsigned int low, const unsigned int high) {

    unsigned int mask = (1U << (high-low+1)) - 1;

    return((data >> low) & mask);
}

//------------------------------------------------------------------------------

unsigned int I2CNetwork::I2CQuantity::range2word(const unsigned int data, const unsigned int low, const unsigned int high) {

    unsigned int mask = (1U << (high-low+1)) - 1;

    return((data & mask) << low);
}

//------------------------------------------------------------------------------

std::string I2CNetwork::I2CQuantity::rw() const {

    if(m_readable) {
        if(m_writable) return("RW"); else return("R-");
    }
    else {
        if(m_writable) return("-W"); else return("--");
    }
}

//------------------------------------------------------------------------------

void I2CNetwork::I2CQuantity::dump(const unsigned int qty) const {

    std::printf("  - I2CQuantity[%2d](%-4s)(%-6s): %-25s",qty,I2CInterface::TRANSFER_TYPE_NAME[m_transfer_type].c_str(),DISPLAY_TYPE_NAME[m_display_type].c_str(),("\""+m_name+"\",").c_str());
    std::printf(" off = 0x%02x, num = %4d, r/w= %s, end = %s, sgn = %s,",m_offset,m_number,rw().c_str(),m_little_endian?"L":"B",m_mantissa_signed?"S":"U");
    std::printf(" exp(sgn = %s, wdt = %d, off = %3d)",m_exponent_signed?"S":"U",m_exponent_width,m_exponent_offset);
    std::printf(" fct = %f, off = %f\n",m_convert_factor,m_convert_offset);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

I2CNetwork::I2CDevice::I2CDevice(const std::string& n, const unsigned int a)

  : m_name(n), m_address(a), m_switch_address(-1), m_switch_select(0), m_page_flag(false), m_page_offset(0), m_page_select(0), m_pmbus(false) {

}

//------------------------------------------------------------------------------

I2CNetwork::I2CDevice::~I2CDevice() {

    // delete I2CQuantities
    for(auto iqty : m_quantities) {
        if(iqty) delete iqty;
    }
}

//------------------------------------------------------------------------------

int I2CNetwork::I2CDevice::typeI2CDevice(const std::string& typ) {

    m_type = typ;

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::I2CDevice::switchI2CDevice(const unsigned int add, const unsigned int sel) {

    m_switch_address = add;
    m_switch_select = sel;

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::I2CDevice::pageI2CDevice(const unsigned int off, const unsigned int sel) {

    m_page_flag = true;
    m_page_offset = off;
    m_page_select = sel;

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::I2CDevice::pmbusI2CDevice() {

    m_pmbus = true;

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::I2CDevice::addI2CQuantity(I2CQuantity* qty, unsigned int& rslt) {

    rslt = m_quantities.size();
    m_quantities.push_back(qty);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

void I2CNetwork::I2CDevice::dump(const unsigned int dev) const {

    std::printf("- I2CDevice[%d](%-9s): %-20s",dev,("\""+m_type+"\"").c_str(),("\""+m_name+"\",").c_str());
    if(m_switch_address <= I2CInterface::DATA_MASK) {
        std::printf(" switch(add = 0x%02x, sel = 0x%02x),",m_switch_address,m_switch_select);
    }
    else {
        std::printf(" switch(NONE),                  ");
    }
    if(m_page_flag) {
        std::printf(" page(off = 0x%02x, sel = 0x%02x),",m_page_offset,m_page_select);
    }
    else {
        std::printf(" page(NONE),                  ");
    }
    std::printf(" address = 0x%02x%s\n",m_address,m_pmbus?", CRC(PMBUS)":"");

    // loop over quantities
    for(unsigned int qty=0; qty<m_quantities.size(); qty++) {
        m_quantities[qty]->dump(qty);
    }
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

I2CNetwork::I2CBus::I2CBus(const std::string& n, I2CInterface* i) : m_name(n), m_i2c(i) {

    // nothing to be done
}

//------------------------------------------------------------------------------

I2CNetwork::I2CBus::~I2CBus() {

    // delete I2CDevices
    for(auto idev : m_devices) {
        if(idev) delete idev;
    }
}

//------------------------------------------------------------------------------

int I2CNetwork::I2CBus::addI2CDevice(I2CDevice* dev, unsigned int& rslt) {

    rslt = m_devices.size();
    m_devices.push_back(dev);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

void I2CNetwork::I2CBus::dump(const unsigned int bus) const {

    std::printf("I2CBus[%d]: %-29s\n",bus,("\""+m_name+"\",").c_str());

    // loop over devices
    for(unsigned int dev=0; dev<m_devices.size(); dev++) {
        m_devices[dev]->dump(dev);
    }
}

//------------------------------------------------------------------------------

void I2CNetwork::I2CBus::clear() {

    // delete I2CDevices
    for(auto idev : m_devices) {
        if(idev) delete idev;
    }
    m_devices.clear();
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

I2CNetwork::I2CNetwork() {

    // nothing to be done
}

//------------------------------------------------------------------------------

I2CNetwork::~I2CNetwork() {

    // delete I2CBusses
    for(auto ibus : m_busses) {
        if(ibus) delete ibus;
    }
}

//------------------------------------------------------------------------------

int I2CNetwork::addI2CBus(const std::string& name, I2CInterface* i2c, unsigned int& rslt) {

    I2CBus* bus = new I2CBus(name,i2c);
    rslt = m_busses.size();
    m_busses.push_back(bus);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::addI2CDevice(const unsigned int bus, const std::string& name, const unsigned int addr, unsigned int& rslt) {

    // reset return value
    rslt = -1;

    // check I2C bus number
    if(bus >= m_busses.size()) {
        CERR("bus number %d not in allowed range [0..%d]",bus,m_busses.size());
        return(WRONGPAR);
    }

    I2CDevice* dev = new I2CDevice(name,addr);
    m_busses[bus]->addI2CDevice(dev,rslt);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::typeI2CDevice(const unsigned int bus, const unsigned int dev, const std::string& typ) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set I2C device type name
    m_busses[bus]->getI2CDevice(dev)->typeI2CDevice(typ);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::switchI2CDevice(const unsigned int bus, const unsigned int dev, const unsigned int add, const unsigned int sel) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set I2C device switch
    m_busses[bus]->getI2CDevice(dev)->switchI2CDevice(add,sel);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::pageI2CDevice(const unsigned int bus, const unsigned int dev, const unsigned int off, const unsigned int sel) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set I2C device page
    m_busses[bus]->getI2CDevice(dev)->pageI2CDevice(off,sel);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::pmbusI2CDevice(const unsigned int bus, const unsigned int dev) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set I2C device pmbus
    m_busses[bus]->getI2CDevice(dev)->pmbusI2CDevice();

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::initI2CBus(const unsigned int bus, const BUS_TYPE typ, const unsigned int daddr) {

    unsigned int dev, qty;

    switch(typ) {
    case BUS_LTC2990:
        addI2CDevice(bus,"LTC2990",daddr,dev);
        ltc2990(bus,dev);
        break;
    case BUS_LTC2991:
        addI2CDevice(bus,"LTC2991",daddr,dev);
        ltc2991(bus,dev);
        break;
    case BUS_LTC3880:
        addI2CDevice(bus,"LTC3880",daddr,dev);
        ltm4676(bus,dev);
        typeI2CDevice(bus,dev,"LTC3880");
        break;
    case BUS_LTM4676:
        addI2CDevice(bus,"LTM4676",daddr,dev);
        ltm4676(bus,dev);
        break;
    case BUS_RXMPODLO:
        addI2CDevice(bus,"RXMPODLO",daddr,dev);
        avagoMiniPodRXLO(bus,dev);
        break;
    case BUS_RXMPODUP1:
        addI2CDevice(bus,"RXMPODUP1",daddr,dev);
        avagoMiniPodRXUP1(bus,dev);
        break;
    case BUS_SFP:
        addI2CDevice(bus,"SFP",daddr,dev);
        avagoSFP(bus,dev);
        break;
    case BUS_SFPEXTD:
        addI2CDevice(bus,"SFPEXTD",daddr,dev);
        avagoSFPExtended(bus,dev);
        break;
    case BUS_ZC706:
        addI2CDevice(bus,"SI570",0x5d,dev);
        switchI2CDevice(bus,dev,0x74,0x01);
        si570(bus,dev);
        addI2CDevice(bus,"ADV7511",0x39,dev);
        switchI2CDevice(bus,dev,0x74,0x02);
        adv7511(bus,dev);
        addI2CDevice(bus,"M24C08",0x54,dev);
        switchI2CDevice(bus,dev,0x74,0x04);
        m24c08(bus,dev);
        addI2CDevice(bus,"DDR3SODIMM",0x50,dev);
        switchI2CDevice(bus,dev,0x74,0x08);
        ddr3sodimm(bus,dev);
        addI2CDevice(bus,"RTC8564JE",0x51,dev);
        switchI2CDevice(bus,dev,0x74,0x10);
        rtc8564je(bus,dev);
        addI2CDevice(bus,"SI5324",0x68,dev);
        switchI2CDevice(bus,dev,0x74,0x10);
        si5324(bus,dev);
        addI2CDevice(bus,"UCD90120A",0x65,dev);
        switchI2CDevice(bus,dev,0x74,0x80);
        ucd90120a(bus,dev);
        break;
    case BUS_MUCTPI0:
    case BUS_MUCTPI0_V2V3:
        // switch 0x70, segment 0
        addI2CDevice(bus,"MPOD_TX1_LO",0x2c,dev);
        switchI2CDevice(bus,dev,0x70,0x01);
        avagoMiniPodTXLO(bus,dev);
        addI2CDevice(bus,"MPOD_TX1_UP1",0x2c,dev);
        switchI2CDevice(bus,dev,0x70,0x01);
        avagoMiniPodTXUP1(bus,dev);
        addI2CDevice(bus,"MPOD_TX2_LO",0x2d,dev);
        switchI2CDevice(bus,dev,0x70,0x01);
        avagoMiniPodTXLO(bus,dev);
        addI2CDevice(bus,"MPOD_TX2_UP1",0x2d,dev);
        switchI2CDevice(bus,dev,0x70,0x01);
        avagoMiniPodTXUP1(bus,dev);
        addI2CDevice(bus,"MPOD_RX06_LO",0x30,dev);
        switchI2CDevice(bus,dev,0x70,0x01);
        avagoMiniPodRXLO(bus,dev);
        addI2CDevice(bus,"MPOD_RX06_UP1",0x30,dev);
        switchI2CDevice(bus,dev,0x70,0x01);
        avagoMiniPodRXUP1(bus,dev);
        addI2CDevice(bus,"MPOD_RX07_LO",0x31,dev);
        switchI2CDevice(bus,dev,0x70,0x01);
        avagoMiniPodRXLO(bus,dev);
        addI2CDevice(bus,"MPOD_RX07_UP1",0x31,dev);
        switchI2CDevice(bus,dev,0x70,0x01);
        avagoMiniPodRXUP1(bus,dev);
        addI2CDevice(bus,"MPOD_RX08_LO",0x32,dev);
        switchI2CDevice(bus,dev,0x70,0x01);
        avagoMiniPodRXLO(bus,dev);
        addI2CDevice(bus,"MPOD_RX08_UP1",0x32,dev);
        switchI2CDevice(bus,dev,0x70,0x01);
        avagoMiniPodRXUP1(bus,dev);
        addI2CDevice(bus,"MPOD_RX09_LO",0x33,dev);
        switchI2CDevice(bus,dev,0x70,0x01);
        avagoMiniPodRXLO(bus,dev);
        addI2CDevice(bus,"MPOD_RX09_UP1",0x33,dev);
        switchI2CDevice(bus,dev,0x70,0x01);
        avagoMiniPodRXUP1(bus,dev);
        // switch 0x70, segment 1
        addI2CDevice(bus,"MPOD_RX01_LO",0x34,dev);
        switchI2CDevice(bus,dev,0x70,0x02);
        avagoMiniPodRXLO(bus,dev);
        addI2CDevice(bus,"MPOD_RX01_UP1",0x34,dev);
        switchI2CDevice(bus,dev,0x70,0x02);
        avagoMiniPodRXUP1(bus,dev);
        addI2CDevice(bus,"MPOD_RX02_LO",0x30,dev);
        switchI2CDevice(bus,dev,0x70,0x02);
        avagoMiniPodRXLO(bus,dev);
        addI2CDevice(bus,"MPOD_RX02_UP1",0x30,dev);
        switchI2CDevice(bus,dev,0x70,0x02);
        avagoMiniPodRXUP1(bus,dev);
        addI2CDevice(bus,"MPOD_RX03_LO",0x31,dev);
        switchI2CDevice(bus,dev,0x70,0x02);
        avagoMiniPodRXLO(bus,dev);
        addI2CDevice(bus,"MPOD_RX03_UP1",0x31,dev);
        switchI2CDevice(bus,dev,0x70,0x02);
        avagoMiniPodRXUP1(bus,dev);
        addI2CDevice(bus,"MPOD_RX04_LO",0x32,dev);
        switchI2CDevice(bus,dev,0x70,0x02);
        avagoMiniPodRXLO(bus,dev);
        addI2CDevice(bus,"MPOD_RX04_UP1",0x32,dev);
        switchI2CDevice(bus,dev,0x70,0x02);
        avagoMiniPodRXUP1(bus,dev);
        addI2CDevice(bus,"MPOD_RX05_LO",0x33,dev);
        switchI2CDevice(bus,dev,0x70,0x02);
        avagoMiniPodRXLO(bus,dev);
        addI2CDevice(bus,"MPOD_RX05_UP1",0x33,dev);
        switchI2CDevice(bus,dev,0x70,0x02);
        avagoMiniPodRXUP1(bus,dev);
        // switch 0x70, segment 2
        addI2CDevice(bus,"MPOD_TX3_LO",0x2c,dev);
        switchI2CDevice(bus,dev,0x70,0x04);
        avagoMiniPodTXLO(bus,dev);
        addI2CDevice(bus,"MPOD_TX3_UP1",0x2c,dev);
        switchI2CDevice(bus,dev,0x70,0x04);
        avagoMiniPodTXUP1(bus,dev);
        addI2CDevice(bus,"MPOD_TX4_LO",0x2d,dev);
        switchI2CDevice(bus,dev,0x70,0x04);
        avagoMiniPodTXLO(bus,dev);
        addI2CDevice(bus,"MPOD_TX4_UP1",0x2d,dev);
        switchI2CDevice(bus,dev,0x70,0x04);
        avagoMiniPodTXUP1(bus,dev);
        addI2CDevice(bus,"MPOD_RX15_LO",0x30,dev);
        switchI2CDevice(bus,dev,0x70,0x04);
        avagoMiniPodRXLO(bus,dev);
        addI2CDevice(bus,"MPOD_RX15_UP1",0x30,dev);
        switchI2CDevice(bus,dev,0x70,0x04);
        avagoMiniPodRXUP1(bus,dev);
        addI2CDevice(bus,"MPOD_RX16_LO",0x31,dev);
        switchI2CDevice(bus,dev,0x70,0x04);
        avagoMiniPodRXLO(bus,dev);
        addI2CDevice(bus,"MPOD_RX16_UP1",0x31,dev);
        switchI2CDevice(bus,dev,0x70,0x04);
        avagoMiniPodRXUP1(bus,dev);
        addI2CDevice(bus,"MPOD_RX17_LO",0x32,dev);
        switchI2CDevice(bus,dev,0x70,0x04);
        avagoMiniPodRXLO(bus,dev);
        addI2CDevice(bus,"MPOD_RX17_UP1",0x32,dev);
        switchI2CDevice(bus,dev,0x70,0x04);
        avagoMiniPodRXUP1(bus,dev);
        addI2CDevice(bus,"MPOD_RX18_LO",0x33,dev);
        switchI2CDevice(bus,dev,0x70,0x04);
        avagoMiniPodRXLO(bus,dev);
        addI2CDevice(bus,"MPOD_RX18_UP1",0x33,dev);
        switchI2CDevice(bus,dev,0x70,0x04);
        avagoMiniPodRXUP1(bus,dev);
        // switch 0x70, segment 3
        addI2CDevice(bus,"MPOD_RX10_LO",0x34,dev);
        switchI2CDevice(bus,dev,0x70,0x08);
        avagoMiniPodRXLO(bus,dev);
        addI2CDevice(bus,"MPOD_RX10_UP1",0x34,dev);
        switchI2CDevice(bus,dev,0x70,0x08);
        avagoMiniPodRXUP1(bus,dev);
        addI2CDevice(bus,"MPOD_RX11_LO",0x30,dev);
        switchI2CDevice(bus,dev,0x70,0x08);
        avagoMiniPodRXLO(bus,dev);
        addI2CDevice(bus,"MPOD_RX11_UP1",0x30,dev);
        switchI2CDevice(bus,dev,0x70,0x08);
        avagoMiniPodRXUP1(bus,dev);
        addI2CDevice(bus,"MPOD_RX12_LO",0x31,dev);
        switchI2CDevice(bus,dev,0x70,0x08);
        avagoMiniPodRXLO(bus,dev);
        addI2CDevice(bus,"MPOD_RX12_UP1",0x31,dev);
        switchI2CDevice(bus,dev,0x70,0x08);
        avagoMiniPodRXUP1(bus,dev);
        addI2CDevice(bus,"MPOD_RX13_LO",0x32,dev);
        switchI2CDevice(bus,dev,0x70,0x08);
        avagoMiniPodRXLO(bus,dev);
        addI2CDevice(bus,"MPOD_RX13_UP1",0x32,dev);
        switchI2CDevice(bus,dev,0x70,0x08);
        avagoMiniPodRXUP1(bus,dev);
        addI2CDevice(bus,"MPOD_RX14_LO",0x33,dev);
        switchI2CDevice(bus,dev,0x70,0x08);
        avagoMiniPodRXLO(bus,dev);
        addI2CDevice(bus,"MPOD_RX14_UP1",0x33,dev);
        switchI2CDevice(bus,dev,0x70,0x08);
        avagoMiniPodRXUP1(bus,dev);
        // switch 0x71, segment 0
        addI2CDevice(bus,"QSFP_LO",0x50,dev);
        switchI2CDevice(bus,dev,0x71,0x01);
        avagoQSFPPlusLO(bus,dev);
        addI2CDevice(bus,"QSFP_H3",0x50,dev);
        switchI2CDevice(bus,dev,0x71,0x01);
        avagoQSFPPlusUP3(bus,dev);
        // switch 0x71, segment 1
        addI2CDevice(bus,"MPOD_TX5_LO",0x2d,dev);
        switchI2CDevice(bus,dev,0x71,0x02);
        avagoMiniPodTXLO(bus,dev);
        addI2CDevice(bus,"MPOD_TX5_UP1",0x2d,dev);
        switchI2CDevice(bus,dev,0x71,0x02);
        avagoMiniPodTXUP1(bus,dev);
        addI2CDevice(bus,"TTC_CLK",0x40,dev);
        switchI2CDevice(bus,dev,0x71,0x02);
        adn2814(bus,dev);
        addI2CDevice(bus,"TTC_SFP",0x50,dev);
        switchI2CDevice(bus,dev,0x71,0x02);
        avagoSFP(bus,dev);
        addI2CDevice(bus,"TTC_SFPEXTD",0x51,dev);
        switchI2CDevice(bus,dev,0x71,0x02);
        avagoSFPExtended(bus,dev);
        // switch 0x71, segment 2
        addI2CDevice(bus,"LTC2991",0x4e,dev);
        switchI2CDevice(bus,dev,0x71,0x04);
        ltc2991(bus,dev);
        // switch 0x71, segment 3
        addI2CDevice(bus,"ETH_SFP",0x50,dev);
        switchI2CDevice(bus,dev,0x71,0x08);
        avagoSFP(bus,dev);
        addI2CDevice(bus,"ETH_SFPPHY",0x56,dev);
        switchI2CDevice(bus,dev,0x71,0x08);
        abcu5740(bus,dev);
        addI2CDevice(bus,"EEPROM",0x57,dev);
        switchI2CDevice(bus,dev,0x71,0x08);
        addI2CQuantity(bus,dev,I2CInterface::IBLK,"MEMORY",0,256,qty);
        displayI2CQuantity(bus,dev,qty,I2CNetwork::MEMORY);
        addI2CQuantity(bus,dev,I2CInterface::IBLK,"MAC_ADDRESS",0x7a,6,qty);
        displayI2CQuantity(bus,dev,qty,I2CNetwork::MEMORY);
        addI2CQuantity(bus,dev,I2CInterface::IBLK,"SERIAL_NUMBER",0xfa,6,qty);
        displayI2CQuantity(bus,dev,qty,I2CNetwork::MEMORY);
        if(typ == BUS_MUCTPI0) {
            // switch 0x72, segment 0
            addI2CDevice(bus,"MSPA_FPGA",0x43,dev);
            switchI2CDevice(bus,dev,0x72,0x01);
            ultrascaleSysmon(bus,dev);
            // switch 0x72, segment 1
            addI2CDevice(bus,"MSPC_FPGA",0x43,dev);
            switchI2CDevice(bus,dev,0x72,0x02);
            ultrascaleSysmon(bus,dev);
        }
        else if(typ == BUS_MUCTPI0_V2V3) {
            // switch 0x72, segment 0
            addI2CDevice(bus,"MSPA_FPGA",0x3b,dev);
            switchI2CDevice(bus,dev,0x72,0x01);
            ultrascaleSysmon(bus,dev);
            // switch 0x72, segment 1
            addI2CDevice(bus,"MSPC_FPGA",0x3b,dev);
            switchI2CDevice(bus,dev,0x72,0x02);
            ultrascaleSysmon(bus,dev);
        }
        // switch 0x72, segment 2
        addI2CDevice(bus,"TRP_FPGA",0x43,dev);
        switchI2CDevice(bus,dev,0x72,0x04);
        ultrascaleSysmon(bus,dev);
        break;
    case BUS_MUCTPI1:
        addI2CDevice(bus,"MSPA_MGTAVCC_CH0",0x49,dev);
        ltm4676(bus,dev);
        typeI2CDevice(bus,dev,"LTM4676A");
        addI2CDevice(bus,"MSPA_MGTAVCC_CH1",0x49,dev);
        ltm4676_p1(bus,dev);
        typeI2CDevice(bus,dev,"LTM4676A");
        addI2CDevice(bus,"MSPA_MGTAVTT_CH0",0x48,dev);
        ltm4676(bus,dev);
        typeI2CDevice(bus,dev,"LTM4676A");
        addI2CDevice(bus,"MSPA_MGTAVTT_CH1",0x48,dev);
        ltm4676_p1(bus,dev);
        typeI2CDevice(bus,dev,"LTM4676A");
        addI2CDevice(bus,"MSPA_VCCINT_CH0",0x47,dev);
        ltm4676(bus,dev);
        typeI2CDevice(bus,dev,"LTM4677");
        addI2CDevice(bus,"MSPA_VCCINT_CH1",0x47,dev);
        ltm4676_p1(bus,dev);
        typeI2CDevice(bus,dev,"LTM4677");
        addI2CDevice(bus,"MSPC_MGTAVCC_CH0",0x4c,dev);
        ltm4676(bus,dev);
        typeI2CDevice(bus,dev,"LTM4676A");
        addI2CDevice(bus,"MSPC_MGTAVCC_CH1",0x4c,dev);
        ltm4676_p1(bus,dev);
        typeI2CDevice(bus,dev,"LTM4676A");
        addI2CDevice(bus,"MSPC_MGTAVTT_CH0",0x4b,dev);
        ltm4676(bus,dev);
        typeI2CDevice(bus,dev,"LTM4676A");
        addI2CDevice(bus,"MSPC_MGTAVTT_CH1",0x4b,dev);
        ltm4676_p1(bus,dev);
        typeI2CDevice(bus,dev,"LTM4676A");
        addI2CDevice(bus,"MSPC_VCCINT_CH0",0x4a,dev);
        ltm4676(bus,dev);
        typeI2CDevice(bus,dev,"LTM4677");
        addI2CDevice(bus,"MSPC_VCCINT_CH1",0x4a,dev);
        ltm4676_p1(bus,dev);
        typeI2CDevice(bus,dev,"LTM4677");
        addI2CDevice(bus,"TRP_VCCINT_CH0",0x44,dev);
        ltm4676(bus,dev);
        typeI2CDevice(bus,dev,"LTM4677");
        addI2CDevice(bus,"TRP_VCCINT_CH1",0x44,dev);
        ltm4676_p1(bus,dev);
        typeI2CDevice(bus,dev,"LTM4677");
        addI2CDevice(bus,"TRP_MGTAVCC_CH0",0x45,dev);
        ltm4676(bus,dev);
        typeI2CDevice(bus,dev,"LTM4677");
        addI2CDevice(bus,"TRP_MGTAVTT_CH1",0x45,dev);
        ltm4676_p1(bus,dev);
        typeI2CDevice(bus,dev,"LTM4677");
        addI2CDevice(bus,"DDR_VCCIO_CH0",0x43,dev);
        ltm4676(bus,dev);
        typeI2CDevice(bus,dev,"LTM4676A");
        addI2CDevice(bus,"ZYNQ_VCCINT_CH1",0x43,dev);
        ltm4676_p1(bus,dev);
        typeI2CDevice(bus,dev,"LTM4676A");
        addI2CDevice(bus,"1.8V_VCCIO_CH0",0x42,dev);
        ltm4676(bus,dev);
        typeI2CDevice(bus,dev,"LTM4676A");
        addI2CDevice(bus,"1.8V_VCCIO_CH1",0x42,dev);
        ltm4676_p1(bus,dev);
        typeI2CDevice(bus,dev,"LTM4676A");
        addI2CDevice(bus,"2.5V_VCCIO_CH0",0x41,dev);
        ltm4676(bus,dev);
        typeI2CDevice(bus,dev,"LTM4676A");
        addI2CDevice(bus,"2.5V_VCCIO_CH1",0x41,dev);
        ltm4676_p1(bus,dev);
        typeI2CDevice(bus,dev,"LTM4676A");
        addI2CDevice(bus,"3.3V_VCCIO_CH0",0x40,dev);
        ltm4676(bus,dev);
        typeI2CDevice(bus,dev,"LTM4676A");
        addI2CDevice(bus,"3.3V_VCCIO_CH1",0x40,dev);
        ltm4676_p1(bus,dev);
        typeI2CDevice(bus,dev,"LTM4676A");
        break;
    case BUS_ALTI: // i2c switch: 0b1110A210, A210 grounded -> 0b1110000 = 0x70
        // switch 0x70, segment 0
        addI2CDevice(bus,"CTP_IN",0x0c,dev); // 000 1100: CTP MDR_IN/MDR_Receive/AD5305 DAC
        switchI2CDevice(bus,dev,0x70,0x01);
        ad5305(bus,dev);
        addI2CDevice(bus,"ALTI_IN",0x0d,dev); // 000 1101: ALTI MDR_IN/MDR_Receive/AD5305 DAC
        switchI2CDevice(bus,dev,0x70,0x01);
        ad5305(bus,dev);
        addI2CDevice(bus,"CLK_GEN_CLEANER_P0",0x68,dev); // 110 1000: CLK_GENERATOR_CLEANER/SI5344B JITTER CLEANER, page 0
        switchI2CDevice(bus,dev,0x70,0x01);
        si53xx_p0(bus,dev);
        addI2CDevice(bus,"CLK_GEN_CLEANER_P2",0x68,dev); // 110 1000: CLK_GENERATOR_CLEANER/SI5344B JITTER CLEANER, page 2
        switchI2CDevice(bus,dev,0x70,0x01);
        si53xx_p2(bus,dev);
        // switch 0x70, segment 1
        addI2CDevice(bus,"BC_SWITCH",0x58,dev); // 101 1000: BC_SOURCE_SWITCH_I2C/DS10CP154
        switchI2CDevice(bus,dev,0x70,0x02);
        ds10cp154(bus,dev);
        // 101 01XX: TRIGGER_TYPE_SWITCH_I2C/High/4xDS10CP154
        addI2CDevice(bus,"TTYP4_SWITCH",0x54,dev);
        switchI2CDevice(bus,dev,0x70,0x02);
        ds10cp154(bus,dev);
        addI2CDevice(bus,"TTYP5_SWITCH",0x55,dev);
        switchI2CDevice(bus,dev,0x70,0x02);
        ds10cp154(bus,dev);
        addI2CDevice(bus,"TTYP6_SWITCH",0x56,dev);
        switchI2CDevice(bus,dev,0x70,0x02);
        ds10cp154(bus,dev);
        addI2CDevice(bus,"TTYP7_SWITCH",0x57,dev);
        switchI2CDevice(bus,dev,0x70,0x02);
        ds10cp154(bus,dev);
        // 101 00XX: TRIGGER_TYPE_SWITCH_I2C/Low/4xDS10CP154
        addI2CDevice(bus,"TTYP0_SWITCH",0x50,dev);
        switchI2CDevice(bus,dev,0x70,0x02);
        ds10cp154(bus,dev);
        addI2CDevice(bus,"TTYP1_SWITCH",0x51,dev);
        switchI2CDevice(bus,dev,0x70,0x02);
        ds10cp154(bus,dev);
        addI2CDevice(bus,"TTYP2_SWITCH",0x52,dev);
        switchI2CDevice(bus,dev,0x70,0x02);
        ds10cp154(bus,dev);
        addI2CDevice(bus,"TTYP3_SWITCH",0x53,dev);
        switchI2CDevice(bus,dev,0x70,0x02);
        ds10cp154(bus,dev);
        // switch 0x70, segment 2
        // 101 00XX: BGO_SOURCE_SWITCH_I2C/4xDS10CP154
        addI2CDevice(bus,"BGO0_SWITCH",0x50,dev);
        switchI2CDevice(bus,dev,0x70,0x04);
        ds10cp154(bus,dev);
        addI2CDevice(bus,"BGO1_SWITCH",0x51,dev);
        switchI2CDevice(bus,dev,0x70,0x04);
        ds10cp154(bus,dev);
        addI2CDevice(bus,"BGO2_SWITCH",0x52,dev);
        switchI2CDevice(bus,dev,0x70,0x04);
        ds10cp154(bus,dev);
        addI2CDevice(bus,"BGO3_SWITCH",0x53,dev);
        switchI2CDevice(bus,dev,0x70,0x04);
        ds10cp154(bus,dev);
        addI2CDevice(bus,"ORB_SWITCH",0x58,dev); // 101 1000: ORBIT_SOURCE_SWITCH_I2C/DS10CP154
        switchI2CDevice(bus,dev,0x70,0x04);
        ds10cp154(bus,dev);
        // 101 01XX: TRIGGER_SOURCE_SWITCH_I2C/High/4xDS10CP154
        addI2CDevice(bus,"L1A_SWITCH",0x54,dev);
        switchI2CDevice(bus,dev,0x70,0x04);
        ds10cp154(bus,dev);
        addI2CDevice(bus,"TTR1_SWITCH",0x55,dev);
        switchI2CDevice(bus,dev,0x70,0x04);
        ds10cp154(bus,dev);
        addI2CDevice(bus,"TTR2_SWITCH",0x56,dev);
        switchI2CDevice(bus,dev,0x70,0x04);
        ds10cp154(bus,dev);
        addI2CDevice(bus,"TTR3_SWITCH",0x57,dev);
        switchI2CDevice(bus,dev,0x70,0x04);
        ds10cp154(bus,dev);
        // switch 0x70, segment 3
        addI2CDevice(bus,"OPTO_SFP_TX_RX_CLK",0x40,dev); // 100 0000: OPTO_SFP_TX_RX/CLOCK_DATA_RECOVER/ADN2814
        switchI2CDevice(bus,dev,0x70,0x08);
        adn2814(bus,dev);
        addI2CDevice(bus,"OPTO_SFP_TX_RX",0x50,dev); // 101 000X: FINISAR FTLF1323
        switchI2CDevice(bus,dev,0x70,0x08);
        ftlf1323(bus,dev);
        addI2CDevice(bus,"OPTO_SFP_TX_RX_EXT",0x51,dev); // 101 000X: FINISAR FTLF1323
        switchI2CDevice(bus,dev,0x70,0x08);
        ftlf1323Extended(bus,dev);
        addI2CDevice(bus,"MAX1617",0x18,dev); // 001 1000: BOARD_POWER/MAX1617
        switchI2CDevice(bus,dev,0x70,0x08);
        max1617(bus,dev);
        addI2CDevice(bus,"LTC2991",0x48,dev); // 100 1000: BOARD_POWER/LTC2991
        switchI2CDevice(bus,dev,0x70,0x08);
        ltc2991(bus,dev);
        break;
    case BUS_ALTI2: // i2c switch: 0b1110A210, A210 grounded -> 0b1110000 = 0x70
        // switch 0x70, segment 0
        addI2CDevice(bus,"CTP_IN",0x0c,dev); // 000 1100: CTP MDR_IN/MDR_Receive/AD5305 DAC
        switchI2CDevice(bus,dev,0x70,0x01);
        ad5305(bus,dev);
        addI2CDevice(bus,"ALTI_IN",0x0d,dev); // 000 1101: ALTI MDR_IN/MDR_Receive/AD5305 DAC
        switchI2CDevice(bus,dev,0x70,0x01);
        ad5305(bus,dev);
        addI2CDevice(bus,"CLK_GEN_CLEANER_P0",0x68,dev); // 110 1000: CLK_GENERATOR_CLEANER/SI5344B JITTER CLEANER, page 0
        switchI2CDevice(bus,dev,0x70,0x01);
        si53xx_p0(bus,dev);
        addI2CDevice(bus,"CLK_GEN_CLEANER_P2",0x68,dev); // 110 1000: CLK_GENERATOR_CLEANER/SI5344B JITTER CLEANER, page 2
        switchI2CDevice(bus,dev,0x70,0x01);
        si53xx_p2(bus,dev);
        addI2CDevice(bus,"OPTO_SFP_TX_RX_CLK",0x40,dev); // 100 0000: OPTO_SFP_TX_RX/CLOCK_DATA_RECOVER/ADN2814
        switchI2CDevice(bus,dev,0x70,0x01);
        adn2814(bus,dev);
        addI2CDevice(bus,"OPTO_SFP_TX_RX",0x50,dev); // 101 000X: FINISAR FTLF1323
        switchI2CDevice(bus,dev,0x70,0x01);
        ftlf1323(bus,dev);
        addI2CDevice(bus,"OPTO_SFP_TX_RX_EXT",0x51,dev); // 101 000X: FINISAR FTLF1323
        switchI2CDevice(bus,dev,0x70,0x01);
        ftlf1323Extended(bus,dev);
        addI2CDevice(bus,"MAX6695",0x18,dev); // 001 1000: BOARD_POWER/MAX6695
        switchI2CDevice(bus,dev,0x70,0x01);
        max6695(bus,dev);
        addI2CDevice(bus,"LTC2991",0x48,dev); // 100 1000: BOARD_POWER/LTC2991
        switchI2CDevice(bus,dev,0x70,0x01);
        ltc2991(bus,dev);
        // switch 0x70, segment 1
        addI2CDevice(bus,"BC_SWITCH",0x58,dev); // 101 1000: BC_SOURCE_SWITCH_I2C/DS10CP154
        switchI2CDevice(bus,dev,0x70,0x02);
        ds10cp154(bus,dev);
        // 101 01XX: TRIGGER_TYPE_SWITCH_I2C/High/4xDS10CP154
        addI2CDevice(bus,"TTYP4_SWITCH",0x54,dev);
        switchI2CDevice(bus,dev,0x70,0x02);
        ds10cp154(bus,dev);
        addI2CDevice(bus,"TTYP5_SWITCH",0x55,dev);
        switchI2CDevice(bus,dev,0x70,0x02);
        ds10cp154(bus,dev);
        addI2CDevice(bus,"TTYP6_SWITCH",0x56,dev);
        switchI2CDevice(bus,dev,0x70,0x02);
        ds10cp154(bus,dev);
        addI2CDevice(bus,"TTYP7_SWITCH",0x57,dev);
        switchI2CDevice(bus,dev,0x70,0x02);
        ds10cp154(bus,dev);
        // 101 00XX: TRIGGER_TYPE_SWITCH_I2C/Low/4xDS10CP154
        addI2CDevice(bus,"TTYP0_SWITCH",0x50,dev);
        switchI2CDevice(bus,dev,0x70,0x02);
        ds10cp154(bus,dev);
        addI2CDevice(bus,"TTYP1_SWITCH",0x51,dev);
        switchI2CDevice(bus,dev,0x70,0x02);
        ds10cp154(bus,dev);
        addI2CDevice(bus,"TTYP2_SWITCH",0x52,dev);
        switchI2CDevice(bus,dev,0x70,0x02);
        ds10cp154(bus,dev);
        addI2CDevice(bus,"TTYP3_SWITCH",0x53,dev);
        switchI2CDevice(bus,dev,0x70,0x02);
        ds10cp154(bus,dev);
        // switch 0x70, segment 2
        // 101 00XX: BGO_SOURCE_SWITCH_I2C/4xDS10CP154
        addI2CDevice(bus,"BGO0_SWITCH",0x50,dev);
        switchI2CDevice(bus,dev,0x70,0x04);
        ds10cp154(bus,dev);
        addI2CDevice(bus,"BGO1_SWITCH",0x51,dev);
        switchI2CDevice(bus,dev,0x70,0x04);
        ds10cp154(bus,dev);
        addI2CDevice(bus,"BGO2_SWITCH",0x52,dev);
        switchI2CDevice(bus,dev,0x70,0x04);
        ds10cp154(bus,dev);
        addI2CDevice(bus,"BGO3_SWITCH",0x53,dev);
        switchI2CDevice(bus,dev,0x70,0x04);
        ds10cp154(bus,dev);
        addI2CDevice(bus,"ORB_SWITCH",0x58,dev); // 101 1000: ORBIT_SOURCE_SWITCH_I2C/DS10CP154
        switchI2CDevice(bus,dev,0x70,0x04);
        ds10cp154(bus,dev);
        // 101 01XX: TRIGGER_SOURCE_SWITCH_I2C/High/4xDS10CP154
        addI2CDevice(bus,"L1A_SWITCH",0x54,dev);
        switchI2CDevice(bus,dev,0x70,0x04);
        ds10cp154(bus,dev);
        addI2CDevice(bus,"TTR1_SWITCH",0x55,dev);
        switchI2CDevice(bus,dev,0x70,0x04);
        ds10cp154(bus,dev);
        addI2CDevice(bus,"TTR2_SWITCH",0x56,dev);
        switchI2CDevice(bus,dev,0x70,0x04);
        ds10cp154(bus,dev);
        addI2CDevice(bus,"TTR3_SWITCH",0x57,dev);
        switchI2CDevice(bus,dev,0x70,0x04);
        ds10cp154(bus,dev);
        // switch 0x70, segment 3
        // switch 0x70, segment 4
        // switch 0x70, segment 5
        // switch 0x70, segment 6
        // switch 0x70, segment 7
        break; 
    case BUS_ZCU102:
        addI2CDevice(bus,"MAX15301_VCCINT",0x13,dev);
        switchI2CDevice(bus,dev,0x75,0x06);
        max15303(bus,dev);
        addI2CDevice(bus,"MAX15303_VCCBRAM",0x14,dev);
        switchI2CDevice(bus,dev,0x75,0x06);
        max15303(bus,dev);
        addI2CDevice(bus,"MAX15303_VCCAUX",0x15,dev);
        switchI2CDevice(bus,dev,0x75,0x06);
        max15303(bus,dev);
        addI2CDevice(bus,"MAX15303_VCC1V2",0x16,dev);
        switchI2CDevice(bus,dev,0x75,0x06);
        max15303(bus,dev);
        addI2CDevice(bus,"MAX15303_VCC3V3",0x17,dev);
        switchI2CDevice(bus,dev,0x75,0x06);
        max15303(bus,dev);
        addI2CDevice(bus,"MAX15301_VADJ_FMC",0x18,dev);
        switchI2CDevice(bus,dev,0x75,0x06);
        max15303(bus,dev);
        break;
    case BUS_LTI_DEMO:
        // TTC PON FMC
        addI2CDevice(bus,"EEPROM",0x50,dev);
        switchI2CDevice(bus,dev,0x74,0x01);
        /* addI2CQuantity(bus,dev,I2CInterface::IBLK,"MEMORY",0,8000,qty); */
        /* displayI2CQuantity(bus,dev,qty,I2CNetwork::MEMORY); */
        addI2CDevice(bus,"SI570",0x55,dev);
        switchI2CDevice(bus,dev,0x74,0x02);
        si570(bus,dev);
        addI2CDevice(bus,"CLK_CONTROLLER",0x38,dev);
        switchI2CDevice(bus,dev,0x74,0x04);
        typeI2CDevice(bus,dev,"PCA8574A");
        addI2CDevice(bus,"CLK_GEN_CLEANER_P0",0x68,dev); // 110 1000: CLK_GENERATOR_CLEANER/SI5344B JITTER CLEANER, page 0
        switchI2CDevice(bus,dev,0x74,0x08);
        si53xx_p0(bus,dev);
        addI2CDevice(bus,"CLK_GEN_CLEANER_P2",0x68,dev); // 110 1000: CLK_GENERATOR_CLEANER/SI5344B JITTER CLEANER, page 2
        switchI2CDevice(bus,dev,0x74,0x08);
        si53xx_p2(bus,dev);
        addI2CDevice(bus,"LED_CONTROLLER",0x38,dev);
        switchI2CDevice(bus,dev,0x74,0x10);
        typeI2CDevice(bus,dev,"PCA8574A");
        addI2CDevice(bus,"SFP",0x50,dev);
        switchI2CDevice(bus,dev,0x74,0x20);
        ftlf1323(bus,dev);
        break;
    case BUS_LTI_FMC:
        // LTI FMC
        addI2CDevice(bus,"SI5345B_P0",0x68,dev); // jitter cleaner
        switchI2CDevice(bus,dev,0x74,0x01);
        si53xx_p0(bus,dev);
        addI2CDevice(bus,"SI5345B_P2",0x68,dev);
        switchI2CDevice(bus,dev,0x74,0x01);
        si53xx_p2(bus,dev);
        addI2CDevice(bus,"SI5394A_P0",0x69,dev); // clock generator
        switchI2CDevice(bus,dev,0x74,0x02);
        si53xx_p0(bus,dev);
        addI2CDevice(bus,"SI5394A_P2",0x69,dev);
        switchI2CDevice(bus,dev,0x74,0x02);
        si53xx_p2(bus,dev);
        addI2CDevice(bus,"Firefly_TX_LO",0x50,dev);
        switchI2CDevice(bus,dev,0x74,0x04);
        samtecFireFlyTXLO(bus,dev);
        addI2CDevice(bus,"Firefly_TX_UP1",0x50,dev);
        switchI2CDevice(bus,dev,0x74,0x04);
        samtecFireFlyTXUP1(bus,dev);
        addI2CDevice(bus,"Firefly_RX_LO",0x54,dev);
        switchI2CDevice(bus,dev,0x74,0x04);
        samtecFireFlyRXLO(bus,dev);
        addI2CDevice(bus,"Firefly_RX_UP1",0x54,dev);
        switchI2CDevice(bus,dev,0x74,0x04);
        samtecFireFlyRXUP1(bus,dev);
        addI2CDevice(bus,"EEPROM",0x57,dev);
        switchI2CDevice(bus,dev,0x74,0x08);
        addI2CQuantity(bus,dev,I2CInterface::IBLK,"MEMORY",0,256,qty);
        displayI2CQuantity(bus,dev,qty,I2CNetwork::MEMORY);
        addI2CQuantity(bus,dev,I2CInterface::IBLK,"SERIAL_NUMBER",0xfa,6,qty);
        displayI2CQuantity(bus,dev,qty,I2CNetwork::MEMORY);
        break;
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::clearI2CBus(const unsigned int bus) {

    // check I2C bus number
    if(bus >= m_busses.size()) {
        CERR("bus number %d not in allowed range [0..%d]",bus,m_busses.size());
        return(WRONGPAR);
    }

    m_busses[bus]->clear();

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::max15303(const unsigned int bus, const unsigned int dev) {
    
    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"MAX15303");

    unsigned int qty;
    pmbusI2CDevice(bus,dev);

//------------------------------------------------------------------------------
// based on doc/MAX15303.pdf p.22
//------------------------------------------------------------------------------
    addI2CQuantity(bus,dev,I2CInterface::DATA,"PAGE",0x00,1,qty);               writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::BYTE,"CLEAR_FAULTS",0x03,0,qty);       readableI2CQuantity(bus,dev,qty,false); writableI2CQuantity(bus,dev,qty,true);
    //addI2CQuantity(bus,dev,I2CInterface::PROC,"SMBALERT_MASK",0x1b,2,qty);      writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"VOUT_MODE",0x20,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS_BYTE",0x78,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"STATUS_WORD",0x79,2,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS_VOUT",0x7a,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS_IOUT",0x7b,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS_INPUT",0x7c,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS_TEMPERATURE",0x7d,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS_CML",0x7e,1,qty);
    //addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS_MFR_SPECIFIC",0x80,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"READ_VIN",0x88,2,qty);           displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    //addI2CQuantity(bus,dev,I2CInterface::WORD,"READ_IIN",0x89,2,qty);           displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"READ_VOUT",0x8b,2,qty);          displayI2CQuantity(bus,dev,qty,FLOAT); convertI2CQuantity(bus,dev,qty,1.0/4096.0); //TODO check coefficient
    addI2CQuantity(bus,dev,I2CInterface::WORD,"READ_IOUT",0x8c,2,qty);          displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"READ_TEMPERATURE_1",0x8d,2,qty); displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"READ_TEMPERATURE_2",0x8e,2,qty); displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"READ_DUTY_CYCLE",0x94,2,qty);    displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    //addI2CQuantity(bus,dev,I2CInterface::WORD,"READ_POUT",0x96,2,qty);          displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"READ_FREQUENCY",0x95,2,qty);     displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    addI2CQuantity(bus,dev,I2CInterface::SBLK,"MFR_ID",0x99,8,qty);             displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::SBLK,"MFR_MODEL",0x9a,13,qty);          displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::SBLK,"MFR_REVISION",0x9b,7,qty);          displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::SBLK,"MFR_LOCATION",0x9c,8,qty);          displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::SBLK,"MFR_DATE",0x9d,6,qty);          displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::SBLK,"MFR_SERIAL",0x9e,13,qty);         displayI2CQuantity(bus,dev,qty,STRING);
    /*addI2CQuantity(bus,dev,I2CInterface::WORD,"MFR_VOUT_PEAK",0xdd,2,qty);      displayI2CQuantity(bus,dev,qty,FLOAT); convertI2CQuantity(bus,dev,qty,1.0/4096.0); // TODO check coeffcient
    addI2CQuantity(bus,dev,I2CInterface::WORD,"MFR_VIN_PEAK",0xde,2,qty);       displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"MFR_TEMPERATURE_1_PEAK",0xdf,2,qty); displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    addI2CQuantity(bus,dev,I2CInterface::BYTE,"MFR_CLEAR_PEAKS",0xe3,0,qty);    readableI2CQuantity(bus,dev,qty,false); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"MFR_PADS",0xe5,2,qty);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"MFR_IIN_OFFSET",0xe9,2,qty);     displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    addI2CQuantity(bus,dev,I2CInterface::BYTE,"MFR_FAULT_LOG_STORE",0xea,0,qty); readableI2CQuantity(bus,dev,qty,false); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::BYTE,"MFR_FAULT_LOG_CLEAR",0xec,0,qty); readableI2CQuantity(bus,dev,qty,false); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"MFR_READ_IIN",0xed,2,qty);        displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    addI2CQuantity(bus,dev,I2CInterface::PBLK,"MFR_FAULT_LOG",0xee,147,qty);     displayI2CQuantity(bus,dev,qty,MEMORY);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"MFR_COMMON",0xef,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"MFR_TEMPERATURE_2_PEAK",0xf4,2,qty); displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);*/
    addI2CQuantity(bus,dev,I2CInterface::SBLK,"IC_DEVICE_ID",0xad,8,qty);       displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::SBLK,"IC_DEVICE_REV",0xae,2,qty);      displayI2CQuantity(bus,dev,qty,STRING);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::avagoSFP(const unsigned int bus, const unsigned int dev) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"SFP");

    unsigned int qty;

//------------------------------------------------------------------------------
// based on doc/AFBR-57R5APZ_20160510175041137.pdf p.14
//------------------------------------------------------------------------------
    addI2CQuantity(bus,dev,I2CInterface::DATA,"SFP_DEVICE",0,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"SFP_FUNCTION",1,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"LC_OPT_CONN",3,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"INT_DISTANCE",7,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"SHORTWAVE_LASER",8,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"MULTIMODE_MEDIA",9,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"FC_PI_SPEED",10,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"8B_10B_ENCODE",11,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"LINK_DIST_150M",16,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"LINK_DIST_70M",17,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"VENDOR_NAME",20,16,qty);         displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"VENDOR_OUI",37,3,qty);           displayI2CQuantity(bus,dev,qty,WORD); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"VENDOR_PART_NAME",40,20,qty);    displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"LASER_WAVELENGTH",60,2,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"CHECKSUM_0_62",63,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"HW_SFP_STATUS",65,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"VENDOR_SERIAL_NUMBER",68,16,qty); displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"VENDOR_DATE_CODE",84,8,qty);     displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"DIGITAL_DIAGNOSTICS",92,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"SW_SFP_STATUS",93,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"SFF_COMPLIANCE",94,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"CHECKSUM_64_94",95,1,qty);
    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::avagoSFPExtended(const unsigned int bus, const unsigned int dev) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"SFPxtd");

    unsigned int qty;

//------------------------------------------------------------------------------
// based on doc/AFBR-57R5APZ_20160510175041137.pdf p.15f
//------------------------------------------------------------------------------
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TEMP_ALARM_HI",0,2,qty);         floatI2CQuantity(bus,dev,qty,true,1.0/256.0); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TEMP_ALARM_LO",2,2,qty);         floatI2CQuantity(bus,dev,qty,true,1.0/256.0); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TEMP_WARNING_HI",4,2,qty);       floatI2CQuantity(bus,dev,qty,true,1.0/256.0); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TEMP_WARNING_LO",6,2,qty);       floatI2CQuantity(bus,dev,qty,true,1.0/256.0); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC_ALARM_HI",8,2,qty);          floatI2CQuantity(bus,dev,qty,false,0.0001);   endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC_ALARM_LO",10,2,qty);         floatI2CQuantity(bus,dev,qty,false,0.0001);   endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC_WARNING_HI",12,2,qty);       floatI2CQuantity(bus,dev,qty,false,0.0001);   endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC_WARNING_LO",14,2,qty);       floatI2CQuantity(bus,dev,qty,false,0.0001);   endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXBIAS_ALARM_HI",16,2,qty);      floatI2CQuantity(bus,dev,qty,false,0.000002); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXBIAS_ALARM_LO",18,2,qty);      floatI2CQuantity(bus,dev,qty,false,0.000002); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXBIAS_WARNING_HI",20,2,qty);    floatI2CQuantity(bus,dev,qty,false,0.000002); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXBIAS_WARNING_LO",22,2,qty);    floatI2CQuantity(bus,dev,qty,false,0.000002); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXPOWER_ALARM_HI",24,2,qty);     floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXPOWER_ALARM_LO",26,2,qty);     floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXPOWER_WARNING_HI",28,2,qty);   floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXPOWER_WARNING_LO",30,2,qty);   floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RXPOWER_ALARM_HI",32,2,qty);     floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RXPOWER_ALARM_LO",34,2,qty);     floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RXPOWER_WARNING_HI",36,2,qty);   floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RXPOWER_WARNING_LO",38,2,qty);   floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"CHECKSUM_0_94",95,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TEMP",96,2,qty);                 floatI2CQuantity(bus,dev,qty,true,1.0/256.0); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC",98,2,qty);                  floatI2CQuantity(bus,dev,qty,false,0.0001);   endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXBIAS",100,2,qty);              floatI2CQuantity(bus,dev,qty,false,0.000002); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXPOWER",102,2,qty);             floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RXPOWER",104,2,qty);             floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS",110,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"FLAGS_ALARM",112,2,qty);         endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"FLAGS_WARNING",116,2,qty);       endianI2CQuantity(bus,dev,qty,false);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::avagoQSFPPlusLO(const unsigned int bus, const unsigned int dev) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"QSFPlo");

    // set page number = 0x00
    pageI2CDevice(bus,dev,0x7f,0x00);

    unsigned int qty;

//------------------------------------------------------------------------------
// based on doc/SFF-8436.pdf p.60ff
//------------------------------------------------------------------------------
    addI2CQuantity(bus,dev,I2CInterface::WORD,"STATUS",1,2,qty);                endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"FAULT",4,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TEMP_ALARM",6,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"VCC_ALARM",7,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RXPOWER_ALARM_1_2",9,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RXPOWER_ALARM_3_4",10,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXBIAS_ALARM_1_2",11,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXBIAS_ALARM_3_4",12,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TEMP",22,2,qty);                 floatI2CQuantity(bus,dev,qty,true,1.0/256.0); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC",26,2,qty);                  floatI2CQuantity(bus,dev,qty,false,0.0001);   endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RXPOWER_1",34,2,qty);            floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RXPOWER_2",36,2,qty);            floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RXPOWER_3",38,2,qty);            floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RXPOWER_4",40,2,qty);            floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXBIAS_1",42,2,qty);             floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXBIAS_2",44,2,qty);             floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXBIAS_3",46,2,qty);             floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXBIAS_4",48,2,qty);             floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXDISABLE",86,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RX_RATE_SELECT",87,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TX_RATE_SELECT",88,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RX_APP_SELECT_4",89,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RX_APP_SELECT_3",90,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RX_APP_SELECT_2",91,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RX_APP_SELECT_1",92,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TX_APP_SELECT_4",94,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TX_APP_SELECT_3",95,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TX_APP_SELECT_2",96,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TX_APP_SELECT_1",97,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"LOS_MASK",100,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TX_FAULT_MASK",101,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TEMP_ALARM_MASK",103,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"VCC_ALARM_MASK",104,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"VENDOR_NAME",148,16,qty);        displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"VENDOR_PART_NUMBER",168,16,qty); displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"VENDOR_SERIAL_NUMBER",196,16,qty); displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"VENDOR_DATE_CODE",212,8,qty);    displayI2CQuantity(bus,dev,qty,STRING);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::avagoQSFPPlusUP3(const unsigned int bus, const unsigned int dev) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"QSFPup3");

    // set page number = 0x03
    pageI2CDevice(bus,dev,0x7f,0x03);

    unsigned int qty;

//------------------------------------------------------------------------------
// based on doc/SFF-8436.pdf p.85ff
//------------------------------------------------------------------------------
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TEMP_ALARM_HI",128,2,qty);       floatI2CQuantity(bus,dev,qty,true,1.0/256.0); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TEMP_ALARM_LO",130,2,qty);       floatI2CQuantity(bus,dev,qty,true,1.0/256.0); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TEMP_WARNING_HI",132,2,qty);     floatI2CQuantity(bus,dev,qty,true,1.0/256.0); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TEMP_WARNING_LO",134,2,qty);     floatI2CQuantity(bus,dev,qty,true,1.0/256.0); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC_ALARM_HI",144,2,qty);        floatI2CQuantity(bus,dev,qty,false,0.0001);   endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC_ALARM_LO",146,2,qty);        floatI2CQuantity(bus,dev,qty,false,0.0001);   endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC_WARNING_HI",148,2,qty);      floatI2CQuantity(bus,dev,qty,false,0.0001);   endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC_WARNING_LO",150,2,qty);      floatI2CQuantity(bus,dev,qty,false,0.0001);   endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RXPOWER_ALARM_HI",176,2,qty);    floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RXPOWER_ALARM_LO",178,2,qty);    floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RXPOWER_WARNING_HI",180,2,qty);  floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RXPOWER_WARNING_LO",182,2,qty);  floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXBIAS_ALARM_HI",184,2,qty);     floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXBIAS_ALARM_LO",186,2,qty);     floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXBIAS_WARNING_HI",188,2,qty);   floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXBIAS_WARNING_LO",190,2,qty);   floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RXAMPLITUDE_1_2",238,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RXAMPLITUDE_3_4",239,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"SQUELCH_DISABLE",240,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RXDISABLE",241,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RXPOWER_ALARM_MASK_1_2",242,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RXPOWER_ALARM_MASK_3_4",243,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXBIAS_ALARM_MASK_1_2",244,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXBIAS_ALARM_MASK_3_4",245,1,qty);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::ddr3SPD(const unsigned int bus, const unsigned int dev) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"DDR3");

    unsigned int qty;

    addI2CQuantity(bus,dev,I2CInterface::WORD,"MANUFACTURER_ID",117,2,qty);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"MODULE_DATE_CODE",120,2,qty);    endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"MODULE_SERIAL_NUMBER",122,4,qty); displayI2CQuantity(bus,dev,qty,MEMORY);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"MODULE_PART_NUMBER",128,18,qty); displayI2CQuantity(bus,dev,qty,STRING);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::avagoMiniPodRXLO(const unsigned int bus, const unsigned int dev) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"RMPODlo");

    // set page number = 0x00
    pageI2CDevice(bus,dev,0x7f,0x00);

    unsigned int qty;

//------------------------------------------------------------------------------
// based on doc/AFBR-811vx3Z-AFBR-821vx3Z-10-GbpsChannel-300m-link-Twelve-Channel-Parallel-Fiber-Optics-Modules.pdf p.43ff
//------------------------------------------------------------------------------
    // RX Memory Map Lower Page
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS",2,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RXLOS",9,2,qty);                 endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"FAULT",11,2,qty);                endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TEMP_ALARM",13,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"VCC_ALARM",14,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RXPOWER_ALARM_10_11",22,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RXPOWER_ALARM_08_09",23,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RXPOWER_ALARM_06_07",24,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RXPOWER_ALARM_04_05",25,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RXPOWER_ALARM_02_03",26,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RXPOWER_ALARM_00_01",27,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TEMPERATURE",28,2,qty);          floatI2CQuantity(bus,dev,qty,true,1.0/256.0); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC_3_3",32,2,qty);              floatI2CQuantity(bus,dev,qty,false,0.0001);   endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC_2_5",34,2,qty);              floatI2CQuantity(bus,dev,qty,false,0.0001);   endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RXPOWER_11",64,2,qty);           floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RXPOWER_10",66,2,qty);           floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RXPOWER_09",68,2,qty);           floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RXPOWER_08",70,2,qty);           floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RXPOWER_07",72,2,qty);           floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RXPOWER_06",74,2,qty);           floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RXPOWER_05",76,2,qty);           floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RXPOWER_04",78,2,qty);           floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RXPOWER_03",80,2,qty);           floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RXPOWER_02",82,2,qty);           floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RXPOWER_01",84,2,qty);           floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RXPOWER_00",86,2,qty);           floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RXRESET",91,1,qty);              writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RXDISABLE",92,2,qty);            endianI2CQuantity(bus,dev,qty,false); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"SQUELCH_DISABLE",94,2,qty);      endianI2CQuantity(bus,dev,qty,false); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"ELAPSED_TIME",88,2,qty);         floatI2CQuantity(bus,dev,qty,false,2.0);      endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"RATE_SELECT",96,3,qty);          endianI2CQuantity(bus,dev,qty,false); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RXLOS_MASK",112,2,qty);          endianI2CQuantity(bus,dev,qty,false); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TEMP_ALARM_MASK",116,1,qty);     writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"VCC_ALARM_MASK",117,1,qty);      writableI2CQuantity(bus,dev,qty,true);
    // RX Memory Map 00h Upper Page
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"VENDOR_NAME",152,16,qty);        displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"VENDOR_PART_NUMBER",171,16,qty); displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"VENDOR_SERIAL_NUMBER",189,16,qty); displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"VENDOR_DATE_CODE",205,8,qty);    displayI2CQuantity(bus,dev,qty,STRING);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::avagoMiniPodRXUP1(const unsigned int bus, const unsigned int dev) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"RMPODup1");

    // set page number = 0x01
    pageI2CDevice(bus,dev,0x7f,0x01);

    unsigned int qty;

//------------------------------------------------------------------------------
// based on doc/AFBR-811vx3Z-AFBR-821vx3Z-10-GbpsChannel-300m-link-Twelve-Channel-Parallel-Fiber-Optics-Modules.pdf p.48ff
//------------------------------------------------------------------------------
    // RX Memory Map 01h Upper Page
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TEMP_ALARM_HI",128,2,qty);       floatI2CQuantity(bus,dev,qty,true,1.0/256.0); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TEMP_ALARM_LO",130,2,qty);       floatI2CQuantity(bus,dev,qty,true,1.0/256.0); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC_3_3_ALARM_HI",144,2,qty);    floatI2CQuantity(bus,dev,qty,false,0.0001);   endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC_3_3_ALARM_LO",146,2,qty);    floatI2CQuantity(bus,dev,qty,false,0.0001);   endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC_2_5_ALARM_HI",152,2,qty);    floatI2CQuantity(bus,dev,qty,false,0.0001);   endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC_2_5_ALARM_LO",154,2,qty);    floatI2CQuantity(bus,dev,qty,false,0.0001);   endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RXPOWER_ALARM_HI",184,2,qty);    floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RXPOWER_ALARM_LO",186,2,qty);    floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"OUTPUT_POLARITY",226,2,qty);     endianI2CQuantity(bus,dev,qty,false); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RXAMPLITUDE_10_11",228,1,qty);   writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RXAMPLITUDE_08_09",229,1,qty);   writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RXAMPLITUDE_06_07",230,1,qty);   writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RXAMPLITUDE_04_05",231,1,qty);   writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RXAMPLITUDE_02_03",232,1,qty);   writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RXAMPLITUDE_00_01",233,1,qty);   writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RXDEEMPHASIS_10_11",234,1,qty);  writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RXDEEMPHASIS_08_09",235,1,qty);  writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RXDEEMPHASIS_06_07",236,1,qty);  writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RXDEEMPHASIS_04_05",237,1,qty);  writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RXDEEMPHASIS_02_03",238,1,qty);  writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RXDEEMPHASIS_00_01",239,1,qty);  writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RXPOWER_ALARM_MASK_10_11",250,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RXPOWER_ALARM_MASK_08_09",251,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RXPOWER_ALARM_MASK_06_07",252,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RXPOWER_ALARM_MASK_04_05",253,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RXPOWER_ALARM_MASK_02_03",254,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RXPOWER_ALARM_MASK_00_01",255,1,qty); writableI2CQuantity(bus,dev,qty,true);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::avagoMiniPodTXLO(const unsigned int bus, const unsigned int dev) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"TMPODlo");

    // set page number = 0x00
    pageI2CDevice(bus,dev,0x7f,0x00);

    unsigned int qty;

//------------------------------------------------------------------------------
// based on doc/AFBR-811vx3Z-AFBR-821vx3Z-10-GbpsChannel-300m-link-Twelve-Channel-Parallel-Fiber-Optics-Modules.pdf p.32ff
//------------------------------------------------------------------------------
    // TX Memory Map Lower Page
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS",2,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXLOS",9,2,qty);                 endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"FAULT",11,2,qty);                endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TEMP_ALARM",13,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"VCC_ALARM",14,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXBIAS_ALARM_10_11",16,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXBIAS_ALARM_08_09",17,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXBIAS_ALARM_06_07",18,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXBIAS_ALARM_04_05",19,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXBIAS_ALARM_02_03",20,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXBIAS_ALARM_00_01",21,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXPOWER_ALARM_10_11",22,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXPOWER_ALARM_08_09",23,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXPOWER_ALARM_06_07",24,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXPOWER_ALARM_04_05",25,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXPOWER_ALARM_02_03",26,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXPOWER_ALARM_00_01",27,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TEMPERATURE",28,2,qty);          floatI2CQuantity(bus,dev,qty,true,1.0/256.0); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC_3_3",32,2,qty);              floatI2CQuantity(bus,dev,qty,false,0.0001);   endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC_2_5",34,2,qty);              floatI2CQuantity(bus,dev,qty,false,0.0001);   endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXBIAS_11",40,2,qty);            floatI2CQuantity(bus,dev,qty,false,0.000002); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXBIAS_10",42,2,qty);            floatI2CQuantity(bus,dev,qty,false,0.000002); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXBIAS_09",44,2,qty);            floatI2CQuantity(bus,dev,qty,false,0.000002); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXBIAS_08",46,2,qty);            floatI2CQuantity(bus,dev,qty,false,0.000002); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXBIAS_07",48,2,qty);            floatI2CQuantity(bus,dev,qty,false,0.000002); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXBIAS_06",50,2,qty);            floatI2CQuantity(bus,dev,qty,false,0.000002); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXBIAS_05",52,2,qty);            floatI2CQuantity(bus,dev,qty,false,0.000002); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXBIAS_04",54,2,qty);            floatI2CQuantity(bus,dev,qty,false,0.000002); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXBIAS_03",56,2,qty);            floatI2CQuantity(bus,dev,qty,false,0.000002); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXBIAS_02",58,2,qty);            floatI2CQuantity(bus,dev,qty,false,0.000002); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXBIAS_01",60,2,qty);            floatI2CQuantity(bus,dev,qty,false,0.000002); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXBIAS_00",62,2,qty);            floatI2CQuantity(bus,dev,qty,false,0.000002); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXPOWER_11",64,2,qty);           floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXPOWER_10",66,2,qty);           floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXPOWER_09",68,2,qty);           floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXPOWER_08",70,2,qty);           floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXPOWER_07",72,2,qty);           floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXPOWER_06",74,2,qty);           floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXPOWER_05",76,2,qty);           floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXPOWER_04",78,2,qty);           floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXPOWER_03",80,2,qty);           floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXPOWER_02",82,2,qty);           floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXPOWER_01",84,2,qty);           floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXPOWER_00",86,2,qty);           floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"ELAPSED_TIME",88,2,qty);         floatI2CQuantity(bus,dev,qty,false,2.0);      endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXRESET",91,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXDISABLE",92,2,qty);            endianI2CQuantity(bus,dev,qty,false); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"SQUELCH_DISABLE",94,2,qty);      endianI2CQuantity(bus,dev,qty,false); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"MARGIN_ACTIVATION",99,2,qty);    endianI2CQuantity(bus,dev,qty,false); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXLOS_MASK",112,2,qty);          endianI2CQuantity(bus,dev,qty,false); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"FAULT_MASK",114,2,qty);          endianI2CQuantity(bus,dev,qty,false); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TEMP_ALARM_MASK",116,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"VCC_ALARM_MASK",117,1,qty); writableI2CQuantity(bus,dev,qty,true);
    // TX Memory Map 00h Upper Page
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"VENDOR_NAME",152,16,qty);        displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"VENDOR_PART_NUMBER",171,16,qty); displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"VENDOR_SERIAL_NUMBER",189,16,qty); displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"VENDOR_DATE_CODE",205,8,qty);    displayI2CQuantity(bus,dev,qty,STRING);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::avagoMiniPodTXUP1(const unsigned int bus, const unsigned int dev) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"TMPODup1");

    // set page number = 0x01
    pageI2CDevice(bus,dev,0x7f,0x01);

    unsigned int qty;

//------------------------------------------------------------------------------
// based on doc/AFBR-811vx3Z-AFBR-821vx3Z-10-GbpsChannel-300m-link-Twelve-Channel-Parallel-Fiber-Optics-Modules.pdf p.39ff
//------------------------------------------------------------------------------
    // TX Memory Map 01h Upper Page
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TEMP_ALARM_HI",128,2,qty);       floatI2CQuantity(bus,dev,qty,true,1.0/256.0); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TEMP_ALARM_LO",130,2,qty);       floatI2CQuantity(bus,dev,qty,true,1.0/256.0); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC_3_3_ALARM_HI",144,2,qty);    floatI2CQuantity(bus,dev,qty,false,0.0001);   endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC_3_3_ALARM_LO",146,2,qty);    floatI2CQuantity(bus,dev,qty,false,0.0001);   endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC_2_5_ALARM_HI",152,2,qty);    floatI2CQuantity(bus,dev,qty,false,0.0001);   endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC_2_5_ALARM_LO",154,2,qty);    floatI2CQuantity(bus,dev,qty,false,0.0001);   endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXBIAS_ALARM_HI",176,2,qty);     floatI2CQuantity(bus,dev,qty,false,0.000002); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXBIAS_ALARM_LO",178,2,qty);     floatI2CQuantity(bus,dev,qty,false,0.000002); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXPOWER_ALARM_HI",184,2,qty);    floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXPOWER_ALARM_LO",186,2,qty);    floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"INPUT_POLARITY",226,2,qty);      endianI2CQuantity(bus,dev,qty,false); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXEQUALIZATION_10_11",228,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXEQUALIZATION_08_09",229,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXEQUALIZATION_06_07",230,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXEQUALIZATION_04_05",231,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXEQUALIZATION_02_03",232,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXEQUALIZATION_00_01",233,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXBIAS_ALARM_MASK_10_11",244,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXBIAS_ALARM_MASK_08_09",245,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXBIAS_ALARM_MASK_06_07",246,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXBIAS_ALARM_MASK_04_05",247,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXBIAS_ALARM_MASK_02_03",248,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXBIAS_ALARM_MASK_00_01",249,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXPOWER_ALARM_MASK_10_11",250,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXPOWER_ALARM_MASK_08_09",251,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXPOWER_ALARM_MASK_06_07",252,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXPOWER_ALARM_MASK_04_05",253,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXPOWER_ALARM_MASK_02_03",254,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TXPOWER_ALARM_MASK_00_01",255,1,qty); writableI2CQuantity(bus,dev,qty,true);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::samtecFireFlyRXLO(const unsigned int bus, const unsigned int dev) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"RFFLYlo");

    // set page number = 0x00
    pageI2CDevice(bus,dev,0x7f,0x00);

    unsigned int qty;

//------------------------------------------------------------------------------
// based on Documents/ Firefly\ Manual\ ECUO\ 14G\ x12\ v05.pdf, p. 54ff
//------------------------------------------------------------------------------
    // RX Memory Map xxh Lower Page
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS",2,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS_SUMMARY",6,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"L-LOS",7,2,qty);                 endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"L-POWER_RX_08_11",14,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"L-POWER_RX_04_07",15,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"L-POWER_RX_00_03",16,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"L-TEMP",17,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"L-VCC",18,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TEMPERATURE",22,1,qty);          floatI2CQuantity(bus,dev,qty,true,1.0);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC",26,2,qty);                  floatI2CQuantity(bus,dev,qty,false,0.0001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"ELAPSED_TIME",38,2,qty);         floatI2CQuantity(bus,dev,qty,false,2.0); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RESET",51,1,qty);                writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"CHANNEL_DISABLE",52,2,qty);      endianI2CQuantity(bus,dev,qty,false); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"OUTPUT_DISABLE",54,2,qty);       endianI2CQuantity(bus,dev,qty,false); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"OUTPUT_AMPLITUDE_10_11",62,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"OUTPUT_AMPLITUDE_08_09",63,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"OUTPUT_AMPLITUDE_06_07",64,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"OUTPUT_AMPLITUDE_04_05",65,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"OUTPUT_AMPLITUDE_02_03",66,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"OUTPUT_AMPLITUDE_00_01",67,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"OUTPUT_DEEMPHASIS_10_11",68,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"OUTPUT_DEEMPHASIS_08_09",69,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"OUTPUT_DEEMPHASIS_06_07",70,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"OUTPUT_DEEMPHASIS_04_05",71,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"OUTPUT_DEEMPHASIS_02_03",72,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"OUTPUT_DEEMPHASIS_00_01",73,1,qty); writableI2CQuantity(bus,dev,qty,true);

    // RX Memory Map 00h Upper Page
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"VENDOR_NAME",152,16,qty);        displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"VENDOR_PART_NUMBER",171,16,qty); displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VENDOR_REVISION_NUMBER",187,2,qty); displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"VENDOR_SERIAL_NUMBER",189,16,qty); displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"VENDOR_DATE_CODE",205,8,qty);    displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"VENDOR_LOT_CODE",213,10,qty);    displayI2CQuantity(bus,dev,qty,STRING);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::samtecFireFlyRXUP1(const unsigned int bus, const unsigned int dev) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"RFFLYup1");

    // set page number = 0x00
    pageI2CDevice(bus,dev,0x7f,0x01);

    unsigned int qty;

//------------------------------------------------------------------------------
// based on Documents/ Firefly\ Manual\ ECUO\ 14G\ x12\ v05.pdf, p. 54ff
//------------------------------------------------------------------------------
    // RX Memory Map 01h Upper Page
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TEMP_ALARM_HI",128,1,qty);       floatI2CQuantity(bus,dev,qty,true,1.0);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TEMP_ALARM_LO",130,1,qty);       floatI2CQuantity(bus,dev,qty,true,1.0);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC_ALARM_HI",144,2,qty);        floatI2CQuantity(bus,dev,qty,false,0.0001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC_ALARM_LO",146,2,qty);        floatI2CQuantity(bus,dev,qty,false,0.0001); endianI2CQuantity(bus,dev,qty,false);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::samtecFireFlyTXLO(const unsigned int bus, const unsigned int dev) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"TFFLYlo");

    // set page number = 0x00
    pageI2CDevice(bus,dev,0x7f,0x00);

    unsigned int qty;

//------------------------------------------------------------------------------
// based on Documents/ Firefly\ Manual\ ECUO\ 14G\ x12\ v05.pdf, p. 54ff
//------------------------------------------------------------------------------
    // TX Memory Map xxh Lower Page
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS",2,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS_SUMMARY",6,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"L-FAULT",9,2,qty);               endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"L-TEMP",17,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"L-VCC",18,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TEMPERATURE",22,1,qty);          floatI2CQuantity(bus,dev,qty,true,1.0);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC",26,2,qty);                  floatI2CQuantity(bus,dev,qty,false,0.0001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"ELAPSED_TIME",38,2,qty);         floatI2CQuantity(bus,dev,qty,false,2.0); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RESET",51,1,qty);                writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"CHANNEL_DISABLE",52,2,qty);      endianI2CQuantity(bus,dev,qty,false); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"CHANNEL_POLARITY_INVERT",58,2,qty); endianI2CQuantity(bus,dev,qty,false); writableI2CQuantity(bus,dev,qty,true);

    // TX Memory Map 00h Upper Page
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"VENDOR_NAME",152,16,qty);        displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"VENDOR_PART_NUMBER",171,16,qty); displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VENDOR_REVISION_NUMBER",187,2,qty); displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"VENDOR_SERIAL_NUMBER",189,16,qty); displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"VENDOR_DATE_CODE",205,8,qty);    displayI2CQuantity(bus,dev,qty,STRING);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::samtecFireFlyTXUP1(const unsigned int bus, const unsigned int dev) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"TFFLYup1");

    // set page number = 0x00
    pageI2CDevice(bus,dev,0x7f,0x01);

    unsigned int qty;

//------------------------------------------------------------------------------
// based on Documents/ Firefly\ Manual\ ECUO\ 14G\ x12\ v05.pdf, p. 54ff
//------------------------------------------------------------------------------
    // TX Memory Map 01h Upper Page
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TEMP_ALARM_HI",128,1,qty);       floatI2CQuantity(bus,dev,qty,true,1.0);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TEMP_ALARM_LO",130,1,qty);       floatI2CQuantity(bus,dev,qty,true,1.0);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC_ALARM_HI",144,2,qty);        floatI2CQuantity(bus,dev,qty,false,0.0001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC_ALARM_LO",146,2,qty);        floatI2CQuantity(bus,dev,qty,false,0.0001); endianI2CQuantity(bus,dev,qty,false);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::pdt012a(const unsigned int bus, const unsigned int dev) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"PDT012A");

    unsigned int qty;

    pmbusI2CDevice(bus,dev);

//------------------------------------------------------------------------------
// based on doc/PDT012a0x.pdf p.25ff
//------------------------------------------------------------------------------
    addI2CQuantity(bus,dev,I2CInterface::DATA,"OPERATION",0x01,1,qty);          // DCS
    addI2CQuantity(bus,dev,I2CInterface::BYTE,"CLEAR_FAULTS",0x03,0,qty);       readableI2CQuantity(bus,dev,qty,false); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"VOUT_MODE",0x20,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VOUT_TRIM",0x22,2,qty);          displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,0,-10);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VOUT_MARGIN_HIGH",0x25,2,qty);   displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,0,-10);
                                                                                writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VOUT_MARGIN_LOW",0x26,2,qty);    displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,0,-10);
                                                                                writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VOUT_SCALE_LOOP",0x29,2,qty);    displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VIN_ON",0x35,2,qty);             displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VIN_OFF",0x36,2,qty);            displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VOUT_OV_FAULT_LIMIT",0x40,2,qty); displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,0,-10);
                                                                                writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"VOUT_OV_FAULT_RESPONSE",0x41,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VOUT_UV_FAULT_LIMIT",0x44,2,qty); displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,0,-10);
                                                                                writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"VOUT_UV_FAULT_RESPONSE",0x45,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"IOUT_OC_FAULT_LIMIT",0x46,2,qty); displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"IOUT_OC_WARN_LIMIT",0x4a,2,qty); displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"POWER_GOOD_ON",0x5e,2,qty);      displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,0,-10);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"POWER_GOOD_OFF",0x5f,2,qty);     displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,0,-10);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TON_RISE",0x61,2,qty);           displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS_BYTE",0x78,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"STATUS_WORD",0x79,2,qty);         // DCS
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS_VOUT",0x7a,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS_IOUT",0x7b,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS_TEMPERATURE",0x7d,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS_CML",0x7e,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"READ_VIN",0x88, 2,qty);          displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);   // DCS
    addI2CQuantity(bus,dev,I2CInterface::WORD,"READ_VOUT",0x8b,2,qty);          displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,0,-10); // DCS
    addI2CQuantity(bus,dev,I2CInterface::WORD,"READ_IOUT",0x8c,2,qty);          displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);   // DCS

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::si570(const unsigned int bus, const unsigned int dev) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"SI570");

    unsigned int qty;

    addI2CQuantity(bus,dev,I2CInterface::DATA,"HS_DIV",7,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"RFREQ",8,5,qty);                 displayI2CQuantity(bus,dev,qty,MEMORY);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"HS_DIV_7PPM",13,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"RFREQ_7PPM",14,5,qty);           displayI2CQuantity(bus,dev,qty,MEMORY);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"CONTROL",135,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"FREEZE",137,1,qty);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::adv7511(const unsigned int bus, const unsigned int dev) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"ADV7511");

    unsigned int qty;

    addI2CQuantity(bus,dev,I2CInterface::DATA,"REVISION",0,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"CTS",1,3,qty);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::m24c08(const unsigned int bus, const unsigned int dev) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"M24C08");

    unsigned int qty;

    addI2CQuantity(bus,dev,I2CInterface::IBLK,"MEMORY",0,256,qty);              displayI2CQuantity(bus,dev,qty,MEMORY);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::ddr3sodimm(const unsigned int bus, const unsigned int dev) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"DDR3so");

    unsigned int qty;

    addI2CQuantity(bus,dev,I2CInterface::IBLK,"SPD_MEMORY",0,256,qty);          displayI2CQuantity(bus,dev,qty,MEMORY);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::rtc8564je(const unsigned int bus, const unsigned int dev) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"RTC8564");

    unsigned int qty;

    addI2CQuantity(bus,dev,I2CInterface::DATA,"SECONDS",0x2,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"MINUTES",0x3,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"HOURS",0x4,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"DAYS",0x5,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"WEEKDAYS",0x6,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"MONTH",0x7,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"YEAR",0x8,1,qty);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::si5324(const unsigned int bus, const unsigned int dev) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"SI5324");

    unsigned int qty;

    addI2CQuantity(bus,dev,I2CInterface::DATA,"CONTROL",0,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"CK_PRIOR",1,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"BW_SEL",2,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"PARTNUM_REVID",134,2,qty);
    endianI2CQuantity(bus,dev,qty,false);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::ucd90120a(const unsigned int bus, const unsigned int dev) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"UCD90120");

    unsigned int qty;

    addI2CQuantity(bus,dev,I2CInterface::WORD,"TEMP",0x8d,2,qty);               floatI2CQuantity(bus,dev,qty,true,1.0); exponentI2CQuantity(bus,dev,qty,5,0);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"PMBUS_REVISION",0x98,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"MFR_ID",0x99,18,qty);            displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"MFR_MODEL",0x9a,12,qty);         displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"MFR_REVISION",0x9b,12,qty);      displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"MFR_LOCATION",0x9c,12,qty);      displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"MFR_DATE",0x9d,6,qty);           displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"MFR_SERIAL",0x9e,12,qty);        displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"DEVICE_ID",0xfd,32,qty);         displayI2CQuantity(bus,dev,qty,STRING);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::ltc2990(const unsigned int bus, const unsigned int dev) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"LTC2990");

    unsigned int qty;

    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS",0x0,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"CONTROL",0x1,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"TRIGGER",0x2,1,qty);             writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TINT",0x4,2,qty);                endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"V1",0x6,2,qty);                  endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"V2",0x8,2,qty);                  endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"V3",0xa,2,qty);                  endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"V4",0xc,2,qty);                  endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC",0xe,2,qty);                 endianI2CQuantity(bus,dev,qty,false);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::ltc2991(const unsigned int bus, const unsigned int dev) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"LTC2991");

    unsigned int qty;

//------------------------------------------------------------------------------
// based on doc/LTC2991ff.pdf p.18ff
//------------------------------------------------------------------------------
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS_LO",0x00,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS_HI",0x01,1,qty);              writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"CONTROL_V1V2_V3V4",0x06,1,qty);      writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"CONTROL_V5V6_V7V8",0x07,1,qty);      writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"CONTROL_PWM_VCC_TINT",0x08,1,qty);   writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"PWM_THRESHOLD_HI",0x09,1,qty);       writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"V1",0x0a,2,qty);                     floatI2CQuantity(bus,dev,qty,false,0.00030518); endianI2CQuantity(bus,dev,qty,false);
                                                                                    maskI2CQuantity(bus,dev,qty,0x00007fff);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"V2",0x0c,2,qty);                     floatI2CQuantity(bus,dev,qty,false,0.00030518); endianI2CQuantity(bus,dev,qty,false);
                                                                                    maskI2CQuantity(bus,dev,qty,0x00007fff);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"V3",0x0e,2,qty);                     floatI2CQuantity(bus,dev,qty,false,0.00030518); endianI2CQuantity(bus,dev,qty,false);
                                                                                    maskI2CQuantity(bus,dev,qty,0x00007fff);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"V4",0x10,2,qty);                     floatI2CQuantity(bus,dev,qty,false,0.00030518); endianI2CQuantity(bus,dev,qty,false);
                                                                                    maskI2CQuantity(bus,dev,qty,0x00007fff);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"V5",0x12,2,qty);                     floatI2CQuantity(bus,dev,qty,false,0.00030518); endianI2CQuantity(bus,dev,qty,false);
                                                                                    maskI2CQuantity(bus,dev,qty,0x00007fff);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"V6",0x14,2,qty);                     floatI2CQuantity(bus,dev,qty,false,0.00030518); endianI2CQuantity(bus,dev,qty,false);
                                                                                    maskI2CQuantity(bus,dev,qty,0x00007fff);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"V7",0x16,2,qty);                     floatI2CQuantity(bus,dev,qty,false,0.00030518); endianI2CQuantity(bus,dev,qty,false);
                                                                                    maskI2CQuantity(bus,dev,qty,0x00007fff);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"V8",0x18,2,qty);                     floatI2CQuantity(bus,dev,qty,false,0.00030518); endianI2CQuantity(bus,dev,qty,false);
                                                                                    maskI2CQuantity(bus,dev,qty,0x00007fff);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TINT",0x1a,2,qty);                   floatI2CQuantity(bus,dev,qty,false,0.0625);     endianI2CQuantity(bus,dev,qty,false);
                                                                                    maskI2CQuantity(bus,dev,qty,0x00007fff);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC",0x1c,2,qty);                    floatI2CQuantity(bus,dev,qty,false,0.00030518,2.5); endianI2CQuantity(bus,dev,qty,false);
                                                                                    maskI2CQuantity(bus,dev,qty,0x00007fff);
//    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC_WORD",0x1c,2,qty);               endianI2CQuantity(bus,dev,qty,false);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::ltm4676(const unsigned int bus, const unsigned int dev) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"LTM4676");

    // set page number = 0x00
    pageI2CDevice(bus,dev,0x00,0x00);

    unsigned int qty;

    pmbusI2CDevice(bus,dev);

//------------------------------------------------------------------------------
// based on doc/LTM4676afa.pdf p.40ff
//------------------------------------------------------------------------------
//    addI2CQuantity(bus,dev,I2CInterface::DATA,"PAGE",0x00,1,qty);               writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::BYTE,"CLEAR_FAULTS",0x03,0,qty);       readableI2CQuantity(bus,dev,qty,false); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::PROC,"SMBALERT_MASK",0x1b,2,qty);      writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"VOUT_MODE",0x20,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS_BYTE",0x78,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"STATUS_WORD",0x79,2,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS_VOUT",0x7a,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS_IOUT",0x7b,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS_INPUT",0x7c,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS_TEMPERATURE",0x7d,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS_CML",0x7e,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS_MFR_SPECIFIC",0x80,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"READ_VIN",0x88,2,qty);           displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"READ_IIN",0x89,2,qty);           displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"READ_VOUT",0x8b,2,qty);          displayI2CQuantity(bus,dev,qty,FLOAT); convertI2CQuantity(bus,dev,qty,1.0/4096.0);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"READ_IOUT",0x8c,2,qty);          displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"READ_TEMPERATURE_1",0x8d,2,qty); displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"READ_TEMPERATURE_2",0x8e,2,qty); displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"READ_DUTY_CYCLE",0x94,2,qty);    displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"READ_POUT",0x96,2,qty);          displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    addI2CQuantity(bus,dev,I2CInterface::SBLK,"MFR_ID",0x99,3,qty);             displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::SBLK,"MFR_MODEL",0x9a,7,qty);          displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::SBLK,"MFR_SERIAL",0x9e,9,qty);         displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"MFR_IOUT_PEAK",0xd7,2,qty);      displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"MFR_VOUT_PEAK",0xdd,2,qty);      displayI2CQuantity(bus,dev,qty,FLOAT); convertI2CQuantity(bus,dev,qty,1.0/4096.0);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"MFR_VIN_PEAK",0xde,2,qty);       displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"MFR_TEMPERATURE_1_PEAK",0xdf,2,qty); displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    addI2CQuantity(bus,dev,I2CInterface::BYTE,"MFR_CLEAR_PEAKS",0xe3,0,qty);    readableI2CQuantity(bus,dev,qty,false); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"MFR_PADS",0xe5,2,qty);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"MFR_IIN_OFFSET",0xe9,2,qty);     displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    addI2CQuantity(bus,dev,I2CInterface::BYTE,"MFR_FAULT_LOG_STORE",0xea,0,qty); readableI2CQuantity(bus,dev,qty,false); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::BYTE,"MFR_FAULT_LOG_CLEAR",0xec,0,qty); readableI2CQuantity(bus,dev,qty,false); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"MFR_READ_IIN",0xed,2,qty);        displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    addI2CQuantity(bus,dev,I2CInterface::PBLK,"MFR_FAULT_LOG",0xee,147,qty);     displayI2CQuantity(bus,dev,qty,MEMORY);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"MFR_COMMON",0xef,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"MFR_TEMPERATURE_2_PEAK",0xf4,2,qty); displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::ltm4676_p1(const unsigned int bus, const unsigned int dev) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"LTM4676");

    // set page number = 0x01
    pageI2CDevice(bus,dev,0x00,0x01);

    unsigned int qty;

    pmbusI2CDevice(bus,dev);

//------------------------------------------------------------------------------
// based on doc/LTM4676afa.pdf p.40ff
//------------------------------------------------------------------------------
//    addI2CQuantity(bus,dev,I2CInterface::DATA,"PAGE",0x00,1,qty);               writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS_BYTE",0x78,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"STATUS_WORD",0x79,2,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS_VOUT",0x7a,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS_IOUT",0x7b,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS_INPUT",0x7c,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS_TEMPERATURE",0x7d,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS_CML",0x7e,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS_MFR_SPECIFIC",0x80,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"READ_VOUT",0x8b,2,qty);          displayI2CQuantity(bus,dev,qty,FLOAT); convertI2CQuantity(bus,dev,qty,1.0/4096.0);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"READ_IOUT",0x8c,2,qty);          displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"READ_TEMPERATURE_1",0x8d,2,qty); displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"READ_DUTY_CYCLE",0x94,2,qty);    displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"READ_POUT",0x96,2,qty);          displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"MFR_IOUT_PEAK",0xd7,2,qty);      displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"MFR_VOUT_PEAK",0xdd,2,qty);      displayI2CQuantity(bus,dev,qty,FLOAT); convertI2CQuantity(bus,dev,qty,1.0/4096.0);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"MFR_TEMPERATURE_1_PEAK",0xdf,2,qty); displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"MFR_READ_IIN",0xed,2,qty);        displayI2CQuantity(bus,dev,qty,FLOAT); exponentI2CQuantity(bus,dev,qty,5,0);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::adn2814(const unsigned int bus, const unsigned int dev) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"ADN2814");

    unsigned int qty;

//------------------------------------------------------------------------------
// based on doc/ADN2814.pdf p.11
//------------------------------------------------------------------------------
    addI2CQuantity(bus,dev,I2CInterface::DATA,"FREQ0",0,1,qty);                 displayI2CQuantity(bus,dev,qty,FLOAT);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"FREQ1",1,1,qty);                 displayI2CQuantity(bus,dev,qty,FLOAT);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"FREQ2",2,1,qty);                 displayI2CQuantity(bus,dev,qty,FLOAT);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RATE",3,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"MISC",4,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"CTRLA",5,1,qty);                 /*readableI2CQuantity(bus,dev,qty,false);*/ writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"CTRLB",6,1,qty);                 /*readableI2CQuantity(bus,dev,qty,false);*/ writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"CTRLC",7,1,qty);                 /*readableI2CQuantity(bus,dev,qty,false);*/ writableI2CQuantity(bus,dev,qty,true);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::abcu5740(const unsigned int bus, const unsigned int dev) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"ABCU5740");

    unsigned int qty;

//------------------------------------------------------------------------------
// based on doc/ABCU-5740RZ_20160510175022842.pdf p.11ff
//------------------------------------------------------------------------------
    addI2CQuantity(bus,dev,I2CInterface::WORD,"CONTROL",0,2,qty);               endianI2CQuantity(bus,dev,qty,false); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"STATUS",1,2,qty);                endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"AUTONEG_ADVERTISEMENT",4,2,qty); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"AUTONEG_LINKPARTNER",5,2,qty);   endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"AUTONEG_EXPANSION",6,2,qty);     endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"AUTONEG_NEXTPAGE_TRANSMIT",7,2,qty); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"AUTONEG_NEXTPAGE_RECEIVED",8,2,qty); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"MASTER_SLAVE_CONTROL",9,2,qty);  endianI2CQuantity(bus,dev,qty,false); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"MASTER_SLAVE_STATUS",10,2,qty);  endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"EXTENDED_CONTROL1",16,2,qty);    endianI2CQuantity(bus,dev,qty,false); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"EXTENDED_STATUS1",17,2,qty);     endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"EXTENDED_CONTROL2",20,2,qty);    endianI2CQuantity(bus,dev,qty,false); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RECEIVE_ERROR_COUNTER",21,2,qty); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"CABLE_DIAGNOSTIC1",22,2,qty);    endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"EXTENDED_CONTROL3",26,2,qty);    endianI2CQuantity(bus,dev,qty,false); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"EXTENDED_STATUS2",27,2,qty);     endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"CABLE_DIAGNOSTIC2",28,2,qty);    endianI2CQuantity(bus,dev,qty,false);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::ultrascaleSysmon(const unsigned int bus, const unsigned int dev) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"SYSMON");

    unsigned int qty;

//------------------------------------------------------------------------------
// based on doc/UG580-ultrascale-sysmon.pdf p.45ff
//------------------------------------------------------------------------------
    addI2CQuantity(bus,dev,I2CInterface::SMON,"TEMPERATURE",0x00,2,qty);        floatI2CQuantity(bus,dev,qty,false,501.3743/65536.0,-273.6777);
    addI2CQuantity(bus,dev,I2CInterface::SMON,"VCCINT",0x01,2,qty);             floatI2CQuantity(bus,dev,qty,false,3.0/65536.0);
    addI2CQuantity(bus,dev,I2CInterface::SMON,"VCCAUX",0x02,2,qty);             floatI2CQuantity(bus,dev,qty,false,3.0/65536.0);
    addI2CQuantity(bus,dev,I2CInterface::SMON,"MGTAVCC",0x03,2,qty);            floatI2CQuantity(bus,dev,qty,false,2.0/65536.0);
    addI2CQuantity(bus,dev,I2CInterface::SMON,"RESET",0x03,2,qty);              readableI2CQuantity(bus,dev,qty,false); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::SMON,"VCCBRAM",0x06,2,qty);            floatI2CQuantity(bus,dev,qty,false,3.0/65536.0);
    addI2CQuantity(bus,dev,I2CInterface::SMON,"MGTAVTT",0x10,2,qty);            floatI2CQuantity(bus,dev,qty,false,2.0/65536.0);
    addI2CQuantity(bus,dev,I2CInterface::SMON,"MAX_TEMPERATURE",0x20,2,qty);    floatI2CQuantity(bus,dev,qty,false,501.3743/65536.0,-273.6777);
    addI2CQuantity(bus,dev,I2CInterface::SMON,"MAX_VCCINT",0x21,2,qty);         floatI2CQuantity(bus,dev,qty,false,3.0/65536.0);
    addI2CQuantity(bus,dev,I2CInterface::SMON,"MAX_VCCAUX",0x22,2,qty);         floatI2CQuantity(bus,dev,qty,false,3.0/65536.0);
    addI2CQuantity(bus,dev,I2CInterface::SMON,"MAX_VCCBRAM",0x23,2,qty);        floatI2CQuantity(bus,dev,qty,false,3.0/65536.0);
    addI2CQuantity(bus,dev,I2CInterface::SMON,"MIN_TEMPERATURE",0x24,2,qty);    floatI2CQuantity(bus,dev,qty,false,501.3743/65536.0,-273.6777);
    addI2CQuantity(bus,dev,I2CInterface::SMON,"MIN_VCCINT",0x25,2,qty);         floatI2CQuantity(bus,dev,qty,false,3.0/65536.0);
    addI2CQuantity(bus,dev,I2CInterface::SMON,"MIN_VCCAUX",0x26,2,qty);         floatI2CQuantity(bus,dev,qty,false,3.0/65536.0);
    addI2CQuantity(bus,dev,I2CInterface::SMON,"MIN_VCCBRAM",0x27,2,qty);        floatI2CQuantity(bus,dev,qty,false,3.0/65536.0);
    addI2CQuantity(bus,dev,I2CInterface::SMON,"CONFIGURATION0",0x40,2,qty);     writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::SMON,"CONFIGURATION1",0x41,2,qty);     writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::SMON,"CONFIGURATION2",0x42,2,qty);     writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::SMON,"CONFIGURATION3",0x43,2,qty);     writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::SMON,"CONFIGURATION4",0x44,2,qty);     writableI2CQuantity(bus,dev,qty,true);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::ad5305(const unsigned int bus, const unsigned int dev) {
    
    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"AD5305");

    unsigned int qty;
 
//------------------------------------------------------------------------------
// based on doc/AD5305_5315_5325.pdf p.16-19ff
//------------------------------------------------------------------------------
    //addI2CQuantity(bus,dev,I2CInterface::WORD,"DAC_****",0x00,2,qty);         writableI2CQuantity(bus,dev,qty,true); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"DACA",0x01,2,qty);               writableI2CQuantity(bus,dev,qty,true); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"DACB",0x02,2,qty);               writableI2CQuantity(bus,dev,qty,true); endianI2CQuantity(bus,dev,qty,false);
    //addI2CQuantity(bus,dev,I2CInterface::WORD,"DAC_**BA",0x03,2,qty);         writableI2CQuantity(bus,dev,qty,true); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"DACC",0x04,2,qty);               writableI2CQuantity(bus,dev,qty,true); endianI2CQuantity(bus,dev,qty,false);
    //addI2CQuantity(bus,dev,I2CInterface::WORD,"DAC_*C*A",0x05,2,qty);         writableI2CQuantity(bus,dev,qty,true); endianI2CQuantity(bus,dev,qty,false);
    //addI2CQuantity(bus,dev,I2CInterface::WORD,"DAC_*CB*",0x06,2,qty);         writableI2CQuantity(bus,dev,qty,true); endianI2CQuantity(bus,dev,qty,false);
    //addI2CQuantity(bus,dev,I2CInterface::WORD,"DAC_*CBA",0x07,2,qty);         writableI2CQuantity(bus,dev,qty,true); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"DACD",0x08,2,qty);               writableI2CQuantity(bus,dev,qty,true); endianI2CQuantity(bus,dev,qty,false);
    //addI2CQuantity(bus,dev,I2CInterface::WORD,"DAC_D**A",0x09,2,qty);         writableI2CQuantity(bus,dev,qty,true); endianI2CQuantity(bus,dev,qty,false);
    //addI2CQuantity(bus,dev,I2CInterface::WORD,"DAC_D*B*",0x0a,2,qty);         writableI2CQuantity(bus,dev,qty,true); endianI2CQuantity(bus,dev,qty,false);
    //addI2CQuantity(bus,dev,I2CInterface::WORD,"DAC_D*BA",0x0b,2,qty);         writableI2CQuantity(bus,dev,qty,true); endianI2CQuantity(bus,dev,qty,false);
    //addI2CQuantity(bus,dev,I2CInterface::WORD,"DAC_DC**",0x0c,2,qty);         writableI2CQuantity(bus,dev,qty,true); endianI2CQuantity(bus,dev,qty,false);
    //addI2CQuantity(bus,dev,I2CInterface::WORD,"DAC_DC*A",0x0d,2,qty);         writableI2CQuantity(bus,dev,qty,true); endianI2CQuantity(bus,dev,qty,false);
    //addI2CQuantity(bus,dev,I2CInterface::WORD,"DAC_DCB*",0x0e,2,qty);         writableI2CQuantity(bus,dev,qty,true); endianI2CQuantity(bus,dev,qty,false);
    //addI2CQuantity(bus,dev,I2CInterface::WORD,"DAC_DCBA",0x0f,2,qty);         writableI2CQuantity(bus,dev,qty,true); endianI2CQuantity(bus,dev,qty,false);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::ds10cp154(const unsigned int bus, const unsigned int dev) {
    
    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"DS10CP154");

    unsigned int qty;

//------------------------------------------------------------------------------
// based on doc/ds10cp154a.pdf p.10-12ff
//------------------------------------------------------------------------------
    addI2CQuantity(bus,dev,I2CInterface::DATA,"CONFIG",0x00,1,qty);             writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"CTRL",0x03,1,qty);               writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"LOS",0x04,1,qty);
       
    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::si53xx_p0(const unsigned int bus, const unsigned int dev) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"SI5344B");

    // set page number = 0x00
    pageI2CDevice(bus,dev,0x01,0x00);

    unsigned int qty;

//------------------------------------------------------------------------------
// based on doc/Si5345-44-42-D-RM.pdf p.125-139ff
//------------------------------------------------------------------------------
    addI2CQuantity(bus,dev,I2CInterface::WORD,"BASE_PART_NUMBER",0x02,2,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"DEVICE_GRADE",0x04,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"DEVICE_REVISION",0x05,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS_FLG",0x0c,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"OOF_LOS_FLG",0x0d,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"HOLD_LOL_FLG",0x0e,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"PLL_CAL_FLG",0x0f,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS_STICKY_FLG",0x11,1,qty);   writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"OOF_LOS_STICKY_FLG",0x12,1,qty);  writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"HOLD_LOL_STICKY_FLG",0x13,1,qty); writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"PLL_CAL_STICKY_FLG",0x14,1,qty);  writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS_INTR_MSK",0x17,1,qty);     writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"OOF_LOS_INTR_MSK",0x18,1,qty);    writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"HOLD_LOL_INTR_MSK",0x19,1,qty);   writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"PLL_CAL_INTR_MSK",0x1a,1,qty);    writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"FINC_FDEC",0x1d,1,qty);           writableI2CQuantity(bus,dev,qty,true);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"PDN_HARD_RST_SYNC",0x1e,1,qty);   writableI2CQuantity(bus,dev,qty,true);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::si53xx_p2(const unsigned int bus, const unsigned int dev) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"SI5344B");

    // set page number = 0x02
    pageI2CDevice(bus,dev,0x01,0x02);

    unsigned int qty;

//------------------------------------------------------------------------------
// based on doc/Si5345-44-42-D-RM.pdf p.144-150ff
//------------------------------------------------------------------------------
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"DESIGN_ID",0x6b,8,qty);           displayI2CQuantity(bus,dev,qty,STRING);
    //addI2CQuantity(bus,dev,I2CInterface::DATA,"PDN_HARD_RST_SYNC",0x1e,1,qty);   writableI2CQuantity(bus,dev,qty,true);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::max1617(const unsigned int bus, const unsigned int dev) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"MAX1617");

    unsigned int qty;

//------------------------------------------------------------------------------
// based on doc/MAX1617.pdf p.12ff
//------------------------------------------------------------------------------
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RLTS_LOCAL_TEMP",0x00,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RRTE_REMOTE_TEMP",0x01,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RSL_STATUS",0x02,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RCL_CONFIG",0x03,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RCRA_CONVERSION_RATE",0x04,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RLHN_LOCAL_THIGH_LIM",0x05,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RLLI_LOCAL_TLOW_LIM",0x06,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RRHI_REMOTE_THIGH_LIM",0x07,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RRLS_REMOTE_TLOW_LIM",0x08,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"WCA_CONFIG",0x09,1,qty);                 writableI2CQuantity(bus,dev,qty,true); readableI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"WCRW_CONVERSION_RATE",0x0a,1,qty);       writableI2CQuantity(bus,dev,qty,true); readableI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"WLHO_LOCAL_THIGH_LIM",0x0b,1,qty);       writableI2CQuantity(bus,dev,qty,true); readableI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"WLLM_LOCAL_TLOW_LIM",0x0c,1,qty);        writableI2CQuantity(bus,dev,qty,true); readableI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"WRHA_REMOTE_THIGH_LIM",0x0d,1,qty);      writableI2CQuantity(bus,dev,qty,true); readableI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"WRLN_REMOTE_TLOW_LIM",0x0e,1,qty);       writableI2CQuantity(bus,dev,qty,true); readableI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::BYTE,"OSHT_ONE_SHOT_COMMAND",0xff,0,qty);      writableI2CQuantity(bus,dev,qty,true); readableI2CQuantity(bus,dev,qty,false);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::max6695(const unsigned int bus, const unsigned int dev) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"MAX6695");

    unsigned int qty;

//------------------------------------------------------------------------------
// based on doc/MAX6695-MAX6696.pdf p.11-12ff
//------------------------------------------------------------------------------
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RLTS_LOCAL_TEMP",0x00,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RRTE_REMOTE_TEMP",0x01,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RSL_STATUS",0x02,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RCL_CONFIG",0x03,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RCRA_CONVERSION_RATE",0x04,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RLHN_LOCAL_THIGH_LIM",0x05,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RLLI_LOCAL_TLOW_LIM",0x06,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RRHI_REMOTE_THIGH_LIM",0x07,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"RRLS_REMOTE_TLOW_LIM",0x08,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"WCA_CONFIG",0x09,1,qty);                 writableI2CQuantity(bus,dev,qty,true); readableI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"WCRW_CONVERSION_RATE",0x0a,1,qty);       writableI2CQuantity(bus,dev,qty,true); readableI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"WLHO_LOCAL_THIGH_LIM",0x0b,1,qty);       writableI2CQuantity(bus,dev,qty,true); readableI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"WLLM_LOCAL_TLOW_LIM",0x0c,1,qty);        writableI2CQuantity(bus,dev,qty,true); readableI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"WRHA_REMOTE_THIGH_LIM",0x0d,1,qty);      writableI2CQuantity(bus,dev,qty,true); readableI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"WRLN_REMOTE_TLOW_LIM",0x0e,1,qty);       writableI2CQuantity(bus,dev,qty,true); readableI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::BYTE,"OSHT_ONE_SHOT_COMMAND",0xff,0,qty);      writableI2CQuantity(bus,dev,qty,true); readableI2CQuantity(bus,dev,qty,false);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::ftlf1323(const unsigned int bus, const unsigned int dev) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"FTLF1323");

    unsigned int qty;

//------------------------------------------------------------------------------
// based on doc/Finisar_ftlf1323p1xtl__specification_0.pdf p.6ff
// based on doc/an-2030_ddmi_for_sfp_rev_e2-20140404_updated.pdf p.3ff
//------------------------------------------------------------------------------
    // based on avagoSFP
    addI2CQuantity(bus,dev,I2CInterface::DATA,"SFP_DEVICE",0,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"SFP_FUNCTION",1,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"LC_OPT_CONN",2,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"INT_DISTANCE",7,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"SHORTWAVE_LASER",8,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"MULTIMODE_MEDIA",9,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"FC_PI_SPEED",10,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"SONET_ENCODE",11,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"LINK_DIST_150M",16,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"LINK_DIST_70M",17,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"VENDOR_NAME",20,16,qty);         displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"VENDOR_OUI",37,3,qty);           displayI2CQuantity(bus,dev,qty,WORD); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"VENDOR_PART_NAME",40,20,qty);    displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"LASER_WAVELENGTH",60,2,qty);     endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"CHECKSUM_0_62",63,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"HW_SFP_STATUS",65,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"VENDOR_SERIAL_NUMBER",68,16,qty); displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::IBLK,"VENDOR_DATE_CODE",84,8,qty);      displayI2CQuantity(bus,dev,qty,STRING);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"DIGITAL_DIAGNOSTICS",92,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"SW_SFP_STATUS",93,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"SFF_COMPLIANCE",94,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"CHECKSUM_64_94",95,1,qty);
    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::ftlf1323Extended(const unsigned int bus, const unsigned int dev) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // set type name
    typeI2CDevice(bus,dev,"FTLF1323xtd");

    unsigned int qty;

//------------------------------------------------------------------------------
// based on doc/Finisar_ftlf1323p1xtl__specification_0.pdf p.6ff
// based on doc/an-2030_ddmi_for_sfp_rev_e2-20140404_updated.pdf p.4ff
//------------------------------------------------------------------------------
    // based on avagoExtended
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TEMP_ALARM_HI",0,2,qty);         floatI2CQuantity(bus,dev,qty,true,1.0/256.0); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TEMP_ALARM_LO",2,2,qty);         floatI2CQuantity(bus,dev,qty,true,1.0/256.0); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TEMP_WARNING_HI",4,2,qty);       floatI2CQuantity(bus,dev,qty,true,1.0/256.0); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TEMP_WARNING_LO",6,2,qty);       floatI2CQuantity(bus,dev,qty,true,1.0/256.0); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC_ALARM_HI",8,2,qty);          floatI2CQuantity(bus,dev,qty,false,0.0001);   endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC_ALARM_LO",10,2,qty);         floatI2CQuantity(bus,dev,qty,false,0.0001);   endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC_WARNING_HI",12,2,qty);       floatI2CQuantity(bus,dev,qty,false,0.0001);   endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC_WARNING_LO",14,2,qty);       floatI2CQuantity(bus,dev,qty,false,0.0001);   endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXBIAS_ALARM_HI",16,2,qty);      floatI2CQuantity(bus,dev,qty,false,0.000002); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXBIAS_ALARM_LO",18,2,qty);      floatI2CQuantity(bus,dev,qty,false,0.000002); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXBIAS_WARNING_HI",20,2,qty);    floatI2CQuantity(bus,dev,qty,false,0.000002); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXBIAS_WARNING_LO",22,2,qty);    floatI2CQuantity(bus,dev,qty,false,0.000002); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXPOWER_ALARM_HI",24,2,qty);     floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXPOWER_ALARM_LO",26,2,qty);     floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXPOWER_WARNING_HI",28,2,qty);   floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXPOWER_WARNING_LO",30,2,qty);   floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RXPOWER_ALARM_HI",32,2,qty);     floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RXPOWER_ALARM_LO",34,2,qty);     floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RXPOWER_WARNING_HI",36,2,qty);   floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RXPOWER_WARNING_LO",38,2,qty);   floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"CHECKSUM_0_94",95,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TEMP",96,2,qty);                 floatI2CQuantity(bus,dev,qty,true,1.0/256.0); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"VCC",98,2,qty);                  floatI2CQuantity(bus,dev,qty,false,0.0001);   endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXBIAS",100,2,qty);              floatI2CQuantity(bus,dev,qty,false,0.000002); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"TXPOWER",102,2,qty);             floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"RXPOWER",104,2,qty);             floatI2CQuantity(bus,dev,qty,false,0.0000001); endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::DATA,"STATUS",110,1,qty);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"FLAGS_ALARM",112,2,qty);         endianI2CQuantity(bus,dev,qty,false);
    addI2CQuantity(bus,dev,I2CInterface::WORD,"FLAGS_WARNING",116,2,qty);       endianI2CQuantity(bus,dev,qty,false);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

void I2CNetwork::ltc3880FaultLog(const std::vector<unsigned int>& data, const unsigned int indt, std::ostream& os) {

    static const double TREAL = 0.0002;
    static struct {
        std::string operator() (const unsigned int src) const {
            switch(src) {
                case 0xff: return("MFR_FAULT_LOG_STORE"); break;
                case 0x00: return("TON_MAX_FAULT_CHAN0"); break;
                case 0x01: return("VOUT_OV_FAULT_CHAN0"); break;
                case 0x02: return("VOUT_UV_FAULT_CHAN0"); break;
                case 0x03: return("IOUT_OC_FAULT_CHAN0"); break;
                case 0x05: return("OT_FAULT_CHAN0"); break;
                case 0x06: return("UT_FAULT_CHAN0"); break;
                case 0x07: return("VIN_OV_FAULT_CHAN0"); break;
                case 0x0a: return("MFR_OT_FAULT_CHAN0"); break;
                case 0x10: return("TON_MAX_FAULT_CHAN1"); break;
                case 0x11: return("VOUT_OV_FAULT_CHAN1"); break;
                case 0x12: return("VOUT_UV_FAULT_CHAN1"); break;
                case 0x13: return("IOUT_OC_FAULT_CHAN1"); break;
                case 0x15: return("OT_FAULT_CHAN1"); break;
                case 0x16: return("UT_FAULT_CHAN1"); break;
                case 0x17: return("VIN_OV_FAULT_CHAN1"); break;
                case 0x1a: return("MFR_OT_FAULT_CHAN1"); break;
                default: return("Unknown source of fault");
            }
        }
    } source;
    static struct {
        double operator() (const uint16_t data) const {
            int mantissa(data & 0x07ff); if(mantissa & 0x0400) mantissa = mantissa - 0x0800;
            int exponent((data & 0xf800) >> 11); if(exponent & 0x0010) exponent = exponent - 0x20;
            return(static_cast<double>(mantissa) * std::pow(2.0,static_cast<double>(exponent)));
        }
    } linear11;

    char s[1024];

    if(data.empty()) {
        os << "FAULT LOG EMPTY !!!!";
        return;
    }

    long long unsigned int count(0);
    for(unsigned int i=0; i<6; i++) count |= static_cast<uint64_t>(data[1+i]) << (8 * i);
    double tsec = static_cast<double>(count) * TREAL;
    os << "-----------------------------------------------------------------------------------------------------------------------------------------------------------" <<std::endl;
    std::sprintf(s,"BYTES = %3zd, FAULT = \"%s\", TIME = 0x%012llx => %15.3f sec",data.size(),source(data[0]).c_str(),count,tsec); os << s << std::endl;

    double value;
    unsigned int idx(7);
    os << "-----------------------------------------------------------------------------------------------------------------------------------------------------------" <<std::endl;
    os << " VOUT_PEAK0  | VOUT_PEAK1 | IOUT_PEAK0 | IOUT_PEAK1 |  VIN_PEAK  |  TEMP1_P0  |  TEMP1_P1  |   TEMP2    |TEMP1_PEAK0 |TEMP1_PEAK1" << std::endl;
    os << "-----------------------------------------------------------------------------------------------------------------------------------------------------------" <<std::endl;
    value = static_cast<double>(getNextTwoByteWord(data,idx)) / 4096.0;
    std::sprintf(s,"  %10.6f",value); os << s;
    value = static_cast<double>(getNextTwoByteWord(data,idx)) / 4096.0;
    std::sprintf(s," | %10.6f",value); os << s;
    value = linear11(getNextTwoByteWord(data,idx));
    std::sprintf(s," | %10.6f",value); os << s;
    value = linear11(getNextTwoByteWord(data,idx));
    std::sprintf(s," | %10.6f",value); os << s;
    value = linear11(getNextTwoByteWord(data,idx));
    std::sprintf(s," | %10.6f",value); os << s;
    value = linear11(getNextTwoByteWord(data,idx));
    std::sprintf(s," | %10.6f",value); os << s;
    value = linear11(getNextTwoByteWord(data,idx));
    std::sprintf(s," | %10.6f",value); os << s;
    value = linear11(getNextTwoByteWord(data,idx));
    std::sprintf(s," | %10.6f",value); os << s;
    value = linear11(getNextTwoByteWord(data,idx));
    std::sprintf(s," | %10.6f",value); os << s;
    value = linear11(getNextTwoByteWord(data,idx));
    std::sprintf(s," | %10.6f",value); os << s << std::endl;
    os << "-----------------------------------------------------------------------------------------------------------------------------------------------------------" <<std::endl;
    os << " READ_VOUT0  | READ_VOUT1 | READ_IOUT0 | READ_IOUT1 |  READ_VIN  |  READ_IIN  | STAT_VOUT0 | STAT_VOUT1 | STAT_WORD0 | STAT_WORD1 | STAT_SPEC0 | STAT_SPEC1" << std::endl;
    os << "-----------------------------------------------------------------------------------------------------------------------------------------------------------" <<std::endl;
    for(unsigned int i=0; i<6; i++) {
        value = static_cast<double>(getNextTwoByteWord(data,idx)) / 4096.0;
        std::sprintf(s,"  %10.6f",value); os << s;
        value = static_cast<double>(getNextTwoByteWord(data,idx)) / 4096.0;
        std::sprintf(s," | %10.6f",value); os << s;
        value = linear11(getNextTwoByteWord(data,idx));
        std::sprintf(s," | %10.6f",value); os << s;
        value = linear11(getNextTwoByteWord(data,idx));
        std::sprintf(s," | %10.6f",value); os << s;
        value = linear11(getNextTwoByteWord(data,idx));
        std::sprintf(s," | %10.6f",value); os << s;
        value = linear11(getNextTwoByteWord(data,idx));
        std::sprintf(s," | %10.6f",value); os << s;
        std::sprintf(s," |    0x%02x   ",data[idx++]); os << s;
        std::sprintf(s," |    0x%02x   ",data[idx++]); os << s;
        std::sprintf(s," |   0x%04x  ",getNextTwoByteWord(data,idx)); os << s;
        std::sprintf(s," |   0x%04x  ",getNextTwoByteWord(data,idx)); os << s;
        std::sprintf(s," |    0x%02x   ",data[idx++]); os << s;
        std::sprintf(s," |    0x%02x   ",data[idx++]); os << s << std::endl;
    }
    os << "-----------------------------------------------------------------------------------------------------------------------------------------------------------" <<std::endl;
}

//------------------------------------------------------------------------------

void I2CNetwork::ltm4676FaultLog(const std::vector<unsigned int>& data, const unsigned int indt, std::ostream& os) {

    static const double TREAL = 0.0002;
    static struct {
        std::string operator() (const unsigned int src) const {
            switch(src) {
                case 0xff: return("MFR_FAULT_LOG_STORE"); break;
                case 0x00: return("TON_MAX_FAULT_CHAN0"); break;
                case 0x01: return("VOUT_OV_FAULT_CHAN0"); break;
                case 0x02: return("VOUT_UV_FAULT_CHAN0"); break;
                case 0x03: return("IOUT_OC_FAULT_CHAN0"); break;
                case 0x05: return("OT_FAULT_CHAN0"); break;
                case 0x06: return("UT_FAULT_CHAN0"); break;
                case 0x07: return("VIN_OV_FAULT_CHAN0"); break;
                case 0x0a: return("MFR_OT_FAULT_CHAN0"); break;
                case 0x10: return("TON_MAX_FAULT_CHAN1"); break;
                case 0x11: return("VOUT_OV_FAULT_CHAN1"); break;
                case 0x12: return("VOUT_UV_FAULT_CHAN1"); break;
                case 0x13: return("IOUT_OC_FAULT_CHAN1"); break;
                case 0x15: return("OT_FAULT_CHAN1"); break;
                case 0x16: return("UT_FAULT_CHAN1"); break;
                case 0x17: return("VIN_OV_FAULT_CHAN1"); break;
                case 0x1a: return("MFR_OT_FAULT_CHAN1"); break;
                default: return("Unknown source of fault");
            }
        }
    } source;
    static struct {
        double operator() (const uint16_t data) const {
            int mantissa(data & 0x07ff); if(mantissa & 0x0400) mantissa = mantissa - 0x0800;
            int exponent((data & 0xf800) >> 11); if(exponent & 0x0010) exponent = exponent - 0x20;
            return(static_cast<double>(mantissa) * std::pow(2.0,static_cast<double>(exponent)));
        }
    } linear11;

    char s[1024];

    if(data.empty()) {
        os << "FAULT LOG EMPTY !!!!";
        return;
    }

//------------------------------------------------------------------------------
// based on doc/LTM4676afa.pdf p.127ff
//------------------------------------------------------------------------------

    long long unsigned int count(0);
    for(unsigned int i=0; i<6; i++) count |= static_cast<uint64_t>(data[5+i]) << (8 * i);
    double tsec = static_cast<double>(count) * TREAL;
    os << "-----------------------------------------------------------------------------------------------------------------------------------------------------------" <<std::endl;
    std::sprintf(s,"BYTES = %3zd, FAULT = \"%s\", TIME = 0x%012llx => %15.3f sec",data.size(),source(data[4]).c_str(),count,tsec); os << s << std::endl;

    double value;
    unsigned int idx(11);
    os << "-----------------------------------------------------------------------------------------------------------------------------------------------------------" <<std::endl;
    os << " VOUT_PEAK0  | VOUT_PEAK1 | IOUT_PEAK0 | IOUT_PEAK1 |  VIN_PEAK  |  TEMP1_P0  |  TEMP1_P1  |   TEMP2" << std::endl;
    os << "-----------------------------------------------------------------------------------------------------------------------------------------------------------" <<std::endl;
    value = static_cast<double>(getNextTwoByteWord(data,idx)) / 4096.0;
    std::sprintf(s,"  %10.6f",value); os << s;
    value = static_cast<double>(getNextTwoByteWord(data,idx)) / 4096.0;
    std::sprintf(s," | %10.6f",value); os << s;
    value = linear11(getNextTwoByteWord(data,idx));
    std::sprintf(s," | %10.6f",value); os << s;
    value = linear11(getNextTwoByteWord(data,idx));
    std::sprintf(s," | %10.6f",value); os << s;
    value = linear11(getNextTwoByteWord(data,idx));
    std::sprintf(s," | %10.6f",value); os << s;
    value = linear11(getNextTwoByteWord(data,idx));
    std::sprintf(s," | %10.6f",value); os << s;
    value = linear11(getNextTwoByteWord(data,idx));
    std::sprintf(s," | %10.6f",value); os << s;
    value = linear11(getNextTwoByteWord(data,idx));
    std::sprintf(s," | %10.6f",value); os << s << std::endl;
    os << "-----------------------------------------------------------------------------------------------------------------------------------------------------------" <<std::endl;
    os << " READ_VOUT0  | READ_VOUT1 | READ_IOUT0 | READ_IOUT1 |  READ_VIN  |  READ_IIN  | STAT_VOUT0 | STAT_VOUT1 | STAT_WORD0 | STAT_WORD1 | STAT_SPEC0 | STAT_SPEC1" << std::endl;
    os << "-----------------------------------------------------------------------------------------------------------------------------------------------------------" <<std::endl;
    for(unsigned int i=0; i<6; i++) {
        value = static_cast<double>(getNextTwoByteWord(data,idx)) / 4096.0;
        std::sprintf(s,"  %10.6f",value); os << s;
        value = static_cast<double>(getNextTwoByteWord(data,idx)) / 4096.0;
        std::sprintf(s," | %10.6f",value); os << s;
        value = linear11(getNextTwoByteWord(data,idx));
        std::sprintf(s," | %10.6f",value); os << s;
        value = linear11(getNextTwoByteWord(data,idx));
        std::sprintf(s," | %10.6f",value); os << s;
        value = linear11(getNextTwoByteWord(data,idx));
        std::sprintf(s," | %10.6f",value); os << s;
        value = linear11(getNextTwoByteWord(data,idx));
        std::sprintf(s," | %10.6f",value); os << s;
        std::sprintf(s," |    0x%02x   ",data[idx++]); os << s;
        std::sprintf(s," |    0x%02x   ",data[idx++]); os << s;
        std::sprintf(s," |   0x%04x  ",getNextTwoByteWord(data,idx)); os << s;
        std::sprintf(s," |   0x%04x  ",getNextTwoByteWord(data,idx)); os << s;
        std::sprintf(s," |    0x%02x   ",data[idx++]); os << s;
        std::sprintf(s," |    0x%02x   ",data[idx++]); os << s << std::endl;
    }
    os << "-----------------------------------------------------------------------------------------------------------------------------------------------------------" <<std::endl;
}

//------------------------------------------------------------------------------

uint16_t I2CNetwork::getNextTwoByteWord(const std::vector<unsigned int>& data, unsigned int& idx) {

    uint16_t dats;

    dats = (data[idx] &0x000000ff) << 8;
    idx++;
    dats = dats | (data[idx] & 0x000000ff);
    idx++;

    return(dats);
}

//------------------------------------------------------------------------------

int I2CNetwork::addI2CQuantity(const unsigned int bus, const unsigned int dev, const I2CInterface::TRANSFER_TYPE ttyp, const std::string& name, const unsigned int offs, const unsigned int numb, unsigned int& rslt) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // add I2C quantity
    I2CQuantity* qty = new I2CQuantity(ttyp,name,offs,numb);
    if(m_busses[bus]->getI2CDevice(dev)->getPmbus()) {
        qty->signedI2CQuantity(true,true);
    }
    m_busses[bus]->getI2CDevice(dev)->addI2CQuantity(qty,rslt);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::readableI2CQuantity(const unsigned int bus, const unsigned int dev, const unsigned int qty, const bool rdb) {

    // check I2C bus/device/quantity number
    if(!exists(bus,dev,qty)) {
        CERR("I2C bus/device/quantity %d/%d/%d does not exist",bus,dev,qty);
        return(WRONGPAR);
    }

    // set I2C quantity readability
    m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->readableI2CQuantity(rdb);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::writableI2CQuantity(const unsigned int bus, const unsigned int dev, const unsigned int qty, const bool wtb) {

    // check I2C bus/device/quantity number
    if(!exists(bus,dev,qty)) {
        CERR("I2C bus/device/quantity %d/%d/%d does not exist",bus,dev,qty);
        return(WRONGPAR);
    }

    // set I2C quantity writability
    m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->writableI2CQuantity(wtb);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::displayI2CQuantity(const unsigned int bus, const unsigned int dev, const unsigned int qty, const DISPLAY_TYPE dtyp) {

    // check I2C bus/device/quantity number
    if(!exists(bus,dev,qty)) {
        CERR("I2C bus/device/quantity %d/%d/%d does not exist",bus,dev,qty);
        return(WRONGPAR);
    }

    // set I2C quantity display type
    m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->displayI2CQuantity(dtyp);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::floatI2CQuantity(const unsigned int bus, const unsigned int dev, const unsigned int qty, const bool man, const float fct, const float off) {

    // check I2C bus/device/quantity number
    if(!exists(bus,dev,qty)) {
        CERR("I2C bus/device/quantity %d/%d/%d does not exist",bus,dev,qty);
        return(WRONGPAR);
    }

    // set I2C quantity display type
    m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->displayI2CQuantity(FLOAT);

    // set I2C quantity signedness
    m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->signedI2CQuantity(man,false);

    // set I2C quantity conversion
    m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->convertI2CQuantity(fct,off);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::endianI2CQuantity(const unsigned int bus, const unsigned int dev, const unsigned int qty, const bool ltl) {

    // check I2C bus/device/quantity number
    if(!exists(bus,dev,qty)) {
        CERR("I2C bus/device/quantity %d/%d/%d does not exist",bus,dev,qty);
        return(WRONGPAR);
    }

    // set I2C quantity endianess
    m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->endianI2CQuantity(ltl);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::signedI2CQuantity(const unsigned int bus, const unsigned int dev, const unsigned int qty, const bool man, const bool exp) {

    // check I2C bus/device/quantity number
    if(!exists(bus,dev,qty)) {
        CERR("I2C bus/device/quantity %d/%d/%d does not exist",bus,dev,qty);
        return(WRONGPAR);
    }

    // set I2C quantity signedness
    m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->signedI2CQuantity(man,exp);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::convertI2CQuantity(const unsigned int bus, const unsigned int dev, const unsigned int qty, const float fct, const float off) {

    // check I2C bus/device/quantity number
    if(!exists(bus,dev,qty)) {
        CERR("I2C bus/device/quantity %d/%d/%d does not exist",bus,dev,qty);
        return(WRONGPAR);
    }

    // set I2C quantity conversion
    m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->convertI2CQuantity(fct,off);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::exponentI2CQuantity(const unsigned int bus, const unsigned int dev, const unsigned int qty, const unsigned int wdt, const int off) {

    // check I2C bus/device/quantity number
    if(!exists(bus,dev,qty)) {
        CERR("I2C bus/device/quantity %d/%d/%d does not exist",bus,dev,qty);
        return(WRONGPAR);
    }

    // set I2C quantity exponent
    m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->exponentI2CQuantity(wdt,off);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::maskI2CQuantity(const unsigned int bus, const unsigned int dev, const unsigned int qty, const unsigned int msk) {

    // check I2C bus/device/quantity number
    if(!exists(bus,dev,qty)) {
        CERR("I2C bus/device/quantity %d/%d/%d does not exist",bus,dev,qty);
        return(WRONGPAR);
    }

    // set I2C quantity conversion
    m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->maskI2CQuantity(msk);

    return(SUCCESS);
}

//------------------------------------------------------------------------------
// READ I2C data locally
//------------------------------------------------------------------------------

int I2CNetwork::localRead(const unsigned int bus, const unsigned int dev, const unsigned int qty, unsigned int& num, uint8_t data[]) {

    num = m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getNumber();
    return(localRead(bus,dev,qty,0,num,data));
}

//------------------------------------------------------------------------------

int I2CNetwork::localRead(const unsigned int bus, const unsigned int dev, const unsigned int qty, const unsigned int off, unsigned int& num, uint8_t data[]) {

    int rtnv(0);
    I2CInterface* ii2c(m_busses[bus]->getI2C());
    I2CDevice* idev(m_busses[bus]->getI2CDevice(dev));
    unsigned int iswc(idev->getSwitchAddress()), offs;
    uint8_t datc[I2CInterface::MAX_BLOCK];

    ii2c->lockBus();

    // write I2C switch if necessary
    if(iswc <= I2CInterface::DATA_MASK) {
        offs = idev->getSwitchSelect();
        unsigned int numb(0);
        CDBG("write switch: address = 0x%02x, select = 0x%02x, numb = %d",iswc,offs,numb);
        if((rtnv = ii2c->DataWrite(iswc,offs,numb,datc,I2CInterface::BYTE)) != SUCCESS) {
            CERR("writing I2C bus/device %d/%d, switch 0x%02x, select 0x%02x",bus,dev,iswc,idev->getSwitchSelect());
            ii2c->unlockBus();
            return(rtnv);
        }
    }

    unsigned int addr(idev->getAddress());

    // write I2C page select if necessary
    if(idev->getPageFlag()) {
        offs = idev->getPageOffset();
        datc[0] = idev->getPageSelect();
        unsigned int numb(1);
        I2CInterface::TRANSFER_TYPE ttyp(I2CInterface::DATA);
        CDBG("write page: address = 0x%02x, offset = 0x%02x, select = 0x%02x, numb = %d",addr,offs,datc[0],numb);

        // add CRC(PMBUS) if necessary
        if(idev->getPmbus()) {
            ttyp = I2CInterface::IBLK;
            uint8_t crc = I2CInterface::crc(addr,offs,datc,numb,0,false);
            CDBG("W%s: numb = %d, CRC = 0x%02x (%d)",I2CInterface::TRANSFER_TYPE_NAME[ttyp].c_str(),numb,crc,crc);
            datc[numb++] = crc;
#ifdef DEBUG
            crc = I2CInterface::crc(addr,offs,datc,numb,0);
            CDBG("R%s: numb = %d, CRC = 0x%02x (%d) - CHECK",I2CInterface::TRANSFER_TYPE_NAME[ttyp].c_str(),numb,crc,crc);
#endif
        }

        // write I2C page select
        if((rtnv = ii2c->DataWrite(addr,offs,numb,datc,ttyp)) != SUCCESS) {
            CERR("writing I2C bus/device %d/%d, page 0x%02x, offset 0x%02x, select 0x%02x",bus,dev,addr,offs,datc[0]);
            ii2c->unlockBus();
            return(rtnv);
        }
    }

    offs = idev->getI2CQuantity(qty)->getOffset() + off;
    I2CInterface::TRANSFER_TYPE ttyp = idev->getI2CQuantity(qty)->getTransferType();

    // consider CRC(PMBUS) if necessary
    if(idev->getPmbus() and (ttyp != I2CInterface::SBLK) and (ttyp != I2CInterface::PBLK)) {
        if((ttyp == I2CInterface::BYTE) or (ttyp == I2CInterface::DATA) or (ttyp == I2CInterface::WORD)) ttyp = I2CInterface::IBLK;
        num++;
    }

    // read I2C data
    CDBG("read data: address = 0x%02x, offset = 0x%02x, num = %d",addr,offs,num);
    if((rtnv = ii2c->DataRead(addr,offs,num,data,ttyp)) != SUCCESS) {
        CERR("reading I2C bus/device/quantity %d/%d/%d",bus,dev,qty);
        ii2c->unlockBus();
        return(rtnv);
    }

    ii2c->unlockBus();

    // check CRC(PMBUS) necessary
    if(idev->getPmbus() and (ttyp != I2CInterface::SBLK) and (ttyp != I2CInterface::PBLK)) {
        unsigned int crc = I2CInterface::crc(addr,offs,data,num,0);
        CDBG("R%s: numb = %d, CRC = 0x%02x (%d)",I2CInterface::TRANSFER_TYPE_NAME[ttyp].c_str(),num,crc,crc);
        if(crc) {
            CERR("CRC(PMBUS) error reading I2C bus/device/quantity %d/%d/%d, numb = %d, CRC = 0x%02x (%d)",bus,dev,qty,num,crc,crc);
            return(FAILURE);
        }
        num--;
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------
// READ I2C data as WORD
//------------------------------------------------------------------------------

int I2CNetwork::read(const unsigned int bus, const unsigned int dev, const unsigned int qty, unsigned int& data, const bool valid) {

    // check I2C bus/device/quantity number
    if(!valid) {
        if(!exists(bus,dev,qty)) {
            CERR("I2C bus/device/quantity %d/%d/%d does not exist",bus,dev,qty);
            return(WRONGPAR);
        }
    }

    int rtnv(0);

    // read I2C remote data if necessary
    if(m_busses[bus]->getI2C()->remote()) {
        if((rtnv = m_busses[bus]->getI2C()->DataRead(dev,qty,data)) != SUCCESS) {
            CERR("reading Bus \"%s\", Device \"%s\", Quantity \"%s\" - REMOTE",m_busses[bus]->getName().c_str(),m_busses[bus]->getI2CDevice(dev)->getName().c_str(),m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getName().c_str());
        }
        return(rtnv);
    }

    uint8_t datc[I2CInterface::MAX_BLOCK];
    unsigned int num(m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getNumber());

    // for PROC
    if(m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getTransferType() == I2CInterface::PROC) {
        datc[0] = data & 0xff;
        datc[1] = (data >> 8) & 0xff;
    }

    // read I2C local data
    if((rtnv = localRead(bus,dev,qty,num,datc)) != SUCCESS) {
        CERR("reading Bus \"%s\", Device \"%s\", Quantity \"%s\"",m_busses[bus]->getName().c_str(),m_busses[bus]->getI2CDevice(dev)->getName().c_str(),m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getName().c_str());
        return(rtnv);
    }

    // convert to word
    data = m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->bytes2word(datc);
    CDBG("data = 0x%08x",data);

    return(SUCCESS);
}

//------------------------------------------------------------------------------
// READ I2C data as FLOAT
//------------------------------------------------------------------------------

int I2CNetwork::read(const unsigned int bus, const unsigned int dev, const unsigned int qty, float& data, const bool valid) {

    // check I2C bus/device/quantity number
    if(!valid) {
        if(!exists(bus,dev,qty)) {
            CERR("I2C bus/device/quantity %d/%d/%d does not exist",bus,dev,qty);
            return(WRONGPAR);
        }
    }

    int rtnv(0);

    // read I2C remote data if necessary
    if(m_busses[bus]->getI2C()->remote()) {
        if((rtnv = m_busses[bus]->getI2C()->DataRead(dev,qty,data)) != SUCCESS) {
            CERR("reading Bus \"%s\", Device \"%s\", Quantity \"%s\" - REMOTE",m_busses[bus]->getName().c_str(),m_busses[bus]->getI2CDevice(dev)->getName().c_str(),m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getName().c_str());
        }
        return(rtnv);
    }

    uint8_t datc[I2CInterface::MAX_BLOCK];
    unsigned int num(m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getNumber());

    // read I2C local data
    if((rtnv = localRead(bus,dev,qty,num,datc)) != SUCCESS) {
        CERR("reading Bus \"%s\", Device \"%s\", Quantity \"%s\"",m_busses[bus]->getName().c_str(),m_busses[bus]->getI2CDevice(dev)->getName().c_str(),m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getName().c_str());
        return(rtnv);
    }

    // convert to float
    unsigned int word = m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->bytes2word(datc);
    CDBG("word = 0x%08x",word);
    data = m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->word2float(word);

    return(SUCCESS);
}

//------------------------------------------------------------------------------
// READ I2C data as STRING
//------------------------------------------------------------------------------

int I2CNetwork::read(const unsigned int bus, const unsigned int dev, const unsigned int qty, std::string& data, const bool valid) {

    // check I2C bus/device/quantity number
    if(!valid) {
        if(!exists(bus,dev,qty)) {
            CERR("I2C bus/device/quantity %d/%d/%d does not exist",bus,dev,qty);
            return(WRONGPAR);
        }
    }

    int rtnv(0);

    // read I2C remote data if necessary
    if(m_busses[bus]->getI2C()->remote()) {
        if((rtnv = m_busses[bus]->getI2C()->DataRead(dev,qty,data)) != SUCCESS) {
            CERR("reading Bus \"%s\", Device \"%s\", Quantity \"%s\" - REMOTE",m_busses[bus]->getName().c_str(),m_busses[bus]->getI2CDevice(dev)->getName().c_str(),m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getName().c_str());
        }
        return(rtnv);
    }

    uint8_t datc[I2CInterface::MAX_BLOCK];
    unsigned int num(m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getNumber());

    // read I2C local data
    if((rtnv = localRead(bus,dev,qty,num,datc)) != SUCCESS) {
        CERR("reading Bus \"%s\", Device \"%s\", Quantity \"%s\"",m_busses[bus]->getName().c_str(),m_busses[bus]->getI2CDevice(dev)->getName().c_str(),m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getName().c_str());
        return(rtnv);
    }

    // convert to string
    std::ostringstream buf;
    for(unsigned int i=0; i<num; i++) buf << (char)(datc[i]&I2CInterface::DATA_MASK);
    data = buf.str();

    return(SUCCESS);
}

//------------------------------------------------------------------------------
// READ I2C data as MEMORY
//------------------------------------------------------------------------------

int I2CNetwork::read(const unsigned int bus, const unsigned int dev, const unsigned int qty, std::vector<unsigned int>& data, const bool valid) {

    unsigned int off(0), num(m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getNumber());
    return(read(bus,dev,qty,off,num,data,valid));
}

//------------------------------------------------------------------------------

int I2CNetwork::read(const unsigned int bus, const unsigned int dev, const unsigned int qty, const unsigned int off, const unsigned int num, std::vector<unsigned int>& data, const bool valid) {

    // check I2C bus/device/quantity number
    if(!valid) {
        if(!exists(bus,dev,qty)) {
            CERR("I2C bus/device/quantity %d/%d/%d does not exist",bus,dev,qty);
            return(WRONGPAR);
        }
    }

    unsigned int len = m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getNumber();

    // check offset
    if(off >= len) {
        CERR("I2C bus/device/quantity %d/%d/%d offset %d not in allowed range [0..%d]",bus,dev,qty,off,len-1);
        return(WRONGPAR);
    }
    // check length
    if((off+num) > len) {
        CERR("I2C bus/device/quantity %d/%d/%d length %d not in allowed range [0..%d]",bus,dev,qty,num,len-off);
        return(WRONGPAR);
    }

    int rtnv(0);

    // read I2C remote data if necessary
    if(m_busses[bus]->getI2C()->remote()) {
        if((rtnv = m_busses[bus]->getI2C()->DataRead(dev,qty,off,num,data)) != SUCCESS) {
            CERR("reading Bus \"%s\", Device \"%s\", Quantity \"%s\" - REMOTE",m_busses[bus]->getName().c_str(),m_busses[bus]->getI2CDevice(dev)->getName().c_str(),m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getName().c_str());
        }
        return(rtnv);
    }

    uint8_t datc[I2CInterface::MAX_BLOCK];
    len = num;

    // read I2C local data
    if((rtnv = localRead(bus,dev,qty,off,len,datc)) != SUCCESS) {
        CERR("reading I2C bus/device/quantity %d/%d/%d",bus,dev,qty);
        return(rtnv);
    }

    // convert to vector
    data.resize(len);
    for(unsigned int i=0; i<len; i++) data[i] = (datc[i]&I2CInterface::DATA_MASK);

    return(SUCCESS);
}

//------------------------------------------------------------------------------
// WRITE I2C data locally
//------------------------------------------------------------------------------

int I2CNetwork::localWrite(const unsigned int bus, const unsigned int dev, const unsigned int qty, unsigned int& num, uint8_t data[]) {

    num = m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getNumber();
    return(localWrite(bus,dev,qty,0,num,data));
}

//------------------------------------------------------------------------------

int I2CNetwork::localWrite(const unsigned int bus, const unsigned int dev, const unsigned int qty, const unsigned int off, unsigned int& num, uint8_t data[]) {

    int rtnv(0);
    I2CInterface* ii2c(m_busses[bus]->getI2C());
    I2CDevice* idev(m_busses[bus]->getI2CDevice(dev));
    unsigned int iswc(idev->getSwitchAddress()), offs;
    uint8_t datc[I2CInterface::MAX_BLOCK];

    ii2c->lockBus();

    // write I2C switch if necessary
    if(iswc <= I2CInterface::DATA_MASK) {
        offs = idev->getSwitchSelect();
        unsigned int numb(0);
        CDBG("write switch: address = 0x%02x, select = 0x%02x, numb = %d",iswc,offs,numb);
        if((rtnv = ii2c->DataWrite(iswc,offs,numb,datc,I2CInterface::BYTE)) != SUCCESS) {
            CERR("writing I2C bus/device %d/%d, switch 0x%02x, select 0x%02x",bus,dev,iswc,idev->getSwitchSelect());
            ii2c->unlockBus();
            return(rtnv);
        }
    }

    unsigned int addr(idev->getAddress());

    // write I2C page select if necessary
    if(idev->getPageFlag()) {
        offs = idev->getPageOffset();
        datc[0] = idev->getPageSelect();
        unsigned int numb(1);
        I2CInterface::TRANSFER_TYPE ttyp(I2CInterface::DATA);
        CDBG("write page: address = 0x%02x, offset = 0x%02x, select = 0x%02x, numb = %d",addr,offs,datc[0],numb);

        // add CRC(PMBUS) if necessary
        if(idev->getPmbus()) {
            ttyp = I2CInterface::IBLK;
            uint8_t crc = I2CInterface::crc(addr,offs,datc,numb,0,false);
            CDBG("W%s: numb = %d, CRC = 0x%02x (%d)",I2CInterface::TRANSFER_TYPE_NAME[ttyp].c_str(),numb,crc,crc);
            datc[numb++] = crc;
#ifdef DEBUG
            crc = I2CInterface::crc(addr,offs,datc,numb,0);
            CDBG("W%s: numb = %d, CRC = 0x%02x (%d) - CHECK",I2CInterface::TRANSFER_TYPE_NAME[ttyp].c_str(),numb,crc,crc);
#endif
        }

        // write I2C page select
        if((rtnv = ii2c->DataWrite(addr,offs,numb,datc,ttyp)) != SUCCESS) {
            CERR("writing I2C bus/device %d/%d, page 0x%02x, offset 0x%02x, select 0x%02x",bus,dev,addr,offs,datc[0]);
            ii2c->unlockBus();
            return(rtnv);
        }
    }

    offs = idev->getI2CQuantity(qty)->getOffset() + off;
    I2CInterface::TRANSFER_TYPE ttyp = idev->getI2CQuantity(qty)->getTransferType();

    // add CRC(PMBUS) if necessary
    if(idev->getPmbus() and (ttyp != I2CInterface::SBLK) and (ttyp != I2CInterface::PBLK)) {
        if((ttyp == I2CInterface::BYTE) or (ttyp == I2CInterface::DATA) or (ttyp == I2CInterface::WORD)) ttyp = I2CInterface::IBLK;
        uint8_t crc = I2CInterface::crc(addr,offs,data,num,0,false);
        CDBG("W%s: num = %d, CRC = 0x%02x (%d)",I2CInterface::TRANSFER_TYPE_NAME[ttyp].c_str(),num,crc,crc);
        data[num++] = crc;
#ifdef DEBUG
        crc = I2CInterface::crc(addr,offs,data,num,0);
        CDBG("R%s: num = %d, CRC = 0x%02x (%d) - CHECK",I2CInterface::TRANSFER_TYPE_NAME[ttyp].c_str(),num,crc,crc);
#endif
    }

    // write I2C data
    CDBG("write data: address = 0x%02x, offset = 0x%02x, num = %d",addr,offs,num);
    if((rtnv = ii2c->DataWrite(addr,offs,num,data,ttyp)) != SUCCESS) {
        CERR("writing I2C bus/device/quantity %d/%d/%d",bus,dev,qty);
        ii2c->unlockBus();
        return(rtnv);
    }

    ii2c->unlockBus();

    return(SUCCESS);
}

//------------------------------------------------------------------------------
// WRITE I2C data as WORD
//------------------------------------------------------------------------------

int I2CNetwork::write(const unsigned int bus, const unsigned int dev, const unsigned int qty, const unsigned int data, const bool valid) {

    // check I2C bus/device/quantity number
    if(!valid) {
        if(!exists(bus,dev,qty)) {
            CERR("I2C bus/device/quantity %d/%d/%d does not exist",bus,dev,qty);
            return(WRONGPAR);
        }
    }

    // check I2C bus/device/quantity writability
    bool wtb;
    writable(bus,dev,qty,wtb);
    if(!wtb) {
        CERR("I2C bus/device/quantity %d/%d/%d is NOT WRITABLE",bus,dev,qty);
        return(WRONGPAR);
    }

    int rtnv(0);
    I2CInterface* ii2c(m_busses[bus]->getI2C());

    // write I2C remote data if necessary
    if(ii2c->remote()) {
        if((rtnv = m_busses[bus]->getI2C()->DataWrite(dev,qty,data)) != SUCCESS) {
            CERR("writing Bus \"%s\", Device \"%s\", Quantity \"%s\" - REMOTE",m_busses[bus]->getName().c_str(),m_busses[bus]->getI2CDevice(dev)->getName().c_str(),m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getName().c_str());
        }
        return(rtnv);
    }

    uint8_t datc[I2CInterface::MAX_BLOCK];
    unsigned int num(m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getNumber());

    // convert from word
    m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->word2bytes(data,num,datc);

    // write I2C local data
    if((rtnv = localWrite(bus,dev,qty,num,datc)) != SUCCESS) {
        CERR("writing Bus \"%s\", Device \"%s\", Quantity \"%s\"",m_busses[bus]->getName().c_str(),m_busses[bus]->getI2CDevice(dev)->getName().c_str(),m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getName().c_str());
        return(rtnv);
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------
// WRITE I2C data as FLOAT
//------------------------------------------------------------------------------

int I2CNetwork::write(const unsigned int bus, const unsigned int dev, const unsigned int qty, const float data, const bool valid) {

    // check I2C bus/device/quantity number
    if(!valid) {
        if(!exists(bus,dev,qty)) {
            CERR("I2C bus/device/quantity %d/%d/%d does not exist",bus,dev,qty);
            return(WRONGPAR);
        }
    }

    // check I2C bus/device/quantity writability
    bool wtb;
    writable(bus,dev,qty,wtb);
    if(!wtb) {
        CERR("I2C bus/device/quantity %d/%d/%d is NOT WRITABLE",bus,dev,qty);
        return(WRONGPAR);
    }

    int rtnv(0);
    I2CInterface* ii2c(m_busses[bus]->getI2C());

    // write I2C remote data if necessary
    if(ii2c->remote()) {
        CERR("writing Bus \"%s\", Device \"%s\", Quantity \"%s\" as FLOAT not implemented - REMOTE",m_busses[bus]->getName().c_str(),m_busses[bus]->getI2CDevice(dev)->getName().c_str(),m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getName().c_str());
        rtnv = I2CInterface::NOTIMPL;
        return(rtnv);
    }

    uint8_t datc[I2CInterface::MAX_BLOCK];
    unsigned int num(m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getNumber());

    // convert from float
    unsigned int word = m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->float2word(data);
    m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->word2bytes(word,num,datc);

    CDBG("word = 0x%08x, revert = %f",word,m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->word2float(word));

    // write I2C local data
    if((rtnv = localWrite(bus,dev,qty,num,datc)) != SUCCESS) {
        CERR("writing Bus \"%s\", Device \"%s\", Quantity \"%s\"",m_busses[bus]->getName().c_str(),m_busses[bus]->getI2CDevice(dev)->getName().c_str(),m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getName().c_str());
        return(rtnv);
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------
// WRITE I2C data as STRING
//------------------------------------------------------------------------------

int I2CNetwork::write(const unsigned int bus, const unsigned int dev, const unsigned int qty, const std::string& data, const bool valid) {

    // check I2C bus/device/quantity number
    if(!valid) {
        if(!exists(bus,dev,qty)) {
            CERR("I2C bus/device/quantity %d/%d/%d does not exist",bus,dev,qty);
            return(WRONGPAR);
        }
    }

    // check I2C bus/device/quantity writability
    bool wtb;
    writable(bus,dev,qty,wtb);
    if(!wtb) {
        CERR("I2C bus/device/quantity %d/%d/%d is NOT WRITABLE",bus,dev,qty);
        return(WRONGPAR);
    }

    int rtnv(0);
    I2CInterface* ii2c(m_busses[bus]->getI2C());

    // write I2C remote data if necessary
    if(ii2c->remote()) {
        CERR("writing Bus \"%s\", Device \"%s\", Quantity \"%s\" as STRING not implemented - REMOTE",m_busses[bus]->getName().c_str(),m_busses[bus]->getI2CDevice(dev)->getName().c_str(),m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getName().c_str());
        rtnv = I2CInterface::NOTIMPL;
        return(rtnv);
    }

    uint8_t datc[I2CInterface::MAX_BLOCK];
    unsigned int num(m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getNumber());

    // convert from string
    for(unsigned int i=0; i<data.size(); i++) datc[i] = (uint8_t)data[i];

    // write I2C local data
    if((rtnv = localWrite(bus,dev,qty,num,datc)) != SUCCESS) {
        CERR("writing Bus \"%s\", Device \"%s\", Quantity \"%s\"",m_busses[bus]->getName().c_str(),m_busses[bus]->getI2CDevice(dev)->getName().c_str(),m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getName().c_str());
        return(rtnv);
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------
// WRITE I2C data as MEMORY
//------------------------------------------------------------------------------

int I2CNetwork::write(const unsigned int bus, const unsigned int dev, const unsigned int qty, const std::vector<unsigned int>& data, const bool valid) {

    unsigned int off(0);
    return(write(bus,dev,qty,off,data,valid));
}

//------------------------------------------------------------------------------

int I2CNetwork::write(const unsigned int bus, const unsigned int dev, const unsigned int qty, const unsigned int off, const std::vector<unsigned int>& data, const bool valid) {

    // check I2C bus/device/quantity number
    if(!valid) {
        if(!exists(bus,dev,qty)) {
            CERR("I2C bus/device/quantity %d/%d/%d does not exist",bus,dev,qty);
            return(WRONGPAR);
        }
    }

    // check I2C bus/device/quantity writability
    bool wtb;
    writable(bus,dev,qty,wtb);
    if(!wtb) {
        CERR("I2C bus/device/quantity %d/%d/%d is NOT WRITABLE",bus,dev,qty);
        return(WRONGPAR);
    }

    unsigned int num = data.size();
    unsigned int len = m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getNumber();

    // check offset
    if(off >= len) {
        CERR("I2C bus/device/quantity %d/%d/%d offset %d not in allowed range [0..%d]",bus,dev,qty,off,len-1);
        return(WRONGPAR);
    }
    // check length
    if((off+num) > len) {
        CERR("I2C bus/device/quantity %d/%d/%d length %d not in allowed range [0..%d]",bus,dev,qty,num,len-off);
        return(WRONGPAR);
    }

    int rtnv(0);
    I2CInterface* ii2c(m_busses[bus]->getI2C());

    // write I2C remote data if necessary
    if(ii2c->remote()) {
        CERR("writing Bus \"%s\", Device \"%s\", Quantity \"%s\" as MEMORY not implemented - REMOTE",m_busses[bus]->getName().c_str(),m_busses[bus]->getI2CDevice(dev)->getName().c_str(),m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getName().c_str());
        rtnv = I2CInterface::NOTIMPL;
        return(rtnv);
    }

    uint8_t datc[I2CInterface::MAX_BLOCK];

    // convert from vector
    for(unsigned int i=0; i<num; i++) datc[i] = (uint8_t)data[i];

    // write I2C local data
    if((rtnv = localWrite(bus,dev,qty,num,datc)) != SUCCESS) {
        CERR("writing Bus \"%s\", Device \"%s\", Quantity \"%s\"",m_busses[bus]->getName().c_str(),m_busses[bus]->getI2CDevice(dev)->getName().c_str(),m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getName().c_str());
        return(rtnv);
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------

I2CInterface* I2CNetwork::getI2CBus(const unsigned int bus) const {

    // check I2C bus number
    if(!exists(bus)) {
        CERR("I2C bus %d does not exist",bus);
        return(nullptr);
    }

    return(m_busses[bus]->getI2C());
}

//------------------------------------------------------------------------------

std::string I2CNetwork::getTypeI2CDevice(const unsigned int bus, const unsigned int dev) const {

    return(m_busses[bus]->getI2CDevice(dev)->getType());
}

//------------------------------------------------------------------------------

unsigned int I2CNetwork::getAddressI2CDevice(const unsigned int bus, const unsigned int dev) const {

    return(m_busses[bus]->getI2CDevice(dev)->getAddress());
}

//------------------------------------------------------------------------------

unsigned int I2CNetwork::getSwitchAddressI2CDevice(const unsigned int bus, const unsigned int dev) const {

    return(m_busses[bus]->getI2CDevice(dev)->getSwitchAddress());
}

//------------------------------------------------------------------------------

unsigned int I2CNetwork::getSwitchSelectI2CDevice(const unsigned int bus, const unsigned int dev) const {

    return(m_busses[bus]->getI2CDevice(dev)->getSwitchSelect());
}

//------------------------------------------------------------------------------

unsigned int I2CNetwork::getPageOffsetI2CDevice(const unsigned int bus, const unsigned int dev) const {

    return(m_busses[bus]->getI2CDevice(dev)->getPageOffset());
}

//------------------------------------------------------------------------------

unsigned int I2CNetwork::getNumberI2CQuantity(const unsigned int bus, const unsigned int dev, const unsigned int qty) const {

    return(m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getNumber());
}

//------------------------------------------------------------------------------

unsigned int I2CNetwork::getNumberI2CQuantity(const I2CNode& n) const {

    return(m_busses[n.bus()]->getI2CDevice(n.dev())->getI2CQuantity(n.qty())->getNumber());
}

//------------------------------------------------------------------------------

std::string I2CNetwork::getNameI2CQuantity(const unsigned int bus, const unsigned int dev, const unsigned int qty) const {

    return(m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getName());
}

//------------------------------------------------------------------------------

I2CInterface::TRANSFER_TYPE I2CNetwork::getTransferTypeI2CQuantity(const unsigned int bus, const unsigned int dev, const unsigned int qty) const {

    return(m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getTransferType());
}

//------------------------------------------------------------------------------

I2CNetwork::DISPLAY_TYPE I2CNetwork::getDisplayTypeI2CQuantity(const unsigned int bus, const unsigned int dev, const unsigned int qty) const {

    return(m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getDisplayType());
}

//------------------------------------------------------------------------------

int I2CNetwork::transferType(const unsigned int bus, const unsigned int dev, const unsigned int qty, I2CInterface::TRANSFER_TYPE& typ) const {

    // check I2C bus/device/quantity number
    if(!exists(bus,dev,qty)) {
        CERR("I2C bus/device/quantity %d/%d/%d does not exist",bus,dev,qty);
        return(WRONGPAR);
    }

    // return I2C name
    typ = m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getTransferType();

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::displayType(const unsigned int bus, const unsigned int dev, const unsigned int qty, DISPLAY_TYPE& typ) const {

    // check I2C bus/device/quantity number
    if(!exists(bus,dev,qty)) {
        CERR("I2C bus/device/quantity %d/%d/%d does not exist",bus,dev,qty);
        return(WRONGPAR);
    }

    // return I2C name
    typ = m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getDisplayType();

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::name(const unsigned int bus, std::string& name) const {

    // reset name
    name.erase();

    // check I2C bus number
    if(!exists(bus)) {
        CERR("I2C bus %d does not exist",bus);
        return(WRONGPAR);
    }

    // return I2C name
    name = m_busses[bus]->getName();

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::name(const unsigned int bus, const unsigned int dev, std::string& name) const {

    // reset name
    name.erase();

    // check I2C bus/device number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    // return I2C name
    name = m_busses[bus]->getName() + "/" + m_busses[bus]->getI2CDevice(dev)->getName();

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::name(const unsigned int bus, const unsigned int dev, const unsigned int qty, std::string& name) const {

    // reset name
    name.erase();

    // check I2C bus/device/quantity number
    if(!exists(bus,dev,qty)) {
        CERR("I2C bus/device/quantity %d/%d/%d does not exist",bus,dev,qty);
        return(WRONGPAR);
    }

    // return I2C name
    name = m_busses[bus]->getName() + "/" + m_busses[bus]->getI2CDevice(dev)->getName() + "/" + m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getName();

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::readable(const unsigned int bus, const unsigned int dev, const unsigned int qty, bool& rdb) const {

    // check I2C bus/device/quantity number
    if(!exists(bus,dev,qty)) {
        CERR("I2C bus/device/quantity %d/%d/%d does not exist",bus,dev,qty);
        return(WRONGPAR);
    }

    // return I2C readability
    rdb = m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getReadable();

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::writable(const unsigned int bus, const unsigned int dev, const unsigned int qty, bool& wtb) const {

    // check I2C bus/device/quantity number
    if(!exists(bus,dev,qty)) {
        CERR("I2C bus/device/quantity %d/%d/%d does not exist",bus,dev,qty);
        return(WRONGPAR);
    }

    // return I2C writability
    wtb = m_busses[bus]->getI2CDevice(dev)->getI2CQuantity(qty)->getWritable();

    return(SUCCESS);
}

//------------------------------------------------------------------------------

void I2CNetwork::dump() const {

    for(unsigned int bus=0; bus<m_busses.size(); bus++) {
        m_busses[bus]->dump(bus);
    }
}

//------------------------------------------------------------------------------

int I2CNetwork::dumpI2CQuantity(const unsigned int bus, const unsigned int dev, const unsigned int qty) {

    int rtnv(0);
    std::string snam;

    // read I2C name
    if((rtnv = name(bus,dev,qty,snam)) != SUCCESS) {
        CERR("reading name from I2C bus/device/quantity %d/%d/%d",bus,dev,qty);
        return(rtnv);
    }

    // read I2C readability
    bool rdb;
    readable(bus,dev,qty,rdb);
    if(!rdb) {
        std::printf("%-49s = <NOT READABLE>\n",("\""+snam+"\"").c_str());
        return(rtnv);
    }

    // read I2C transfer type
    I2CInterface::TRANSFER_TYPE ttyp;
    transferType(bus,dev,qty,ttyp);
    if(ttyp == I2CInterface::PROC) {
        std::printf("%-49s = <PROCESS CALL>\n",("\""+snam+"\"").c_str());
        return(rtnv);
    }   

    // read I2C display type
    DISPLAY_TYPE dtyp;
    displayType(bus,dev,qty,dtyp);

    // read I2C memory
    if(dtyp == MEMORY) {

        // read I2C data as memory
        unsigned int off(0), num(getNumberI2CQuantity(bus,dev,qty));
        std::vector<unsigned int> dat;
        if((rtnv = read(bus,dev,qty,off,num,dat)) != SUCCESS) {
            CERR("reading data from I2C bus/device/quantity %d/%d/%d - VECTOR",bus,dev,qty);
            return(rtnv);
        }
        std::printf("%-49s = ",("\""+snam+"\"").c_str());
        if(dat.empty()) {
            std::cout << "<NO DATA>" << std::endl;
        }
        else {
            bool flag(false);
            unsigned int i;
            for(i=0; i<num; i++) {
                if(flag and (i%16 == 0)) { std::printf("%-45s",""); flag = false; }
                std::printf("%s0x%02x",flag?", ":"",dat[i]);
                flag = true;
                if(i%16 == 15) std::cout << std::endl;
            }
            if(i%16 != 0) std::cout << std::endl;
        }
    }

    // read I2C word
    else if(dtyp == WORD) {

        // read I2C data as word
        unsigned int dat;
        if((rtnv = read(bus,dev,qty,dat)) != SUCCESS) {
            CERR("reading data from I2C bus/device/quantity %d/%d/%d - WORD",bus,dev,qty);
            return(rtnv);
        }
        unsigned int num(getNumberI2CQuantity(bus,dev,qty));
        std::printf("%-49s = 0x%0*x\n",("\""+snam+"\"").c_str(),num*2,dat);
    }

    // read I2C float
    else if(dtyp == FLOAT) {

        // read I2C data as float
        float dat;
        if((rtnv = read(bus,dev,qty,dat)) != SUCCESS) {
            CERR("reading data from I2C bus/device/quantity %d/%d/%d - FLOAT",bus,dev,qty);
            return(rtnv);
        }
        std::printf("%-49s = %f\n",("\""+snam+"\"").c_str(),dat);
    }

    // read I2C string
    else if(dtyp == STRING) {

        // read I2C data as string
        std::string dat;
        if((rtnv = read(bus,dev,qty,dat)) != SUCCESS) {
            CERR("reading data from I2C bus/device/quantity %d/%d/%d - STRING",bus,dev,qty);
            return(rtnv);
        }
        std::printf("%-49s = \"%s\"\n",("\""+snam+"\"").c_str(),XUTIL::trimString(XUTIL::printableString(dat)).c_str());
    }

    return(rtnv);
}

//------------------------------------------------------------------------------

int I2CNetwork::findI2CDevice(const unsigned int bus, const unsigned int indx, const std::string& name, unsigned int& rslt) {

    // check I2C bus number
    if(!exists(bus)) {
        CERR("I2C bus %d does not exist",bus);
        return(WRONGPAR);
    }

    I2CBus* ibus = m_busses[bus];

    // find I2C device
    unsigned int ix(0);
    rslt = -1;
    for(unsigned int idev=0; idev<ibus->numI2CDevices(); idev++) {
        if(ibus->getI2CDevice(idev)->getName() == name) {
            if(ix == indx) {
                rslt = idev;
                return(SUCCESS);
            }
            ix++;
        }
    }

    return(FAILURE);
}

//------------------------------------------------------------------------------

int I2CNetwork::findI2CQuantity(const unsigned int bus, const unsigned int dev, const std::string& name, unsigned int& rslt) {

    // check I2C bus/dev number
    if(!exists(bus,dev)) {
        CERR("I2C bus/device %d/%d does not exist",bus,dev);
        return(WRONGPAR);
    }

    I2CDevice* idev = m_busses[bus]->getI2CDevice(dev);

    // find I2C quantity
    rslt = -1;
    for(unsigned int iqty=0; iqty<idev->numI2CQuantities(); iqty++) {
        if(idev->getI2CQuantity(iqty)->getName() == name) {
            rslt = iqty;
            return(SUCCESS);
        }
    }

    return(FAILURE);
}

//------------------------------------------------------------------------------

int I2CNetwork::getI2CNode(const std::string& name, I2CNode& node) {

    std::vector<std::string> args;

    if(XUTIL::splitString(name,"/",args) != 3) {
        CERR("cannot read I2C bus/device/quantity name \"%s\"",name.c_str());
        return(WRONGPAR);
    }

    unsigned int bus, dev, qty;
    I2CBus* ibus(nullptr);
    I2CDevice* idev(nullptr);
    I2CQuantity* iqty(nullptr);

    // I2C bus
    for(bus=0; bus<m_busses.size(); bus++) {
        if(m_busses[bus]->getName() == args[0]) {
            ibus = m_busses[bus];
            break;
        }
    }
    if(!ibus) {
        CERR("cannot find I2C bus name \"%s\"",args[0].c_str());
        return(WRONGPAR);
    }

    // I2C device
    for(dev=0; dev<ibus->numI2CDevices(); dev++) {
        if(ibus->getI2CDevice(dev)->getName() == args[1]) {
            idev = ibus->getI2CDevice(dev);
            break;
        }
    }
    if(!idev) {
        CERR("cannot find I2C device name \"%s\"",args[1].c_str());
        return(WRONGPAR);
    }

    // I2C quantity
    for(qty=0; qty<m_busses[bus]->getI2CDevice(dev)->numI2CQuantities(); qty++) {
        if(idev->getI2CQuantity(qty)->getName() == args[2]) {
            iqty = idev->getI2CQuantity(qty);
            break;
        }
    }
    if(!iqty) {
        CERR("cannot find I2C quantity name \"%s\"",args[1].c_str());
        return(WRONGPAR);
    }

    // validate I2C node
    node.m_name = name;
    node.m_bus = bus;
    node.m_dev = dev;
    node.m_qty = qty;

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::getI2CNode(const unsigned int bus, const unsigned int dev, const unsigned int qty, I2CNode& node) {

    // check I2C bus/device/quantity number
    if(!exists(bus,dev,qty)) {
        CERR("I2C bus/device/quantity %d/%d/%d does not exist",bus,dev,qty);
        return(WRONGPAR);
    }

    // validate I2C node
    name(bus,dev,qty,node.m_name);
    node.m_bus = bus;
    node.m_dev = dev;
    node.m_qty = qty;

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::writeRegister(const unsigned int bus, const unsigned int dev, const unsigned int offs, I2CInterface::TRANSFER_TYPE ttyp, unsigned int& num, uint8_t data[]) {

    int rtnv(0);
    I2CInterface* ii2c(m_busses[bus]->getI2C());
    I2CDevice* idev(m_busses[bus]->getI2CDevice(dev));
    unsigned int iswc(idev->getSwitchAddress());
    uint8_t datc[I2CInterface::MAX_BLOCK];

    ii2c->lockBus();

    if(iswc <= I2CInterface::DATA_MASK) {
        int offset = idev->getSwitchSelect();
        unsigned int numb(0);
        CDBG("write switch: address = 0x%02x, select = 0x%02x, numb = %d",iswc,offset,numb);
        if((rtnv = ii2c->DataWrite(iswc,offset,numb,datc,I2CInterface::BYTE)) != SUCCESS) {
            CERR("writing I2C bus/device %d/%d, switch 0x%02x, select 0x%02x",bus,dev,iswc,idev->getSwitchSelect());
            ii2c->unlockBus();
            return(rtnv);
        }
    }

    // write I2C data
    CDBG("write data: address = 0x%02x, offset = 0x%02x, num = %d",idev->getAddress(),offs,num);
    if((rtnv = ii2c->DataWrite(idev->getAddress(),offs,num,data,ttyp)) != SUCCESS) {
        CERR("writing I2C bus/device %d/%d, offset: 0x%02x",bus,dev,offs);
        ii2c->unlockBus();
        return(rtnv);
    }

    ii2c->unlockBus();

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int I2CNetwork::readRegister(const unsigned int bus, const unsigned int dev, const unsigned int offs, I2CInterface::TRANSFER_TYPE ttyp, unsigned int& num, uint8_t data[]) {

    int rtnv(0);
    I2CInterface* ii2c(m_busses[bus]->getI2C());
    I2CDevice* idev(m_busses[bus]->getI2CDevice(dev));
    unsigned int iswc(idev->getSwitchAddress());
    uint8_t datc[I2CInterface::MAX_BLOCK];

    ii2c->lockBus();

    // write I2C switch if necessary
    if(iswc <= I2CInterface::DATA_MASK) {
        int offset = idev->getSwitchSelect();
        unsigned int numb(0);
        CDBG("write switch: address = 0x%02x, select = 0x%02x, numb = %d",iswc,offset,numb);
        if((rtnv = ii2c->DataWrite(iswc,offset,numb,datc,I2CInterface::BYTE)) != SUCCESS) {
            CERR("writing I2C bus/device %d/%d, switch 0x%02x, select 0x%02x",bus,dev,iswc,idev->getSwitchSelect());
            ii2c->unlockBus();
            return(rtnv);
        }
    }

    // read I2C data
    CDBG("read data: address = 0x%02x, offset = 0x%02x, num = %d",idev->getAddress(),offs,num);
    if((rtnv = ii2c->DataRead(idev->getAddress(),offs,num,data,ttyp)) != SUCCESS) {
        CERR("reading I2C bus/device %d/%d, offset: 0x%02x",bus,dev,offs);
        ii2c->unlockBus();
        return(rtnv);
    }

    ii2c->unlockBus();

    return(SUCCESS);
}
