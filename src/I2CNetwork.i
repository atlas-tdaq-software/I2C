%module I2CNetwork
%{
#include "I2CDevice.h"
#include "I2CNetwork.h"

using namespace LVL1;
%}

%include "typemaps.i"
%include "std_string.i"

%apply unsigned int &OUTPUT { unsigned int & };
%apply float        &OUTPUT { float & };
%apply std::string  &INPUT  { const std::string & };
//%apply std::string  OUTPUT  { std::string & };

%feature ("flatnested");

%include "../I2C/I2CInterface.h"
%include "../I2C/I2CDevice.h"

%rename("read_float") read(const unsigned int, const unsigned int, const unsigned int, float &, const bool);

%include "../I2C/I2CNetwork.h"
