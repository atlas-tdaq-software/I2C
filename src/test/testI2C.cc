//******************************************************************************
// file: testI2C.cc
// desc: test program for I2C (VMEbus) controller
// auth: 20-OCT-2017 R. Spiwoks, copy from testI2CDeveice.cc
//******************************************************************************

#include "I2C/I2C.h"
#include "RCDUtilities/RCDUtilities.h"

#include <string>
#include <algorithm>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <stdlib.h>
#include <errno.h>

static const unsigned int MODULE_TYPE_NUMBER = 5;

//------------------------------------------------------------------------------

int readNumber(const std::string& str, unsigned int& num) {

    // copy string
    std::string s(str);

    // remove space(s)
    s.erase(remove(s.begin(),s.end(),' '),s.end());

    // check if string empty
    if(s.empty()) {
        std::fprintf(stderr,"ERROR - input string is empty\n");
        return(-1);
    }

    int base(10);
    char* end;

    // check if string is hexadecimal
    if((s.size() >= 2) && ((s.substr(0,2) == "0x") || s.substr(0,2) == "0X")) base = 16;

    // check if string starts with minus sign
    if((s.size() >= 1) && (s.substr(0,1) == "-")) {
        std::fprintf(stderr,"negative value\n");
        return(-1);
    }

    // try to convert string to integer
    errno = 0;
    num = std::strtoul(s.c_str(),&end,base);
    if(end[0] != '\0') {
        std::fprintf(stderr,"ERROR - illegal character(s)\n");
        return(-1);
    }
    else if(errno == ERANGE) {
        std::fprintf(stderr,"ERROR - number conversion range exception\n");
        return(-1);
    }
    else if(errno == EINVAL) {
        std::fprintf(stderr,"invalid number conversion\n");
        return(-1);
    }

    return(0);
}

// -----------------------------------------------------------------------------

void printData(const unsigned int* data, const unsigned int size) {

    unsigned int i(0);

    for(i=0; i<size; i++) {
        if(i%16 == 0) std::printf("data[%3d] = ",i);
        std::printf(" 0x%02x",data[i]&0xff);
        if(i%16 == 15) std::printf("\n");
    }
    if(i%16 != 0) std::printf("\n");
}

// -----------------------------------------------------------------------------

int enterInt(const char* prompt, int lo, int hi) {

     int val;
     char buf[20];

     while(true) {
         std::printf("%s [%d..%d]: ",prompt,lo,hi);
         buf[0] = '\0';
         if(!std::cin) std::cin.clear();
         std::cin >> buf;
         if(buf[0] == '\0') {
             continue;
         }
         else {
             val = strtol(buf,(char **)0,0);
             if((val < lo) || (val > hi)) {
                 continue;
             }
             else {
                 break;
             }
         }
     }

     return(val);
}

// -----------------------------------------------------------------------------

unsigned long enterHex(const char* prompt, unsigned long lo, unsigned long hi) {

     unsigned long val;
     char buf[20];
     while(1) {
         std::printf("%s [%08lx..%08lx]: ",prompt,lo,hi);
         buf[0] = '\0';
         if(!std::cin) std::cin.clear();
         std::cin >> buf;
         if(buf[0] == '\0') {
             continue;
         }
         else {
             // kludge for value "ffffffff"
             val = (unsigned long)strtoul(buf, (char **)0, 16);
             if((val < lo) || (val > hi)) {
                 continue;
             }
             else {
                 break;
             }
         }
     }

     return(val);
}

// -----------------------------------------------------------------------------

void printText(const unsigned int* data, const unsigned int size) {

    uint8_t datc;

    std::printf("data      = \"");
    for(unsigned int i=0; i<size; i++) {
        datc = data[i] & 0xff;
        if(datc < 32) std::printf("?");
        else if (datc >= 127 ) std::printf(".");
        else std::printf("%c",datc);
    }
    std::printf("\"\n");
}

// -----------------------------------------------------------------------------

void usage() {

    std::cout << "-------------------------------------------------------------------------" << std::endl;
    std::cout << "testI2C <command> <options>" << std::endl;
    std::cout << "-------------------------------------------------------------------------" << std::endl;
    std::cout << "  command:" << std::endl;
    std::cout << "    RBYTE <bus> <swc>" << std::endl;
    std::cout << "    WBYTE <bus> <swc> <sel>" << std::endl;
    std::cout << "    RDATA <bus> <dev> <off>" << std::endl;
    std::cout << "    WDATA <bus> <dev> <off> <dat>" << std::endl;
    std::cout << "    RWORD <bus> <dev> <off>" << std::endl;
    std::cout << "    WWORD <bus> <dev> <off> <dat>" << std::endl;
    std::cout << "    RIBLK <bus> <dev> <off> <num>" << std::endl;
    std::cout << "    WIBLK <bus> <dev> <off> <num> <dat> ..." << std::endl;
    std::cout << "  options:" << std::endl;
    std::cout << "    -M <mod> module type number (1-CTPCORE,2-CTPIN,3-MICTP,4-CTPCOREPLUS,5-ALTI)" << std::endl;
    std::cout << "    -S <slt> module slot number (only CTPIN)" << std::endl;
}

//------------------------------------------------------------------------------

int main(int argc, char **argv) {

    std::string icmd;
    unsigned int narg(1), ibus, iswc, isel, idev, ioff, idat;
    unsigned int data[LVL1::I2CInterface::MAX_BLOCK];
    int imod(-1), slot(-1);

    if(argc < 2) {
        usage();
        std::exit(-1);
    }

    icmd = argv[1];
    if(icmd == "RBYTE") {
        if(argc < 4) {
            printf("usage: %s RBYTE <bus> <swc>\n",argv[0]);
            std::exit(-1);
        }
        if(readNumber(argv[2],ibus) != 0) {
            printf("usage: %s RBYTE <bus> <swc>\n",argv[0]);
            std::exit(-1);
        }
        if(readNumber(argv[3],iswc) != 0) {
            printf("usage: %s RBYTE <bus> <swc>\n",argv[0]);
           std::exit(-1);
        }
        narg = 4;
    }
    else if(icmd == "WBYTE") {
        if(argc < 5) {
            printf("usage: %s WBYTE <bus> <swc> <sel>\n",argv[0]);
            std::exit(-1);
        }
        if(readNumber(argv[2],ibus) != 0) {
            printf("usage: %s WBYTE <bus> <swc> <sel>\n",argv[0]);
            std::exit(-1);
        }
        if(readNumber(argv[3],iswc) != 0) {
            printf("usage: %s WBYTE <bus> <swc> <sel>\n",argv[0]);
           std::exit(-1);
        }
        if(readNumber(argv[4],isel) != 0) {
            printf("usage: %s WBYTE <bus> <swc> <sel>\n",argv[0]);
            std::exit(-1);
        }
        narg = 5;
    }
    else if((icmd == "RDATA") or (icmd == "RWORD")) {
        if(argc < 5) {
            printf("usage: %s %s <bus> <dev> <off>\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        if(readNumber(argv[2],ibus) != 0) {
            printf("usage: %s %s <bus> <dev> <off>\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        if(readNumber(argv[3],idev) != 0) {
            printf("usage: %s %s <bus> <dev> <off>\n",argv[0],icmd.c_str());
           std::exit(-1);
        }
        if(readNumber(argv[4],ioff) != 0) {
            printf("usage: %s %s <bus> <dev> <off>\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        narg = 5;
    }
    else if((icmd == "WDATA") or (icmd == "WWORD")) {
        if(argc < 6) {
            printf("usage: %s %s <bus> <dev> <off> <dat>\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        if(readNumber(argv[2],ibus) != 0) {
            printf("usage: %s %s <bus> <dev> <off> <dat>\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        if(readNumber(argv[3],idev) != 0) {
            printf("usage: %s %s <bus> <dev> <off> <dat>\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        if(readNumber(argv[4],ioff) != 0) {
            printf("usage: %s %s <bus> <dev> <off> <dat>\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        if(readNumber(argv[5],idat) != 0) {
            printf("usage: %s %s <bus> <dev> <off> <dat>\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        narg = 6;
    }
    else if((icmd == "RIBLK")) {
        if(argc < 6) {
            printf("usage: %s %s <bus> <dev> <off> <num>\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        if(readNumber(argv[2],ibus) != 0) {
            printf("usage: %s %s <bus> <dev> <off> <num>\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        if(readNumber(argv[3],idev) != 0) {
            printf("usage: %s %s <bus> <dev> <off> <num>\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        if(readNumber(argv[4],ioff) != 0) {
            printf("usage: %s %s <bus> <dev> <off> <num>\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        if(readNumber(argv[5],idat) != 0) {
            printf("usage: %s %s <bus> <dev> <off> <num>\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        narg = 6;
    }
    else if((icmd == "WIBLK")) {
        if(argc < 7) {
            printf("usage: %s %s <bus> <dev> <off> <num> <dat> ...\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        if(readNumber(argv[2],ibus) != 0) {
            printf("usage: %s %s <bus> <dev> <off> <num> <dat> ...\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        if(readNumber(argv[3],idev) != 0) {
            printf("usage: %s %s <bus> <dev> <off> <num> <dat> ...\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        if(readNumber(argv[4],ioff) != 0) {
            printf("usage: %s %s <bus> <dev> <off> <num> <dat> ...\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        if(readNumber(argv[5],idat) != 0) {
            printf("usage: %s %s <bus> <dev> <off> <num> <dat> ...\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        if(idat == 0) {
            printf("usage: %s %s <bus> <dev> <off> <num> <dat> ... - <num> must be greater than 1\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        narg = 6;
        for(unsigned int i=0; i<idat; i++) {
            if(argc < (int)(7+i)) {
                printf("usage: %s %s <bus> <dev> <off> <num> <dat> ... - there must be <num> <dat> values\n",argv[0],icmd.c_str());
                std::exit(-1);
            }
            unsigned int xdat;
            if(readNumber(argv[6+i],xdat) != 0) {
                printf("usage: %s %s <bus> <dev> <off> <num> <dat> ... - cannot read <dat> value\n",argv[0],icmd.c_str());
                std::exit(-1);
            }
            data[i] = xdat;
            narg++;
        }
    }
    else {
        usage();
        std::exit(-1);
    }

    // handle (optional) flags
    while((int)narg < argc) {
        if(argv[narg][0] == '-') {
            switch (argv[narg][1]) {
            case 'M': {
                    unsigned int xmod(imod);
                    if(readNumber(argv[narg+1],xmod) != 0) {
                        printf("cannot read module type number \"%s\"\n",argv[2]);
                        std::exit(-1);
                    }
                    imod = (int)xmod; narg++;
                    if((imod < 0) or (imod > (int)MODULE_TYPE_NUMBER)) {
                        printf("module type number %d not in allowed range [0..%d]\n",imod,MODULE_TYPE_NUMBER);
                        std::exit(-1);
                    }
                }
                break;

            case 'S': {
                    unsigned int xslot(slot);
                    if(readNumber(argv[narg+1],xslot) != 0) {
                        printf("cannot read module slot number number \"%s\"\n",argv[2]);
                        std::exit(-1);
                    }
                    slot = (int)xslot; narg++;
                    if((slot < 7) or (slot > 9)) {
                        printf("module slot number %d not in allowed range [7..9]\n",imod);
                        std::exit(-1);
                    }
                    break;
                }
            default:
                fprintf(stderr,"unsupported option \"%s\"!\n",argv[narg]);
                usage();
                std::exit(-1);
            }
        }
        else {
            usage();
            std::exit(-1);
        }
        narg++;
    }

    RCD::VME* vme;
    RCD::VMEMasterMap* vmm;

    // open VME driver/library
    vme = RCD::VME::Open();

    // open VME master mapping
    LVL1::I2C::MODULE_TYPE m = LVL1::I2C::UNKNOWN;
    if(imod < 0) {
        imod = enterInt("Module type (1-CTPCORE, 2-CTPIN, 3-MICTP, 4-CTPCOREPLUS, 5-ALTI)",0,MODULE_TYPE_NUMBER);
    }
    switch (imod) {
    case 1: {       // CTPCORE
            m = LVL1::I2C::CTP_CORE;
            vmm = vme->MasterMap(0x58000000,0x100000,2,0);
        }
        break;

    case 2: {       // CTPIN
            m = LVL1::I2C::CTP_IN;
            if(slot < 0) {
                slot = enterInt("Slot number: ",7,9);
            }
            unsigned int vme_addr = (slot & 0x1F) << 27;
            vmm = vme->MasterMap(vme_addr,0x290000,2,0);
        }
        break;

    case 3: {       // MICTP
            m = LVL1::I2C::MICTP;
            vmm = vme->MasterMap(0x60000000,0x80000,2,0);
        }
        break;

    case 4: {       // CTPCOREPLUS
            m = LVL1::I2C::CTPCOREPLUS;
            vmm = vme->MasterMap(0x58000000,0x210000,2,0);
        }
        break;

    case 5: {       // ALTI
            m = LVL1::I2C::ALTI;
            vmm = vme->MasterMap(0x08000000,0x01000000,2,0);
        }
        break;

    default: {
            unsigned int p = (unsigned int)enterHex("Vmebus_address                       (hex)",0,0xffffffff); if (p < 0x100) p = p << 24;
            unsigned int s = (unsigned int)enterHex("Window_size                          (hex)",0,0xffffffff);
            unsigned int a = (unsigned int)enterHex("Address_modifier                     (hex)",0,0xffffffff);
            vmm = vme->MasterMap(p,s,a,0);
        }
    }
    if((*vmm)()) {
        CERR("opening VME master mapping","");
        return(-1);
    }

    // create I2C
    LVL1::I2C i2c(m,vmm,ibus);

    unsigned int addr(0), offs(0), numb(0);
    uint8_t datc(0);
    uint16_t dats(0);

    if(icmd == "RBYTE") {
        addr = iswc;
        if(i2c.DataRead(addr,datc) == LVL1::I2CInterface::SUCCESS) std::printf("Read:  addr 0x%02x, data = 0x%02x\n",addr,datc);
    }
    else if(icmd == "WBYTE") {
        addr = iswc;
        datc = isel;
        if(i2c.DataWrite(addr,datc) == LVL1::I2CInterface::SUCCESS) std::printf("Write: addr 0x%02x, data = 0x%02x\n",addr,datc);
    }
    else if(icmd == "RDATA") {
        addr = idev;
        offs = ioff;
        if(i2c.DataRead(addr,offs,datc) == LVL1::I2CInterface::SUCCESS) std::printf("Read:  addr 0x%02x, offs = 0x%02x, data = 0x%02x\n",addr,offs,datc);
    }
    else if(icmd == "WDATA") {
        addr = idev;
        offs = ioff;
        datc = idat;
        if(i2c.DataWrite(addr,offs,datc) == LVL1::I2CInterface::SUCCESS) std::printf("Write: addr 0x%02x, offs = 0x%02x, data = 0x%02x\n",addr,offs,datc);
    }
    else if(icmd == "RWORD") {
        addr = idev;
        offs = ioff;
        if(i2c.DataRead(addr,offs,dats) == LVL1::I2CInterface::SUCCESS) std::printf("Read:  addr 0x%02x, offs = 0x%02x, data = 0x%04x\n",addr,offs,dats);
    }
    else if(icmd == "WWORD") {
        addr = idev;
        offs = ioff;
        dats = idat;
        if(i2c.DataWrite(addr,offs,dats) == LVL1::I2CInterface::SUCCESS) std::printf("Write: addr 0x%02x, offs = 0x%02x, data = 0x%04x\n",addr,offs,dats);
    }
    else if(icmd == "RIBLK") {
        addr = idev;
        offs = ioff;
        numb = idat;
        if(i2c.DataRead(addr,offs,numb,data) == LVL1::I2CInterface::SUCCESS) {
            std::printf("Read:  addr 0x%02x, offs = 0x%02x:\n",addr,offs);
            printData(data,numb);
            printText(data,numb);
        }
    }
    else if(icmd == "WIBLK") {
        addr = idev;
        offs = ioff;
        numb = idat;
        if(i2c.DataWrite(addr,offs,numb,data) == LVL1::I2CInterface::SUCCESS) {
            std::printf("Wrote: addr 0x%02x, offs = 0x%02x:\n",addr,offs);
            printData(data,numb);
            printText(data,numb);
        }
    }

    // close VME master mapping
    RCD::VME::Close();
}
