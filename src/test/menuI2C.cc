//******************************************************************************
// file: menuI2C.cc
// desc: I2C Interface - menu program
// auth: 05-AUG-2004, R. Spiwoks
//******************************************************************************

#include "RCDUtilities/RCDUtilities.h"
#include "RCDMenu/RCDMenu.h"
#include "I2C/I2C.h"

#include <cstdlib>
#include <iostream>

using namespace LVL1;
using namespace RCD;

VME* vme;
VMEMasterMap* vmm;
I2C* module;

//------------------------------------------------------------------------------

class I2COpen : public MenuItem {
  public:
    I2COpen() { setName("open I2C"); }
    int action() {

        // open VME driver/library
        vme = VME::Open();

        // open VME master mapping
        unsigned int bus(0);
        I2C::MODULE_TYPE m = I2C::UNKNOWN;
        int sel = enterInt("Module type (1-CTPCORE, 2-CTPIN, 3-MICTP, 4-CTPCOREPLUS, 5-ALTI)", 0, 5);
        switch (sel) {
        case 1: {       // CTPCORE
                m = I2C::CTP_CORE;
                vmm = vme->MasterMap(0x58000000,0x100000,2,0);
            }
            break;

        case 2: {       // CTPIN
                m = I2C::CTP_IN;
                int slot = enterInt("Slot number: ", 7, 9);
                unsigned int vme_addr = (slot & 0x1F) << 27;
                vmm = vme->MasterMap(vme_addr,0x290000,2,0);
            }
            break;

        case 3: {       // MICTP
                m = I2C::MICTP;
                vmm = vme->MasterMap(0x60000000,0x80000,2,0);
            }
            break;

        case 4: {       // CTPCOREPLUS
                m = I2C::CTPCOREPLUS;
                bus = enterInt("Bus number: ",0,I2C::BUS_NUMBER-1);
                vmm = vme->MasterMap(0x58000000,0x210000,2,0);
            }
            break;

        case 5: {       // ALTI
                m = I2C::ALTI;
                u_int p = (u_int)enterHex("Vmebus_address   (hex)",0,0xffffffff);
                if(p < 0x100) p = p << 24;
                vmm = vme->MasterMap(p,0x01000000,2,0);
            }
            break;

        default: {
                unsigned int p = (unsigned int)enterHex("Vmebus_address                       (hex)",0,0xffffffff); if (p < 0x100) p = p << 24;
                unsigned int s = (unsigned int)enterHex("Window_size                          (hex)",0,0xffffffff);
                unsigned int a = (unsigned int)enterHex("Address_modifier                     (hex)",0,0xffffffff);
                vmm = vme->MasterMap(p,s,a,0);
            }
        }

        // create I2C
        module = new I2C(m,vmm,bus);

        return((*vmm)());
    }
};

//------------------------------------------------------------------------------

class I2CClose : public MenuItem {
  public:
    I2CClose() { setName("close I2C"); }
    int action() {

        // delete I2C
        delete module;

        // close VME master mapping
        VME::Close();

        return(0);
    }
};

//------------------------------------------------------------------------------

class I2CPrescaleLowRead : public MenuItem {
  public:
    I2CPrescaleLowRead() { setName("read  \"PrescaleLow\""); }
    int action() {

        int rtnv(0);
        unsigned int data;

        if((rtnv = module->PrescaleLow_Read(data)) != I2C::SUCCESS) {
            CERR("reading I2C prescale low value","");
            return(rtnv);
        }
        std::printf("data = 0x%02x\n",data&I2C::DATA_MASK);

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class I2CPrescaleLowWrite : public MenuItem {
  public:
    I2CPrescaleLowWrite() { setName("write \"PrescaleLow\""); }
    int action() {

        unsigned int data = enterHex("PrescaleLow",0,0xffffffff);

        return(module->PrescaleLow_Write(data));
    }
};

//------------------------------------------------------------------------------

class I2CPrescaleHighRead : public MenuItem {
  public:
    I2CPrescaleHighRead() { setName("read  \"PrescaleHigh\""); }
    int action() {

        int rtnv(0);
        unsigned int data;

        if((rtnv = module->PrescaleHigh_Read(data)) != I2C::SUCCESS) {
            CERR("reading I2C prescale high value","");
            return(rtnv);
        }
        std::printf("data = 0x%02x\n",data&I2C::DATA_MASK);

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class I2CPrescaleHighWrite : public MenuItem {
  public:
    I2CPrescaleHighWrite() { setName("write \"PrescaleHigh\""); }
    int action() {

        unsigned int data = enterHex("PrescaleHigh",0,0xffffffff);

        return(module->PrescaleHigh_Write(data));
    }
};

//------------------------------------------------------------------------------

class I2CControlRead : public MenuItem {
  public:
    I2CControlRead() { setName("read  \"Control\""); }
    int action() {

        int rtnv(0);
        I2C_CONTROL_BITSTRING bitset;

        if((rtnv = module->Control_Read(bitset)) != I2C::SUCCESS) {
            CERR("reading I2C control value","");
            return(rtnv);
        }
        bitset.print();

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class I2CControlWrite : public MenuItem {
  public:
    I2CControlWrite() { setName("write \"Control\""); }
    int action() {

        I2C_CONTROL_BITSTRING bitset;
        bitset = (BitSet)(enterString("Control"));

        return(module->Control_Write(bitset));
    }
};

//------------------------------------------------------------------------------

class I2CTransmitDataRead : public MenuItem {
  public:
    I2CTransmitDataRead() { setName("read  \"TransmitData\""); }
    int action() {

        int rtnv(0);
        unsigned int data;

        if((rtnv = module->TransmitData_Read(data)) != I2C::SUCCESS) {
            CERR("reading I2C data transmit value","");
            return(rtnv);
        }
        std::printf("data = 0x%02x\n",data&I2C::DATA_MASK);

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class I2CTransmitDataWrite : public MenuItem {
  public:
    I2CTransmitDataWrite() { setName("write \"TransmitData\""); }
    int action() {

        unsigned int data = enterHex("TransmitData",0,0xffffffff);

        return(module->TransmitData_Write(data));
    }
};

//------------------------------------------------------------------------------

class I2CTransmitAddrRead : public MenuItem {
  public:
    I2CTransmitAddrRead() { setName("read  \"TransmitAddr\""); }
    int action() {

        int rtnv(0);
        I2C_TRANSMITADDR_BITSTRING bitset;

        if((rtnv = module->TransmitAddr_Read(bitset)) != I2C::SUCCESS) {
            CERR("read I2C transmit address value","");
            return(rtnv);
        }
        bitset.print();
        std::printf("transmit address value = %s\n",I2C::printAddress(bitset.number()[0]).c_str());

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class I2CTransmitAddrWrite : public MenuItem {
  public:
    I2CTransmitAddrWrite() { setName("write \"TransmitAddr\""); }
    int action() {

        I2C_TRANSMITADDR_BITSTRING bitset;
        bitset = (BitSet)(enterString("TransmitAddr"));

        return(module->TransmitAddr_Write(bitset));
    }
};

//------------------------------------------------------------------------------

class I2CReceiveRead : public MenuItem {
  public:
    I2CReceiveRead() { setName("read  \"Receive\""); }
    int action() {

        int rtnv(0);
        unsigned int data;

        if((rtnv = module->Receive_Read(data)) != I2C::SUCCESS) {
            CERR("reading I2C receive value","");
            return(rtnv);
        }
        std::printf("data = 0x%02x\n",data&I2C::DATA_MASK);

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class I2CReceiveWrite : public MenuItem {
  public:
    I2CReceiveWrite() { setName("write \"Receive\""); }
    int action() {

        unsigned int data = enterHex("Receive",0,0xffffffff);

        return(module->Receive_Write(data));
    }
};

//------------------------------------------------------------------------------

class I2CCommandRead : public MenuItem {
  public:
    I2CCommandRead() { setName("read  \"Command\""); }
    int action() {

        int rtnv(0);
        I2C_COMMAND_BITSTRING bitset;

        if((rtnv = module->Command_Read(bitset)) != I2C::SUCCESS) {
            CERR("reading I2C command value","");
            return(rtnv);
        }
        bitset.print();
        std::printf("command value = %s\n",I2C::printCommand(bitset.number()[0]).c_str());

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class I2CCommandWrite : public MenuItem {
  public:
    I2CCommandWrite() { setName("write \"Command\""); }
    int action() {

        I2C_COMMAND_BITSTRING bitset;
        bitset = (BitSet)(enterString("Command"));

        return(module->Command_Write(bitset));
    }
};

//------------------------------------------------------------------------------

class I2CStatusRead : public MenuItem {
  public:
    I2CStatusRead() { setName("read  \"Status\""); }
    int action() {

        int rtnv(0);
        I2C_STATUS_BITSTRING bitset;

        if((rtnv = module->Status_Read(bitset)) != I2C::SUCCESS) {
            CERR("reading I2C status value","");
            return(rtnv);
        }
        bitset.print();
        std::printf("status value = %s\n",I2C::printStatus(bitset.number()[0]).c_str());

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class I2CStatusWrite : public MenuItem {
  public:
    I2CStatusWrite() { setName("write \"Status\""); }
    int action() {

        I2C_STATUS_BITSTRING bitset;
        bitset = (BitSet)(enterString("Status"));

        return(module->Status_Write(bitset));
    }
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

class I2CPrescaleRead : public MenuItem {
  public:
    I2CPrescaleRead() { setName("read  prescale"); }
    int action() {

        int rtnv(0);
        unsigned int prsc;

        // read I2C prescale
        if((rtnv = module->PrescaleRead(prsc)) != I2C::SUCCESS) {
            CERR("reading I2C prescale","");
            return(rtnv);
        }
        std::printf("\nPrescale value: %5d (= 0x%04x)\n",prsc,prsc);

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class I2CPrescaleWrite : public MenuItem {
  public:
    I2CPrescaleWrite() { setName("write prescale"); }
    int action() {

        int rtnv(0);
        unsigned int prsc = enterNumber("Prescale value",0,I2C::PRSC_MASK);

        // write I2C prescale
        if((rtnv = module->PrescaleWrite(prsc)) != I2C::SUCCESS) {
            CERR("writing I2C prescale","");
            return(rtnv);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class I2CEnableRead : public MenuItem {
  public:
    I2CEnableRead() { setName("read  enable"); }
    int action() {

        int rtnv(0);
        bool ena;

        // read I2C enable
        if((rtnv = module->EnableRead(ena)) != I2C::SUCCESS) {
            CERR("reading I2C enable","");
            return(rtnv);
        }
        std::printf("\nControl value = %s\n",ena?"ENABLED":"DISABLED");

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class I2CEnableWrite : public MenuItem {
  public:
    I2CEnableWrite() { setName("write enable"); }
    int action() {

        int rtnv(0);
        bool ena = (bool)enterInt("Control enable (0=no, 1=yes)",0,1);

        // write I2C enable
        if((rtnv = module->EnableWrite(ena)) != I2C::SUCCESS) {
            CERR("writing I2C enable","");
            return(rtnv);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class I2CDataRead : public MenuItem {
  public:
    I2CDataRead() { setName("read  data"); }
    int action() {

        int rtnv(0);
        unsigned int addr = enterNumber("Address",0,I2C::ADDR_MASK);
        unsigned int data;

        // read I2C data
        if((rtnv = module->DataRead(addr,data)) != I2C::SUCCESS) {
            CERR("reading I2C data from address 0x%02x",addr);
            return(rtnv);
        }
        std::printf("\nData[%3d] = 0x%02x (= %3d)\n",addr,data,data);

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class I2CDataReadMemory : public MenuItem {
  public:
    I2CDataReadMemory() { setName("read  data [memory]"); }
    int action() {

        int rtnv(0);
        unsigned int addr = enterNumber("Address",0,I2C::ADDR_MASK);
        unsigned int offs = enterNumber("Byte offset",0,I2C::DATA_SIZE-1);
        unsigned int numb = enterNumber("Number of bytes",0,I2C::DATA_SIZE-offs);
        unsigned int data[I2C::DATA_SIZE];

        // read I2C data
        if((rtnv = module->DataRead(addr,offs,numb,data)) != I2C::SUCCESS) {
            CERR("reading I2C data from address 0x%02x, offset 0x%02x, %d bytes",addr,offs,numb);
            return(rtnv);
        }
        std::cout << std::endl;
        for(unsigned int i=0; i<numb; i++) {
             std::printf("Data[%3d] = 0x%02x (= %3d)\n",offs+i,data[i],data[i]);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class I2CDataWrite : public MenuItem {
  public:
    I2CDataWrite() { setName("write data"); }
    int action() {

        int rtnv(0);
        unsigned int addr = enterNumber("Address",0,I2C::ADDR_MASK);
        unsigned int data = enterNumber("Data",0,I2C::DATA_MASK);

        // write I2C data
        if((rtnv = module->DataWrite(addr,data)) != I2C::SUCCESS) {
            CERR("writing I2C data to address 0x%02x",addr);
            return(rtnv);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class I2CDataWriteMemory : public MenuItem {
  public:
    I2CDataWriteMemory() { setName("write data [memory]"); }
    int action() {

        int rtnv(0);
        unsigned int addr = enterNumber("Address",0,I2C::ADDR_MASK);
        unsigned int offs = enterNumber("Byte offset",0,I2C::DATA_SIZE-1);
        unsigned int data = enterNumber("Data",0,I2C::DATA_MASK);

        // write I2C data
        if((rtnv = module->DataWrite(addr,offs,data)) != I2C::SUCCESS) {
            CERR("writing I2C data to address 0x%02x, offset 0x%02x",addr,offs);
            return(rtnv);
        }

        return(rtnv);
    }
};
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int main() {

    Menu menu("I2C main menu");

    // menu for I2C
    menu.init(new I2COpen);
    menu.add(new I2CPrescaleRead);
    menu.add(new I2CPrescaleWrite);
    menu.add(new I2CEnableRead);
    menu.add(new I2CEnableWrite);
    menu.add(new I2CDataRead);
    menu.add(new I2CDataReadMemory);
    menu.add(new I2CDataWrite);
    menu.add(new I2CDataWriteMemory);

    // low-level menu
    Menu menuLow("LOW-LEVEL MENU");
    menuLow.add(new I2CPrescaleLowRead);
    menuLow.add(new I2CPrescaleLowWrite);
    menuLow.add(new I2CPrescaleHighRead);
    menuLow.add(new I2CPrescaleHighWrite);
    menuLow.add(new I2CControlRead);
    menuLow.add(new I2CControlWrite);
    menuLow.add(new I2CTransmitDataRead);
    menuLow.add(new I2CTransmitDataWrite);
    menuLow.add(new I2CTransmitAddrRead);
    menuLow.add(new I2CTransmitAddrWrite);
    menuLow.add(new I2CReceiveRead);
    menuLow.add(new I2CReceiveWrite);
    menuLow.add(new I2CCommandRead);
    menuLow.add(new I2CCommandWrite);
    menuLow.add(new I2CStatusRead);
    menuLow.add(new I2CStatusWrite);
    menu.add(&menuLow);

    menu.exit(new I2CClose);

    // execute menu
    menu.execute();

    exit(0);
}
