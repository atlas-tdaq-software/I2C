//******************************************************************************
// file: testI2CDevice.cc
// desc: test program for I2CDevice controller
// auth: 30-AUG-2016 R. Spiwoks
// auth: 16-FEB-2017 R. Spiwoks, imported from zynq_i2c
//******************************************************************************

#include "I2C/I2CDevice.h"

#include <string>
#include <algorithm>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <errno.h>
#include <string.h>

//------------------------------------------------------------------------------

int readNumber(const std::string& str, unsigned int& num) {

    // copy string
    std::string s(str);

    // remove space(s)
    s.erase(remove(s.begin(),s.end(),' '),s.end());

    // check if string empty
    if(s.empty()) {
        std::fprintf(stderr,"ERROR - input string is empty\n");
        return(-1);
    }

    int base(10);
    char* end;

    // check if string is hexadecimal
    if((s.size() >= 2) && ((s.substr(0,2) == "0x") || s.substr(0,2) == "0X")) base = 16;

    // check if string starts with minus sign
    if((s.size() >= 1) && (s.substr(0,1) == "-")) {
        std::fprintf(stderr,"negative value\n");
        return(-1);
    }

    // try to convert string to integer
    errno = 0;
    num = std::strtoul(s.c_str(),&end,base);
    if(end[0] != '\0') {
        std::fprintf(stderr,"ERROR - illegal character(s)\n");
        return(-1);
    }
    else if(errno == ERANGE) {
        std::fprintf(stderr,"ERROR - number conversion range exception\n");
        return(-1);
    }
    else if(errno == EINVAL) {
        std::fprintf(stderr,"invalid number conversion\n");
        return(-1);
    }

    return(0);
}

// -----------------------------------------------------------------------------

void printData(const uint8_t* data, const unsigned int size) {

    unsigned int i(0);

    for(i=0; i<size; i++) {
        if(i%16 == 0) std::printf("data[%3d] = ",i);
        std::printf(" 0x%02x",data[i]);
        if(i%16 == 15) std::printf("\n");
    }
    if(i%16 != 0) std::printf("\n");
}

// -----------------------------------------------------------------------------

void printText(const uint8_t* data, const unsigned int size) {

    std::printf("data      = \"");
    for(unsigned int i=0; i<size; i++) {
        if(data[i] < 32) std::printf("?");
        else if (data[i] >= 127 ) std::printf(".");
        else std::printf("%c",data[i]);
    }
    std::printf("\"\n");
}

// -----------------------------------------------------------------------------

void usage() {

    std::cout << "---------------------------------" << std::endl;
    std::cout << "testI2CDevice <command> <options>" << std::endl;
    std::cout << "---------------------------------" << std::endl;
    std::cout << "  command:" << std::endl;
    std::cout << "    LFUNC <bus>" << std::endl;
    std::cout << "    RBYTE <bus> <swc>" << std::endl;
    std::cout << "    WBYTE <bus> <swc> <sel>" << std::endl;
    std::cout << "    RDATA <bus> <dev> <off>" << std::endl;
    std::cout << "    WDATA <bus> <dev> <off> <dat>" << std::endl;
    std::cout << "    RWORD <bus> <dev> <off>" << std::endl;
    std::cout << "    WWORD <bus> <dev> <off> <dat>" << std::endl;
    std::cout << "    RIBLK <bus> <dev> <off> <num>" << std::endl;
    std::cout << "    WIBLK <bus> <dev> <off> <num> <dat> ..." << std::endl;
    std::cout << "    RSBLK <bus> <dev> <off>" << std::endl;
    std::cout << "    WSBLK <bus> <dev> <off> <num> <dat> ..." << std::endl;
    std::cout << "    RPBLK <bus> <dev> <off> <num>" << std::endl;
    std::cout << "    WPBLK <bus> <dev> <off> <num> <dat> ..." << std::endl;
    std::cout << "    RPROC <bus> <dev> <off> <dlo> <dhi>" << std::endl;
    std::cout << "    BPROC <bus> <dev> <off> <num> <dat> ..." << std::endl;
    std::cout << "    RSMON <bus> <dev> <off> <dat>" << std::endl;
    std::cout << "    WSMON <bus> <dev> <off> <dat>" << std::endl;
    std::cout << "  options:" << std::endl;
    std::cout << "    -F       force slave address" << std::endl;
    std::cout << "    -P       use packet error checking (DEVICE)" << std::endl;
    std::cout << "    -S       use packet error checking (SOFTWARE)" << std::endl;
    std::cout << "    -L <int> run in a loop (0xffffffff = infinity)" << std::endl;
}

//------------------------------------------------------------------------------

int main(int argc, char **argv) {

    std::string icmd;
    unsigned int narg(1), ibus, iswc, isel, idev, ioff, idat, loop(1);
    uint8_t blck[LVL1::I2CInterface::MAX_BLOCK];
    bool force(false);
    LVL1::I2CDevice::PEC_TYPE pmbus(LVL1::I2CDevice::NO_PEC);

    if(argc < 2) {
        usage();
        std::exit(-1);
    }

    icmd = argv[1];
    if(icmd == "LFUNC") {
        if(argc < 3) {
            printf("usage: %s LFUNC <bus>\n",argv[0]);
            std::exit(-1);
        }
        if(readNumber(argv[2],ibus) != 0) {
            printf("usage: %s LFUNC <bus>\n",argv[0]);
            std::exit(-1);
        }
        narg = 3;
    }
    else if(icmd == "RBYTE") {
        if(argc < 4) {
            printf("usage: %s RBYTE <bus> <swc>\n",argv[0]);
            std::exit(-1);
        }
        if(readNumber(argv[2],ibus) != 0) {
            printf("usage: %s RBYTE <bus> <swc>\n",argv[0]);
            std::exit(-1);
        }
        if(readNumber(argv[3],iswc) != 0) {
            printf("usage: %s RBYTE <bus> <swc>\n",argv[0]);
           std::exit(-1);
        }
        narg = 4;
    }
    else if(icmd == "WBYTE") {
        if(argc < 5) {
            printf("usage: %s WBYTE <bus> <swc> <sel>\n",argv[0]);
            std::exit(-1);
        }
        if(readNumber(argv[2],ibus) != 0) {
            printf("usage: %s WBYTE <bus> <swc> <sel>\n",argv[0]);
            std::exit(-1);
        }
        if(readNumber(argv[3],iswc) != 0) {
            printf("usage: %s WBYTE <bus> <swc> <sel>\n",argv[0]);
           std::exit(-1);
        }
        if(readNumber(argv[4],isel) != 0) {
            printf("usage: %s WBYTE <bus> <swc> <sel>\n",argv[0]);
            std::exit(-1);
        }
        narg = 5;
    }
    else if((icmd == "RDATA") or (icmd == "RWORD") or (icmd == "RSBLK") or(icmd == "RSMON")) {
        if(argc < 5) {
            printf("usage: %s %s <bus> <dev> <off>\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        if(readNumber(argv[2],ibus) != 0) {
            printf("usage: %s %s <bus> <dev> <off>\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        if(readNumber(argv[3],idev) != 0) {
            printf("usage: %s %s <bus> <dev> <off>\n",argv[0],icmd.c_str());
           std::exit(-1);
        }
        if(readNumber(argv[4],ioff) != 0) {
            printf("usage: %s %s <bus> <dev> <off>\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        narg = 5;
    }
    else if((icmd == "WDATA") or (icmd == "WWORD") or (icmd == "WSMON")) {
        if(argc < 6) {
            printf("usage: %s %s <bus> <dev> <off> <dat>\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        if(readNumber(argv[2],ibus) != 0) {
            printf("usage: %s %s <bus> <dev> <off> <dat>\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        if(readNumber(argv[3],idev) != 0) {
            printf("usage: %s %s <bus> <dev> <off> <dat>\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        if(readNumber(argv[4],ioff) != 0) {
            printf("usage: %s %s <bus> <dev> <off> <dat>\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        if(readNumber(argv[5],idat) != 0) {
            printf("usage: %s %s <bus> <dev> <off> <dat>\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        narg = 6;
    }
    else if((icmd == "RIBLK") or (icmd == "RPBLK")) {
        if(argc < 6) {
            printf("usage: %s %s <bus> <dev> <off> <num>\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        if(readNumber(argv[2],ibus) != 0) {
            printf("usage: %s %s <bus> <dev> <off> <num>\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        if(readNumber(argv[3],idev) != 0) {
            printf("usage: %s %s <bus> <dev> <off> <num>\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        if(readNumber(argv[4],ioff) != 0) {
            printf("usage: %s %s <bus> <dev> <off> <num>\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        if(readNumber(argv[5],idat) != 0) {
            printf("usage: %s %s <bus> <dev> <off> <num>\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        narg = 6;
    }
    else if((icmd == "WIBLK") or (icmd == "WSBLK") or (icmd == "WPBLK") or (icmd == "BPROC")) {
        if(argc < 7) {
            printf("usage: %s %s <bus> <dev> <off> <num> <dat> ...\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        if(readNumber(argv[2],ibus) != 0) {
            printf("usage: %s %s <bus> <dev> <off> <num> <dat> ...\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        if(readNumber(argv[3],idev) != 0) {
            printf("usage: %s %s <bus> <dev> <off> <num> <dat> ...\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        if(readNumber(argv[4],ioff) != 0) {
            printf("usage: %s %s <bus> <dev> <off> <num> <dat> ...\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        if(readNumber(argv[5],idat) != 0) {
            printf("usage: %s %s <bus> <dev> <off> <num> <dat> ...\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        if(idat == 0) {
            printf("usage: %s %s <bus> <dev> <off> <num> <dat> ... - <num> must be greater than 1\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        narg = 6;
        for(unsigned int i=0; i<idat; i++) {
            if(argc < (int)(7+i)) {
                printf("usage: %s %s <bus> <dev> <off> <num> <dat> ... - there must be <num> <dat> values\n",argv[0],icmd.c_str());
                std::exit(-1);
            }
            unsigned int xdat;
            if(readNumber(argv[6+i],xdat) != 0) {
                printf("usage: %s %s <bus> <dev> <off> <num> <dat> ... - cannot read <dat> value\n",argv[0],icmd.c_str());
                std::exit(-1);
            }
            blck[i] = xdat;
            narg++;
        }
    }
    else if(icmd == "RPROC") {
        if(argc < 7) {
            printf("usage: %s %s <bus> <dev> <off> <dlo> <dhi>\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        if(readNumber(argv[2],ibus) != 0) {
            printf("usage: %s %s <bus> <dev> <off> <dlo> <dhi>\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        if(readNumber(argv[3],idev) != 0) {
            printf("usage: %s %s <bus> <dev> <off> <dlo> <dhi>\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        if(readNumber(argv[4],ioff) != 0) {
            printf("usage: %s %s <bus> <dev> <off> <dlo> <dhi>\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        unsigned int xdat;
        if(readNumber(argv[5],xdat) != 0) {
            printf("usage: %s %s <bus> <dev> <off> <dlo> <dhi>\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        blck[0] = xdat;
        if(readNumber(argv[6],xdat) != 0) {
            printf("usage: %s %s <bus> <dev> <off> <dlo> <dhi>\n",argv[0],icmd.c_str());
            std::exit(-1);
        }
        blck[1] = xdat;
        narg = 7;
    }
    else {
        usage();
        std::exit(-1);
    }

    // handle (optional) flags
    while((int)narg < argc) {
        if(argv[narg][0] == '-') {
            switch (argv[narg][1]) {
            case 'F':
                force = true;
                break;
            case 'L':
                if(((int)(narg+1) >= argc) or (strcmp(argv[narg+1],"\0"))) {
                    fprintf(stderr,"missing argument for \"%s\"!\n",argv[narg]);
                    usage();
                    std::exit(-1);
                }
                if(readNumber(argv[narg+1],loop) != 0) {
                    fprintf(stderr,"wrong argument for \"%s\": \"%s\"!\n",argv[narg],argv[narg+1]);
                    usage();
                    std::exit(-1);
                }
                narg++;
                break;
            case 'P':
                pmbus = LVL1::I2CDevice::PEC_DEVICE;
                break;
            case 'S':
                pmbus = LVL1::I2CDevice::PEC_SOFT;
                break;
            default:
                fprintf(stderr,"unsupported option \"%s\"!\n",argv[narg]);
                usage();
                std::exit(-1);
            }
        }
        else {
            usage();
            std::exit(-1);
        }
        narg++;
    }

    LVL1::I2CDevice i2c(ibus,force,pmbus);

    unsigned int addr(0), offs(0), numb(0);
    uint8_t datc(0);
    uint16_t dats(0);

    // begin of for loop
    for(unsigned int i=0; (i==0xffffffff) or (i<loop); i++) {

    if(icmd == "LFUNC") {
       i2c.PrintFunctions();
    }
    else if(icmd == "RBYTE") {
        addr = iswc;
        if(i2c.Read(addr,datc) == LVL1::I2CInterface::SUCCESS) std::printf("Read:  addr 0x%02x, data = 0x%02x\n",addr,datc);
    }
    else if(icmd == "WBYTE") {
        addr = iswc;
        datc = isel;
        if(i2c.Write(addr,datc) == LVL1::I2CInterface::SUCCESS) std::printf("Write: addr 0x%02x, data = 0x%02x\n",addr,datc);
    }
    else if(icmd == "RDATA") {
        addr = idev;
        offs = ioff;
        if(i2c.Read(addr,offs,datc) == LVL1::I2CInterface::SUCCESS) std::printf("Read:  addr 0x%02x, offs = 0x%02x, data = 0x%02x\n",addr,offs,datc);
    }
    else if(icmd == "WDATA") {
        addr = idev;
        offs = ioff;
        datc = idat;
        if(i2c.Write(addr,offs,datc) == LVL1::I2CInterface::SUCCESS) std::printf("Write: addr 0x%02x, offs = 0x%02x, data = 0x%02x\n",addr,offs,datc);
    }
    else if(icmd == "RWORD") {
        addr = idev;
        offs = ioff;
        if(i2c.Read(addr,offs,dats) == LVL1::I2CInterface::SUCCESS) std::printf("Read:  addr 0x%02x, offs = 0x%02x, data = 0x%04x\n",addr,offs,dats);
    }
    else if(icmd == "WWORD") {
        addr = idev;
        offs = ioff;
        dats = idat;
        if(i2c.Write(addr,offs,dats) == LVL1::I2CInterface::SUCCESS) std::printf("Write: addr 0x%02x, offs = 0x%02x, data = 0x%04x\n",addr,offs,dats);
    }
    else if(icmd == "RIBLK") {
        addr = idev;
        offs = ioff;
        numb = idat;
        if(i2c.ReadIBlock(addr,offs,numb,blck) == LVL1::I2CInterface::SUCCESS) {
            std::printf("Read:  addr 0x%02x, offs = 0x%02x:\n",addr,offs);
            printData(blck,numb);
            printText(blck,numb);
        }
    }
    else if(icmd == "WIBLK") {
        addr = idev;
        offs = ioff;
        numb = idat;
        if(i2c.WriteIBlock(addr,offs,numb,blck) == LVL1::I2CInterface::SUCCESS) {
            std::printf("Wrote: addr 0x%02x, offs = 0x%02x:\n",addr,offs);
            printData(blck,numb);
            printText(blck,numb);
        }
    }
    else if(icmd == "RSBLK") {
        addr = idev;
        offs = ioff;
std::printf("RSBLK RSBLK RSBLK\n");
        if(i2c.ReadSBlock(addr,offs,numb,blck) == LVL1::I2CInterface::SUCCESS) {
std::printf("RSBLK RSBLK RSBLK numb = %d\n",numb);
            if(numb == 0) {
                std::printf("Read:  addr 0x%02x, offs = 0x%02x, numb = 0 - PMBUS block has no data\n",addr,offs);
            }
            else {
                std::printf("Read:  addr 0x%02x, offs = 0x%02x, numb = %d:\n",addr,offs,numb);
                printData(blck,numb);
                printText(blck,numb);
            }
        }
    }
    else if(icmd == "WSBLK") {
        addr = idev;
        offs = ioff;
        numb = idat;
        if(i2c.WriteSBlock(addr,offs,numb,blck) == LVL1::I2CInterface::SUCCESS) {
            std::printf("Wrote: addr 0x%02x, offs = 0x%02x:\n",addr,offs);
            printData(blck,numb);
            printText(blck,numb);
        }
    }
    else if(icmd == "RPBLK") {
        addr = idev;
        offs = ioff;
        numb = idat;
        if(i2c.ReadPBlock(addr,offs,numb,blck) == LVL1::I2CInterface::SUCCESS) {
            std::printf("Read:  addr 0x%02x, offs = 0x%02x:\n",addr,offs);
            printData(blck,numb);
            printText(blck,numb);
        }
    }
    else if(icmd == "WPBLK") {
        addr = idev;
        offs = ioff;
        numb = idat;
        if(i2c.WritePBlock(addr,offs,numb,blck) == LVL1::I2CInterface::SUCCESS) {
            std::printf("Wrote: addr 0x%02x, offs = 0x%02x:\n",addr,offs);
            printData(blck,numb);
            printText(blck,numb);
        }
    }
    else if(icmd == "RPROC") {
        addr = idev;
        offs = ioff;
        dats = blck[0] | (blck[1]<<8);
        if(i2c.ReadProcessCall(addr,offs,dats) == LVL1::I2CInterface::SUCCESS) std::printf("Read:  addr 0x%02x, offs = 0x%02x, data = 0x%04x\n",addr,offs,dats);
    }
    else if(icmd == "BPROC") {
        addr = idev;
        offs = ioff;
        numb = idat;
        if(i2c.ReadBlockProcessCall(addr,offs,numb,blck) == LVL1::I2CInterface::SUCCESS) {
            std::printf("Read:  addr 0x%02x, offs = 0x%02x, data = 0x%04x\n",addr,offs,dats);
            printData(blck,numb);
            printText(blck,numb);
        }
    }
    else if(icmd == "RSMON") {
        addr = idev;
        offs = ioff;
        if(i2c.ReadSystemMonitor(addr,offs,dats) == LVL1::I2CInterface::SUCCESS) std::printf("Read:  addr 0x%02x, offs = 0x%02x, data = 0x%04x\n",addr,offs,dats);
    }
    else if(icmd == "WSMON") {
        addr = idev;
        offs = ioff;
        dats = idat;
        if(i2c.WriteSystemMonitor(addr,offs,dats) == LVL1::I2CInterface::SUCCESS) std::printf("Write: addr 0x%02x, offs = 0x%02x, data = 0x%04x\n",addr,offs,dats);
    }

    // end of for loop
    }
}
