//******************************************************************************
// file: menuI2CNetwork.cc
// desc: I2C Interface - menu program for I2C network
// auth: 05-AUG-2004, R. Spiwoks
// auth: 01-SEP-2016, R. Spiwoks, migrated to ZYNQ/petalinux
// auth: 13-FEB-2017, R. Spiwoks, migrated to ZynqI2C
//******************************************************************************

#include "I2C/I2CNetwork.h"

#ifdef __ZYNQ__
#include "I2C/I2CDevice.h"
#include "zynq_util/Utility.h"
#include "zynq_menu/Menu.h"
#include "zynq_log/Logging.h"
#define XUTIL Utility
using namespace LVL1;
using namespace ZYNQ;
#else
#include "I2C/I2C.h"
#include "I2C/I2CCommon.h"
#include "RCDMenu/RCDMenu.h"
#include "RCDUtilities/RCDUtilities.h"
#define XUTIL I2CCommon
using namespace LVL1;
using namespace RCD;
#endif

#include <cstdlib>
#include <iostream>
#include <sstream>

#ifdef __ZYNQ__
I2CDevice* i2c;
bool i2csflag;
int i2csaddr;
uint8_t i2csdata;
#else
VME* vme;
VMEMasterMap* vmm;
I2C* i2c;
#endif
I2CNetwork* i2cnet;
I2CNetwork::BUS_TYPE i2cmod;

//------------------------------------------------------------------------------

class I2COpen : public MenuItem {
  public:
    I2COpen() { setName("open I2C"); }
    int action() {

        int daddr(0xff);

#ifdef __ZYNQ__
        int bus = enterInt("I2C bus                         ",0,4);
        i2c = new I2CDevice(bus,false,I2CDevice::NO_PEC);

        i2csflag = enterInt("I2C switch         (0=no, 1=yes)",0,1);
        int dflag(false);
        if(i2csflag) {
            i2csaddr = enterHex("I2C switch address",0,0x7f);
            int rtnv(0);
            if((rtnv = i2c->Read(i2csaddr,i2csdata)) != I2CNetwork::SUCCESS) {
                CERR("reading data byte from switch address 0x%0x",i2csaddr);
                return(rtnv);
            }
            int sdata = enterHex("I2C switch data   ",0,0xff);
            if((rtnv = i2c->Write(i2csaddr,sdata)) != I2CNetwork::SUCCESS) {
                CERR("writing data byte 0x%0x to switch address 0x%0x",sdata,i2csaddr);
                return(rtnv);
            }
            dflag = true;
        }
        if(!dflag) {
            dflag = enterInt("I2C device address (0=no, 1=yes)",0,1);
        }
        if(dflag) {
            daddr = enterHex("I2C device address",0,0x7f);
        }
#else
        // open VME driver/library
        vme = VME::Open();

        // open VME master mapping
        unsigned int bus(0);
        I2C::MODULE_TYPE m = I2C::UNKNOWN;
        int sel = enterInt("Module type (1-CTPCORE, 2-CTPIN, 3-MICTP, 4-CTPCOREPLUS, 5-ALTI)", 0, 5);
        switch (sel) {
        case 1: {       // CTPCORE
                m = I2C::CTP_CORE;
                vmm = vme->MasterMap(0x58000000,0x100000,2,0);
            }
            break;

        case 2: {       // CTPIN
                m = I2C::CTP_IN;
                int slot = enterInt("Slot number: ", 7, 9);
                unsigned int vme_addr = (slot & 0x1F) << 27;
                vmm = vme->MasterMap(vme_addr,0x290000,2,0);
            }
            break;

        case 3: {       // MICTP
                m = I2C::MICTP;
                vmm = vme->MasterMap(0x60000000,0x80000,2,0);
            }
            break;

        case 4: {       // CTPCOREPLUS
                m = I2C::CTPCOREPLUS;
                bus = enterInt("Bus number: ",0,I2C::BUS_NUMBER-1);
                vmm = vme->MasterMap(0x58000000,0x210000,2,0);
            }
            break;

        case 5: {       // ALTI
                m = I2C::ALTI;
                u_int p = (u_int)enterHex("Vmebus_address   (hex)",0,0xffffffff);
                if(p < 0x100) p = p << 24;
                vmm = vme->MasterMap(p,0x01000000,2,0);
            }
            break;

        default: {
                unsigned int p = (unsigned int)enterHex("Vmebus_address                       (hex)",0,0xffffffff); if (p < 0x100) p = p << 24;
                unsigned int s = (unsigned int)enterHex("Window_size                          (hex)",0,0xffffffff);
                unsigned int a = (unsigned int)enterHex("Address_modifier                     (hex)",0,0xffffffff);
                vmm = vme->MasterMap(p,s,a,0);
            }
        }
        if((*vmm)()) {
            CERR("opening VME master mapping","");
            return(-1);
        }

        // create I2C
        i2c = new I2C(m,vmm,bus);
#endif

        unsigned int b;

        // create I2C
        i2cnet = new I2CNetwork();
        i2cnet->addI2CBus("BUS[0]",i2c,b);

        std::ostringstream buf;
        bool flag(false);
        for(unsigned int i=0; i<I2CNetwork::BUS_TYPE_NUMBER; i++) {
            buf << (flag?", ":"") << i << "=" << I2CNetwork::BUS_TYPE_NAME[i];
            flag = true;
        }
        i2cmod = static_cast<I2CNetwork::BUS_TYPE>(enterInt(("I2C model ("+buf.str()+")").c_str(),0,I2CNetwork::BUS_TYPE_NUMBER-1));
        i2cnet->initI2CBus(b,i2cmod,daddr);

        return(0);
    }
};

//------------------------------------------------------------------------------

class I2CClose : public MenuItem {
  public:
    I2CClose() { setName("close I2C"); }
    int action() {

#ifdef __ZYNQ__
        if(i2csflag) {
            int rtnv(0);
            if((rtnv = i2c->Write(i2csaddr,i2csdata)) != I2CNetwork::SUCCESS) {
                CERR("writing data byte 0x%0x to switch address 0x%0x",i2csaddr,i2csdata);
                return(rtnv);
            }
        }
#endif

        // delete I2C
        delete i2c;
        delete i2cnet;

#ifndef __ZYNQ__
        // close VME master mapping
        VME::Close();
#endif

        return(0);
    }
};

//------------------------------------------------------------------------------

class I2CDump : public MenuItem {
  public:
    I2CDump() { setName("dump  I2C"); }
    int action() {

        i2cnet->dump();

        return(0);
    }
};

//------------------------------------------------------------------------------

class I2CRead : public MenuItem {
  public:
    I2CRead() { setName("read  I2C"); }
    int action() {

        int rtnv(0);
        unsigned int bus, ndv, dev, qty, off, len, wdat, i;
        I2CInterface::TRANSFER_TYPE ttyp;
        I2CNetwork::DISPLAY_TYPE dtyp;
        std::string snam, sdat;
        std::vector<unsigned int> vdat;
        float fdat;

        bus = 0;
        ndv = i2cnet->numI2CDevices(bus);
        dev = (ndv == 1) ? 0 : (unsigned int)enterInt("I2C device   ",0,ndv-1);
        qty = (unsigned int)enterInt("I2C quantity ",0,i2cnet->numI2CQuantities(bus,dev)-1);

        // read I2C transfer type
        if((rtnv = i2cnet->transferType(bus,dev,qty,ttyp)) != I2CNetwork::SUCCESS) {
            CERR("reading transfer type from I2C bus/device/quantity %d/%d/%d",bus,dev,qty);
            return(rtnv);
        }

        // read I2C display type
        if((rtnv = i2cnet->displayType(bus,dev,qty,dtyp)) != I2CNetwork::SUCCESS) {
            CERR("reading display type from I2C bus/device/quantity %d/%d/%d",bus,dev,qty);
            return(rtnv);
        }

        // read I2C name
        if((rtnv = i2cnet->name(bus,dev,qty,snam)) != I2CNetwork::SUCCESS) {
            CERR("reading name from I2C bus/device/quantity %d/%d/%d",bus,dev,qty);
            return(rtnv);
        }

        // check I2C readability
        bool rdb;
        i2cnet->readable(bus,dev,qty,rdb);
        if(!rdb) {
            CERR("I2C bus/device/quantity %d/%d/%d is NOT READABLE",bus,dev,qty);
            return(rtnv);
        }

        // read I2C data
        if(dtyp == I2CNetwork::MEMORY) {

            unsigned int num = i2cnet->getNumberI2CQuantity(bus,dev,qty);
            if(i2cnet->getNameI2CQuantity(bus,dev,qty) == "MFR_FAULT_LOG") {
                off = 0;
                len = num;
            }
            else {
                off = (unsigned int)enterInt("MEM offset",0,num-1);
                len = (unsigned int)enterInt("MEM length",0,num-off);
            }

            // read I2C data as memory
            if((rtnv = i2cnet->read(bus,dev,qty,off,len,vdat)) != I2CNetwork::SUCCESS) {
                CERR("reading I2C data from bus %d, device %d, quantity %d - MEMORY",bus,dev,qty);
                return(rtnv);
            }

            if(i2cnet->getNameI2CQuantity(bus,dev,qty) == "MFR_FAULT_LOG") {
                if((i2cnet->getTypeI2CDevice(bus,dev) == "LTC3880") or (i2cnet->getTypeI2CDevice(bus,dev) == "LTM4676A")) {
                    i2cnet->ltc3880FaultLog(vdat,50,std::cout);
                }
                else {
                    i2cnet->ltm4676FaultLog(vdat,50,std::cout);
                }
            }
            else {
                bool flag(false);
                std::ostringstream buf;
                for(i=0; i<vdat.size(); i++) {
                    if(flag and (i%16 == 0)) { flag = false; buf.str(""); }
                    std::printf("%s0x%02x",flag?", ":"",vdat[i]);
                    if(vdat[i] < 32) buf << "?"; else if(vdat[i] >= 127) buf << "."; else buf << (char)vdat[i];
                    flag = true;
                    if(i%16 == 15) std::cout << "    " << buf.str() << std::endl;
                }
                if(i%16 != 0) std::cout << "    " << buf.str() << std::endl;
            }
        }
        else if(dtyp == I2CNetwork::WORD) {

            // read I2C data as word using PROC
            if(ttyp == I2CInterface::PROC) {
                if(i2cnet->getNameI2CQuantity(bus,dev,qty) == "SMBALERT_MASK") {
                    std::cout << "SMBALERT_MASK: 0x7a01=VOUT, 0x7b01=IOUT, 0x7c01=INPUT, 0x7d01=TEMP, 0x7e01=CML, 0x8001=MFR" << std::endl;
                }
                wdat = (unsigned int)enterNumber("DATA[PROCESS CALL]",0,0xffff);
            }

            // read I2C data as word
            if((rtnv = i2cnet->read(bus,dev,qty,wdat)) != I2CNetwork::SUCCESS) {
                CERR("reading data from I2C bus/device/quantity %d/%d/%d - WORD",bus,dev,qty);
                return(rtnv);
            }
            unsigned int num = i2cnet->getNumberI2CQuantity(bus,dev,qty);
            std::printf("\"%s\" = 0x%0*x\n",snam.c_str(),num*2,wdat);
        }
        else if(dtyp == I2CNetwork::FLOAT) {

            // read I2C data as float
            if((rtnv = i2cnet->read(bus,dev,qty,fdat)) != I2CNetwork::SUCCESS) {
                CERR("reading data from I2C bus/device/quantity %d/%d/%d - FLOAT",bus,dev,qty);
                return(rtnv);
            }
            std::printf("\"%s\" = %f\n",snam.c_str(),fdat);
        }
        else if(dtyp == I2CNetwork::STRING) {

            // read I2C data as string
            if((rtnv = i2cnet->read(bus,dev,qty,sdat)) != I2CNetwork::SUCCESS) {
                CERR("reading data from I2C bus/device/quantity %d/%d/%d - STRING",bus,dev,qty);
                return(rtnv);
            }
            std::printf("\"%s\" = \"%s\"\n",snam.c_str(),XUTIL::trimString(XUTIL::printableString(sdat)).c_str());
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class I2CReadNode : public MenuItem {
  public:
    I2CReadNode() { setName("read  I2C [node]"); }
    int action() {

        int rtnv(0);
        std::string bus, dev, qty, nam;
        I2CNetwork::I2CNode nod;

        bus = "BUS[0]";
        dev = enterString("I2C device  ");
        qty = enterString("I2C quantity");
        nam = bus + "/" + dev + "/" + qty;

        // read I2C node
        if((rtnv = i2cnet->getI2CNode(nam,nod)) != I2CNetwork::SUCCESS) {
            CERR("reading node from I2C bus/device/quantity \"%s\"",nam.c_str());
            return(rtnv);
        }

        // print I2C data
        if((rtnv = i2cnet->dumpI2CQuantity(nod)) != I2CNetwork::SUCCESS) {
            CERR("printing data from I2C node \"%s\"",nam.c_str());
            return(rtnv);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class I2CReadDevice : public MenuItem {
  public:
    I2CReadDevice() { setName("read  I2C [device]"); }
    int action() {

        int rtnv(0);
        unsigned int bus, dev, qty;
        std::string snam;

        bus = 0;
        i2cnet->name(bus,snam);
        std::cout << "----------------------------------------" << std::endl;
        std::printf("I2C bus %d: \"%s\":\n",bus,snam.c_str());
        for(dev=0; dev<i2cnet->numI2CDevices(bus); dev++) {
            i2cnet->name(bus,dev,snam);
            std::printf("I2C bus/device %d/%2d: \"%s\"\n",bus,dev,snam.c_str());
        }
        std::cout << "----------------------------------------" << std::endl;
        dev = (unsigned int)enterInt("I2C device ",0,i2cnet->numI2CDevices(bus)-1);

        for(qty=0; qty<i2cnet->numI2CQuantities(bus,dev); qty++) {
            if((rtnv = i2cnet->dumpI2CQuantity(bus,dev,qty)) != I2CNetwork::SUCCESS) {
                CERR("printing I2C bus/device/quantity %d/%d/%d",bus,dev,qty);
                continue;
            }
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class I2CReadAll : public MenuItem {
  public:
    I2CReadAll() { setName("read  I2C [all]"); }
    int action() {

        int rtnv(0);
        unsigned int bus, dev, qty;

        bus = 0;
        std::cout << "================================================================================" << std::endl;
        for(dev=0; dev<i2cnet->numI2CDevices(bus); dev++) {
            std::cout << "--------------------------------------------------------------------------------" << std::endl;
            for(qty=0; qty<i2cnet->numI2CQuantities(bus,dev); qty++) {
                if((rtnv = i2cnet->dumpI2CQuantity(bus,dev,qty)) != I2CNetwork::SUCCESS) {
                    CERR("printing I2C bus/device/quantity %d/%d/%d",bus,dev,qty);
                    continue;
                }
            }
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class I2CWrite : public MenuItem {
  public:
    I2CWrite() { setName("write I2C"); }
    int action() {

        int rtnv(0);
        unsigned int bus, ndv, dev, qty, wdat(0);
        I2CInterface::TRANSFER_TYPE ttyp;
        I2CNetwork::DISPLAY_TYPE dtyp;
        unsigned int dnum;
        std::string snam, sdat;
        std::ostringstream buf;

        bus = 0;
        ndv = i2cnet->numI2CDevices(bus);
        dev = (ndv == 1) ? 0 : (unsigned int)enterInt("I2C device   ",0,ndv-1);
        qty = (unsigned int)enterInt("I2C quantity ",0,i2cnet->numI2CQuantities(bus,dev)-1);

        // read I2C name
        if((rtnv = i2cnet->name(bus,dev,qty,snam)) != I2CNetwork::SUCCESS) {
            CERR("reading name from I2C bus/device/quantity %d/%d/%d",bus,dev,qty);
            return(rtnv);
        }

        // read I2C transfer type
        if((rtnv = i2cnet->transferType(bus,dev,qty,ttyp)) != I2CNetwork::SUCCESS) {
            CERR("reading transfer type from I2C bus/device/quantity %d/%d/%d",bus,dev,qty);
            return(rtnv);
        }

        // read I2C display type
        if((rtnv = i2cnet->displayType(bus,dev,qty,dtyp)) != I2CNetwork::SUCCESS) {
            CERR("reading display type from I2C bus/device/quantity %d/%d/%d",bus,dev,qty);
            return(rtnv);
        }

        // check I2C writability
        bool wtb;
        i2cnet->writable(bus,dev,qty,wtb);
        if(!wtb) {
            CERR("I2C bus/device/quantity %d/%d/%d is NOT WRITABLE",bus,dev,qty);
            return(rtnv);
        }

        // check transfer type
        if((ttyp != I2CInterface::BYTE) and (ttyp != I2CInterface::DATA) and (ttyp != I2CInterface::WORD)) {
            CERR("writing to I2C bus/device/quantity %d/%d/%d of transfer type \"%s\" NOT IMPLEMENTED",bus,dev,qty,I2CInterface::TRANSFER_TYPE_NAME[ttyp].c_str());
            return(rtnv);
        }

        // write I2C as word
        if(dtyp == I2CNetwork::WORD) {
            if((dnum = i2cnet->getNumberI2CQuantity(bus,dev,qty)) > 0) {
                if(dnum>4) {
                    CERR("writing to I2C bus/device/quantity %d/%d/%d of display type \"%s\" with more than 4 bytes (%d bytes) NOT IMPLEMENTED",bus,dev,qty,I2CNetwork::DISPLAY_TYPE_NAME[dtyp].c_str(),dnum);
                    return(rtnv);
                }
                if(i2cnet->getNameI2CQuantity(bus,dev,qty) == "SMBALERT_MASK") {
                    std::cout << "SMBALERT_MASK (0xmm): 0xmm7a=VOUT, 0xmm7b=IOUT, 0xmm7c=INPUT, 0xmm7d=TEMP, 0xmm7e=CML, 0xmm80=MFR" << std::endl;
                }
                unsigned int mdat(0xff);
                for(auto i=0U; i<(dnum-1); ++i) mdat = (mdat << 8) | 0xff;
                wdat = (unsigned int)enterNumber("DATA",0,mdat);
            }
            if((rtnv = i2cnet->write(bus,dev,qty,wdat)) != I2CNetwork::SUCCESS) {
                CERR("writing data from I2C bus/device/quantity %d/%d/%d - WORD",bus,dev,qty);
                return(rtnv);
            }
        }

        // write I2C as float
        else if(dtyp == I2CNetwork::FLOAT) {
            float fdat(0.0);
            if(i2cnet->getNumberI2CQuantity(bus,dev,qty)) {
                char buf[20];
                while(true) {
                    std::printf("DATA[FLOAT]: ");
                    buf[0] = '\0';
                    if(!cin) cin.clear();
                    cin >> buf;
                    if(buf[0] == '\0') {
                        continue;
                    }
                    else {
                        fdat = strtof(buf,nullptr);
                        break;
                    }
                }
                cin.getline(buf,1);
            }
            if((rtnv = i2cnet->write(bus,dev,qty,fdat)) != I2CNetwork::SUCCESS) {
                CERR("writing data to I2C bus/device/quantity %d/%d/%d - FLOAT",bus,dev,qty);
                return(rtnv);
            }
        }

        // write I2C as string
        else if(dtyp == I2CNetwork::STRING) {
            std::string sdat;
            if(i2cnet->getNumberI2CQuantity(bus,dev,qty)) {
                sdat = enterString("DATA[STRING]");
            }
            if((rtnv = i2cnet->write(bus,dev,qty,sdat)) != I2CNetwork::SUCCESS) {
                CERR("writing data to I2C bus/device/quantity %d/%d/%d - STRING",bus,dev,qty);
                return(rtnv);
            }
        }

        else {
            CERR("writing to I2C bus/device/quantity %d/%d/%d of display type \"%s\" NOT IMPLEMENTED",bus,dev,qty,I2CNetwork::DISPLAY_TYPE_NAME[dtyp].c_str());
            return(rtnv);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int main() {

    Menu menu("I2C main menu");

    // menu for I2C
    menu.init(new I2COpen);
    menu.add(new I2CDump);
    menu.add(new I2CRead);
    menu.add(new I2CReadNode);
    menu.add(new I2CReadDevice);
    menu.add(new I2CReadAll);
    menu.add(new I2CWrite);
    menu.exit(new I2CClose);

    // execute menu
    menu.execute();

    exit(0);
}
