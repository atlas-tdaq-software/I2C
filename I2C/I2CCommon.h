#ifndef _I2CCOMMON_H_
#define _I2CCOMMON_H_

//******************************************************************************
// file: I2CCommon.h
// desc: pure static class for the I2C common parameters
// auth: 29-JAN-2019 R. Spiwoks, copied from MuctpiCommon.h
//******************************************************************************

#include <string>
#include <vector>

namespace LVL1 {

// I2C common class
class I2CCommon {

  public:

    // public type definitions

    // public constructor & destructor

    // public methods
    static std::string  rightTrimString(const std::string&);
    static std::string  leftTrimString(const std::string&);
    static std::string  trimString(const std::string&);
    static std::string  printableString(const std::string&);
    static int          splitString(const std::string&, const std::string&, std::vector<std::string>&);

};      // class I2CCommon

}       // namespace LVL1

#endif  // define _I2CCOMMON_H_
