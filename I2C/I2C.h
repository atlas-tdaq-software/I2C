#ifndef LVL1_I2C_H
#define LVL1_I2C_H

//******************************************************************************
// file: I2C.h
// desc: I2C Interface
// auth: 05-AUG-2004 R. Spiwoks
// modf: 06-SEP-2004 R. Spiwoks, added support for CTPCORE
//******************************************************************************

#include "I2C/I2C_BITSTRING.h"
#include "I2C/I2CInterface.h"
#include "RCDVme/RCDVme.h"

namespace LVL1 {

// I2C controller class
class I2C : public I2CInterface {

  public:

    typedef enum {UNKNOWN, CTP_CORE, CTP_IN, MICTP, CTPCOREPLUS, ALTI} MODULE_TYPE;
    static const int          MODULE_NUMBER = ALTI + 1;
    static const std::string  MODULE_NAME[MODULE_NUMBER];
    static const unsigned int BUS_NUMBER        =          6;

    static const unsigned int PRSC_MASK         = 0x0000ffff;

    // constructor and desctructor
    I2C(MODULE_TYPE, RCD::VMEMasterMap*, const unsigned int = 0, const unsigned int = 0);
    virtual ~I2C();
    virtual bool remote() const                 { return(false); }

    // low-level (register) functions - DEPRECATED !!!!
    int PrescaleLow_Read(unsigned int&);
    int PrescaleLow_Write(unsigned int);
    int PrescaleHigh_Read(unsigned int&);
    int PrescaleHigh_Write(unsigned int);
    int Control_Read(I2C_CONTROL_BITSTRING&);
    int Control_Write(I2C_CONTROL_BITSTRING&);
    int TransmitData_Read(unsigned int&);
    int TransmitData_Write(unsigned int);
    int TransmitAddr_Read(I2C_TRANSMITADDR_BITSTRING&);
    int TransmitAddr_Write(I2C_TRANSMITADDR_BITSTRING&);
    int Receive_Read(unsigned int&);
    int Receive_Write(unsigned int);
    int Command_Read(I2C_COMMAND_BITSTRING&);
    int Command_Write(I2C_COMMAND_BITSTRING&);
    int Status_Read(I2C_STATUS_BITSTRING&);
    int Status_Write(I2C_STATUS_BITSTRING&);

    // high-level functions - ONLY FOR COMPATIBILITY !!!!
    int Prescale(unsigned int);                 // only for compatibility
    int Enable();                               // only for compatibility
    int Disable();                              // only for compatibility
    int Read(unsigned int, unsigned int&);      // only for compatibility
    int Write(unsigned int, unsigned int);      // only for compatibility

    // high-level functions - RECOMMENDED !!!!
    int PrescaleRead(unsigned int&);
    int PrescaleWrite(const unsigned int p)                             { return(const_cast<I2C*>(this)->Prescale(p)); }
    int EnableRead(bool&);
    int EnableWrite(const bool);
    int DataRead(const unsigned int a, unsigned int& d)                 { return(const_cast<I2C*>(this)->Read(a,d)); }
    int DataWrite(const unsigned int a, const unsigned int d)           { return(const_cast<I2C*>(this)->Write(a,d)); }
    int DataWrite(const unsigned int, const unsigned int, const unsigned int);

    // higher-level functions (for local I2C interface)
    int DataRead(const unsigned int a, uint8_t& d);
    int DataRead(const unsigned int, const unsigned int, uint8_t&);
    int DataRead(const unsigned int, const unsigned int, uint16_t&);
    int DataRead(const unsigned int, const unsigned int, unsigned int&, unsigned int[]);
    int DataWrite(const unsigned int a, const uint8_t d);
    int DataWrite(const unsigned int a, const unsigned int o, const uint8_t d);
    int DataWrite(const unsigned int a, const unsigned int o, const uint16_t d);
    int DataWrite(const unsigned int, const unsigned int, const unsigned int, const unsigned int[]);

    // local I2C interface
    virtual int DataRead(const unsigned int, const unsigned int, unsigned int&, uint8_t[], const TRANSFER_TYPE);
    virtual int DataWrite(const unsigned int, const unsigned int, unsigned int&, const uint8_t[], const TRANSFER_TYPE);

    //public static helpers
    static std::string printAddress(const unsigned int);
    static std::string printCommand(const unsigned int);
    static std::string printStatus(const unsigned int);

  private:

    // private constants
    static const unsigned int   ADDR_CTPCOREPLUS_OFFSET = 0x00000100;
    static const unsigned int   ADDR_PRESCALE_LOW       = 0x00000000;
    static const unsigned int   ADDR_PRESCALE_HIGH      = 0x00000004;
    static const unsigned int   ADDR_CONTROL            = 0x00000008;
    static const unsigned int   ADDR_TRANSMIT_DATA      = 0x0000000c;
    static const unsigned int   ADDR_TRANSMIT_ADDR      = 0x0000000c;
    static const unsigned int   ADDR_RECEIVE            = 0x0000000c;
    static const unsigned int   ADDR_COMMAND            = 0x00000010;
    static const unsigned int   ADDR_STATUS             = 0x00000010;
    static const unsigned int   PRSC_LL_MASK            = 0x000000ff;
    static const unsigned int   PRSC_LL_SHIFT           =          8;
    static const unsigned int   CMMD_STA_MASK           = 0x00000080;
    static const unsigned int   CMMD_STO_MASK           = 0x00000040;
    static const unsigned int   CMMD_RD_MASK            = 0x00000020;
    static const unsigned int   CMMD_WR_MASK            = 0x00000010;
    static const unsigned int   CMMD_ACK_MASK           = 0x00000008;
    static const unsigned int   STAT_ACK_MASK           = 0x00000080;
    static const unsigned int   STAT_BSY_MASK           = 0x00000040;
    static const unsigned int   STAT_TIP_MASK           = 0x00000002;
    static const unsigned int   STAT_IRQ_MASK           = 0x00000001;
    static const unsigned int   STAT_TIMEOUT            =        100;
    static const unsigned int   STRING_LENGTH           =       1024;

    // private members
    I2C_COMMAND_BITSTRING       m_cmd;
    I2C_STATUS_BITSTRING        m_stat;
    RCD::VMEMasterMap*          m_vmm;
    unsigned int                m_base;
    unsigned int                m_i2c_prescale_low;
    unsigned int                m_i2c_prescale_high;
    unsigned int                m_i2c_control;
    unsigned int                m_i2c_transmit_data;
    unsigned int                m_i2c_transmit_addr;
    unsigned int                m_i2c_receive;
    unsigned int                m_i2c_command;
    unsigned int                m_i2c_status;

    // private constructor (for initialization)
    I2C();

    // private methods
    int WaitTransfer(const bool);
    int WriteAddress(const unsigned int, const bool);
    int WriteData(const unsigned int, const bool);
    int ReadData(unsigned int&, const bool);

};      // class I2C

}       // namespace LVL1

#endif  // LVL1_I2C_H
