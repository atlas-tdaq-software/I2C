#ifndef _I2CINTERFACE_H_
#define _I2CINTERFACE_H_

//*****************************************************************************
// file: I2CInterface.h
// desc: class for I2C (abstract) interface
// auth: 15-FEB-2017 R. Spiwoks
//*****************************************************************************

#include <string>
#include <vector>
#include <stdint.h>

namespace LVL1 {

// I2C interface class
class I2CInterface {

  public:

    // public type definitions
    typedef enum { BYTE=0, DATA, WORD, IBLK, SBLK, PBLK, PROC, SMON } TRANSFER_TYPE;
    static const unsigned int   TRANSFER_TYPE_NUMBER = SMON + 1;
    static const std::string    TRANSFER_TYPE_NAME[TRANSFER_TYPE_NUMBER];

    // public constants
    static const unsigned int   ADDR_MASK       = 0x0000007f;
    static const unsigned int   DATA_MASK       = 0x000000ff;
    static const unsigned int   DATA_SIZE       = DATA_MASK + 1;
    static const unsigned int   MAX_BLOCK       = DATA_SIZE + 32;

    static const int            SUCCESS         =       0;
    static const int            FAILURE         =      -1;
    static const int            TIMEOUT         =      -2;
    static const int            WRONGPAR        =      -3;
    static const int            NOTIMPL         =      -4;

    // public constructor/destructor
    I2CInterface() = delete;
    I2CInterface(const unsigned int d)  : m_xmit(true), m_debug(d), m_status(SUCCESS) {}
    virtual ~I2CInterface() {}
    virtual int operator()() const      { return(m_status); }
    virtual bool remote() const = 0;
    bool xmit() const                   { return(m_xmit); }
    void xmit(const bool x)             { m_xmit = x; }

    // public methods - local I2C interface
    virtual int DataRead(const unsigned int, const unsigned int, unsigned int&, uint8_t[], const TRANSFER_TYPE) { return(NOTIMPL); }
    virtual int DataWrite(const unsigned int, const unsigned int, unsigned int&, const uint8_t[], const TRANSFER_TYPE) { return(NOTIMPL); }

    virtual void lockBus() {};
    virtual void unlockBus() {};

    // public methods - remote I2C interface
    virtual int DataRead(const unsigned int, const unsigned int, unsigned int&) { return(NOTIMPL); }
    virtual int DataRead(const unsigned int, const unsigned int, float&)        { return(NOTIMPL); }
    virtual int DataRead(const unsigned int, const unsigned int, std::string&)  { return(NOTIMPL); }
    virtual int DataRead(const unsigned int, const unsigned int, const unsigned int, const unsigned int, std::vector<unsigned int>&) { return(NOTIMPL); }
    virtual int DataWrite(const unsigned int, const unsigned int, const unsigned int) { return(NOTIMPL); }
    virtual int DataWrite(const unsigned int, const unsigned int, const float) { return(NOTIMPL); }

    // public static methods
    static unsigned int crc(const unsigned int, const unsigned int, const unsigned int[], const unsigned int, const unsigned int, const bool = true);
    static unsigned int crc(const unsigned int, const unsigned int, const uint8_t[], const unsigned int, const uint8_t, const bool = true);
    static unsigned int crcpoly107(const unsigned int[], const unsigned int, const unsigned int);
    static unsigned int crcpoly107(const uint8_t[], const unsigned int, const uint8_t);

  protected:

    // protected members
    bool                        m_xmit;
    unsigned int                m_debug;
    int                         m_status;

    // protected constants
    static const unsigned int   ADDR_SHFT       =          1;
    static const unsigned int   READ_MASK       = 0x00000001;
    static const unsigned int   PEC_SIZE_READ   =          3;
    static const unsigned int   PEC_SIZE_WRITE  =          2;

};      // class I2CInterface

}       // namespace LVL1

#endif  // _I2CINTERFACE_H_
