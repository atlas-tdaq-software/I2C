#ifndef LVL1_I2C_BITSRTING_H
#define LVL1_I2C_BITSTRING_H

//******************************************************************************
// file: I2C_BITSTRING.h
// desc: I2C Interface - Bit String
// auth: 05/08/04, Ralf SPIWOKS
//******************************************************************************

// $Id: I2C_BITSTRING.h,v 1.2 2007/04/13 09:14:04 berge Exp $

#include "RCDBitString/BitSet.h"

namespace LVL1 {

  //------------------------------------------------------------------------------

  class I2C_CONTROL_BITSTRING : public RCD::BitSet {

  public:
    // constructor and destructor
    I2C_CONTROL_BITSTRING();
    ~I2C_CONTROL_BITSTRING();

    // assignment operator
    I2C_CONTROL_BITSTRING& operator=(const RCD::BitSet&);

    // field functions
    std::string Enable() const;
    void        Enable(const std::string& value);

    std::string InterruptEnable() const;
    void        InterruptEnable(const std::string& value);

    // printing function
    void print();

  private:

    static const RCD::BitSet MASK_ENABLE;
    static const RCD::BitSet VALUE_ENABLE_DISABLED;
    static const RCD::BitSet VALUE_ENABLE_ENABLED;

    static const RCD::BitSet MASK_INTERRUPTENABLE;
    static const RCD::BitSet VALUE_INTERRUPTENABLE_DISABLED;
    static const RCD::BitSet VALUE_INTERRUPTENABLE_ENABLED;

  };

  //------------------------------------------------------------------------------

  class I2C_TRANSMITADDR_BITSTRING : public RCD::BitSet {

  public:
    // constructor and destructor
    I2C_TRANSMITADDR_BITSTRING();
    ~I2C_TRANSMITADDR_BITSTRING();

    // assignment operator
    I2C_TRANSMITADDR_BITSTRING& operator=(const RCD::BitSet&);

    // field functions
    std::string SlaveAddress() const;
    void        SlaveAddress(const std::string& value);

    std::string ReadWriteFlag() const;
    void        ReadWriteFlag(const std::string& value);

    // printing function
    void print();

  private:

    static const RCD::BitSet MASK_SLAVEADDRESS;

    static const RCD::BitSet MASK_READWRITEFLAG;
    static const RCD::BitSet VALUE_READWRITEFLAG_READ;
    static const RCD::BitSet VALUE_READWRITEFLAG_WRITE;

  };

  //------------------------------------------------------------------------------

  class I2C_COMMAND_BITSTRING : public RCD::BitSet {

  public:
    // constructor and destructor
    I2C_COMMAND_BITSTRING();
    ~I2C_COMMAND_BITSTRING();

    // assignment operator
    I2C_COMMAND_BITSTRING& operator=(const RCD::BitSet&);

    // field functions
    std::string STA() const;
    void        STA(const std::string& value);

    std::string STO() const;
    void        STO(const std::string& value);

    std::string RD() const;
    void        RD(const std::string& value);

    std::string WR() const;
    void        WR(const std::string& value);

    std::string ACK() const;
    void        ACK(const std::string& value);

    std::string IACK() const;
    void        IACK(const std::string& value);

    // printing function
    void print();

  private:

    static const RCD::BitSet MASK_STA;
    static const RCD::BitSet VALUE_STA_OFF;
    static const RCD::BitSet VALUE_STA_ON;

    static const RCD::BitSet MASK_STO;
    static const RCD::BitSet VALUE_STO_OFF;
    static const RCD::BitSet VALUE_STO_ON;

    static const RCD::BitSet MASK_RD;
    static const RCD::BitSet VALUE_RD_OFF;
    static const RCD::BitSet VALUE_RD_ON;

    static const RCD::BitSet MASK_WR;
    static const RCD::BitSet VALUE_WR_OFF;
    static const RCD::BitSet VALUE_WR_ON;

    static const RCD::BitSet MASK_ACK;
    static const RCD::BitSet VALUE_ACK_OFF;
    static const RCD::BitSet VALUE_ACK_ON;

    static const RCD::BitSet MASK_IACK;
    static const RCD::BitSet VALUE_IACK_OFF;
    static const RCD::BitSet VALUE_IACK_ON;

  };

  //------------------------------------------------------------------------------

  class I2C_STATUS_BITSTRING : public RCD::BitSet {

  public:
    // constructor and destructor
    I2C_STATUS_BITSTRING();
    ~I2C_STATUS_BITSTRING();

    // assignment operator
    I2C_STATUS_BITSTRING& operator=(const RCD::BitSet&);

    // field functions
    std::string RxACK() const;
    void        RxACK(const std::string& value);

    std::string Busy() const;
    void        Busy(const std::string& value);

    std::string Transfer() const;
    void        Transfer(const std::string& value);

    std::string Interrupt() const;
    void        Interrupt(const std::string& value);

    // printing function
    void print();

  private:

    static const RCD::BitSet MASK_RXACK;
    static const RCD::BitSet VALUE_RXACK_NONE_RECEIVED;
    static const RCD::BitSet VALUE_RXACK_RECEIVED;

    static const RCD::BitSet MASK_BUSY;
    static const RCD::BitSet VALUE_BUSY_START_DETECTED;
    static const RCD::BitSet VALUE_BUSY_STOP_DETECTED;

    static const RCD::BitSet MASK_TRANSFER;
    static const RCD::BitSet VALUE_TRANSFER_COMPLETED;
    static const RCD::BitSet VALUE_TRANSFER_IN_PROGRESS;

    static const RCD::BitSet MASK_INTERRUPT;
    static const RCD::BitSet VALUE_INTERRUPT_OFF;
    static const RCD::BitSet VALUE_INTERRUPT_ON;

  };

}     // namespace LVL1

#endif // LVL1_I2C_BITSTRING_H
