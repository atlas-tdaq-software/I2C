#ifndef _I2CNETWORK_H_
#define _I2CNETWORK_H_

//*****************************************************************************
// file: I2CNetwork.h
// desc: class for I2C network
// auth: 26-MAR-2015 R. Spiwoks
//*****************************************************************************

#include "I2C/I2CInterface.h"

#include <string>
#include <vector>
#include <iostream>

namespace LVL1 {

// I2C network class
class I2CNetwork {

  public:

    // public type
    typedef enum {WORD, FLOAT, STRING, MEMORY} DISPLAY_TYPE;
    static const unsigned int           DISPLAY_TYPE_NUMBER = MEMORY + 1;
    static const std::string            DISPLAY_TYPE_NAME[DISPLAY_TYPE_NUMBER];

    typedef enum {BUS_LTC2990, BUS_LTC2991, BUS_LTC3880, BUS_LTM4676, BUS_RXMPODLO, BUS_RXMPODUP1, BUS_SFP, BUS_SFPEXTD,
                  BUS_ZC706, BUS_MUCTPI0, BUS_MUCTPI0_V2V3, BUS_MUCTPI1, BUS_ZCU102, BUS_ALTI, BUS_ALTI2, BUS_LTI_DEMO,
                  BUS_LTI_FMC} BUS_TYPE;
    static const unsigned int           BUS_TYPE_NUMBER = BUS_LTI_FMC + 1;
    static const std::string            BUS_TYPE_NAME[BUS_TYPE_NUMBER];

    class I2CNode {
      public:
        I2CNode();
        I2CNode(const I2CNode&);
        I2CNode& operator=(const I2CNode&);
       ~I2CNode();
        std::string name() const        { return(m_name); }
        unsigned int bus() const        { return(m_bus); }
        unsigned int dev() const        { return(m_dev); }
        unsigned int qty() const        { return(m_qty); }
        void dump() const;
        friend I2CNetwork;
      private:
        std::string                     m_name;
        unsigned int                    m_bus;
        unsigned int                    m_dev;
        unsigned int                    m_qty;
    };

    // public constants
    static const int                    SUCCESS         = I2CInterface::SUCCESS;
    static const int                    FAILURE         = I2CInterface::FAILURE;
    static const int                    TIMEOUT         = I2CInterface::TIMEOUT;
    static const int                    WRONGPAR        = I2CInterface::WRONGPAR;

    // public constructor/destructor
    I2CNetwork();
   ~I2CNetwork();

    // public methods
    int addI2CBus(const std::string&, I2CInterface*, unsigned int&);
    int initI2CBus(const unsigned int, const BUS_TYPE, const unsigned int = 0);
    int clearI2CBus(const unsigned int);
    int addI2CDevice(const unsigned int, const std::string&, const unsigned int, unsigned int&);
    int typeI2CDevice(const unsigned int, const unsigned int, const std::string&);
    int switchI2CDevice(const unsigned int, const unsigned int, const unsigned int, const unsigned int);
    int pageI2CDevice(const unsigned int, const unsigned int, const unsigned int, const unsigned int);
    int pmbusI2CDevice(const unsigned int, const unsigned int);

    int avagoSFP(const unsigned int, const unsigned int);
    int avagoSFPExtended(const unsigned int, const unsigned int);
    int avagoQSFPPlusLO(const unsigned int, const unsigned int);
    int avagoQSFPPlusUP3(const unsigned int, const unsigned int);
    int ddr3SPD(const unsigned int, const unsigned int);
    int avagoMiniPodRXLO(const unsigned int, const unsigned int);
    int avagoMiniPodRXUP1(const unsigned int, const unsigned int);
    int avagoMiniPodTXLO(const unsigned int, const unsigned int);
    int avagoMiniPodTXUP1(const unsigned int, const unsigned int);
    int samtecFireFlyRXLO(const unsigned int, const unsigned int);
    int samtecFireFlyRXUP1(const unsigned int, const unsigned int);
    int samtecFireFlyTXLO(const unsigned int, const unsigned int);
    int samtecFireFlyTXUP1(const unsigned int, const unsigned int);
    int pdt012a(const unsigned int, const unsigned int);
    int si570(const unsigned int, const unsigned int);
    int adv7511(const unsigned int, const unsigned int);
    int m24c08(const unsigned int, const unsigned int);
    int ddr3sodimm(const unsigned int, const unsigned int);
    int rtc8564je(const unsigned int, const unsigned int);
    int si5324(const unsigned int, const unsigned int);
    int ucd90120a(const unsigned int, const unsigned int);
    int ltc2990(const unsigned int, const unsigned int);
    int ltc2991(const unsigned int, const unsigned int);
    int ltm4676(const unsigned int, const unsigned int);
    int ltm4676_p1(const unsigned int, const unsigned int);
    int adn2814(const unsigned int, const unsigned int);
    int abcu5740(const unsigned int, const unsigned int);
    int ultrascaleSysmon(const unsigned int, const unsigned int);
    // added modules
    int ad5305(const unsigned int, const unsigned int); // AD5305 DAC    
    int ds10cp154(const unsigned int, const unsigned int);  // DS10CP154 cross-point switch
    int si53xx_p0(const unsigned int, const unsigned int); // SI5344B jitter cleaner, page 0
    int si53xx_p2(const unsigned int, const unsigned int); // SI5344B jitter cleaner, page 2
    int ftlf1323(const unsigned int, const unsigned int); // FTLF1323 SFP transceiver
    int ftlf1323Extended(const unsigned int, const unsigned int); // FTLF1323 SFP transceiver
    int max1617(const unsigned int, const unsigned int); // MAX1617 temperature sensor
    int max6695(const unsigned int, const unsigned int); // MAX6695 temperature sensor
    int max15303(const unsigned int, const unsigned int); // MAX15303 temperature sensor
    void ltc3880FaultLog(const std::vector<unsigned int>&, const unsigned int = 0, std::ostream& = std::cout);
    void ltm4676FaultLog(const std::vector<unsigned int>&, const unsigned int = 0, std::ostream& = std::cout);

    int addI2CQuantity(const unsigned int, const unsigned int, const I2CInterface::TRANSFER_TYPE, const std::string&, const unsigned int, const unsigned int, unsigned int&);
    int readableI2CQuantity(const unsigned int, const unsigned int, const unsigned int, const bool);
    int writableI2CQuantity(const unsigned int, const unsigned int, const unsigned int, const bool);
    int displayI2CQuantity(const unsigned int, const unsigned int, const unsigned int, const DISPLAY_TYPE);
    int floatI2CQuantity(const unsigned int, const unsigned int, const unsigned int, const bool, const float, const float = 0.0);
    int endianI2CQuantity(const unsigned int, const unsigned int, const unsigned int, const bool);
    int signedI2CQuantity(const unsigned int, const unsigned int, const unsigned int, const bool, const bool = false);
    int convertI2CQuantity(const unsigned int, const unsigned int, const unsigned int, const float, const float = 0.0);
    int exponentI2CQuantity(const unsigned int, const unsigned int, const unsigned int, const unsigned int, const int);
    int maskI2CQuantity(const unsigned int, const unsigned int, const unsigned int, const unsigned int = 0xffffffff);

    int read(const unsigned int, const unsigned int, const unsigned int, unsigned int&, const bool = false);
    int read(const unsigned int, const unsigned int, const unsigned int, float&, const bool = false);
    int read(const unsigned int, const unsigned int, const unsigned int, std::string&, const bool = false);
    int read(const unsigned int, const unsigned int, const unsigned int, std::vector<unsigned int>&, const bool = false);
    int read(const unsigned int, const unsigned int, const unsigned int, const unsigned int, const unsigned int, std::vector<unsigned int>&, const bool = false);

    int read(const I2CNode& n, unsigned int& d)                 { return(read(n.m_bus,n.m_dev,n.m_qty,d,true)); }
    int read(const I2CNode& n, float& d)                        { return(read(n.m_bus,n.m_dev,n.m_qty,d,true)); }
    int read(const I2CNode& n, std::string& d)                  { return(read(n.m_bus,n.m_dev,n.m_qty,d,true)); }
    int read(const I2CNode& n, std::vector<unsigned int>& d)    { return(read(n.m_bus,n.m_dev,n.m_qty,d,true)); }
    int read(const I2CNode& n, const unsigned int o, const unsigned int l, std::vector<unsigned int>& d)
                                                                { return(read(n.m_bus,n.m_dev,n.m_qty,o,l,d,true)); }

    int write(const unsigned int, const unsigned int, const unsigned int, const unsigned int, const bool = false);
    int write(const unsigned int, const unsigned int, const unsigned int, const float, const bool = false);
    int write(const unsigned int, const unsigned int, const unsigned int, const std::string&, const bool = false);
    int write(const unsigned int, const unsigned int, const unsigned int, const std::vector<unsigned int>&, const bool = false);
    int write(const unsigned int, const unsigned int, const unsigned int, const unsigned int, const std::vector<unsigned int>&, const bool = false);

    int write(const I2CNode& n, const unsigned int d)           { return(write(n.m_bus,n.m_dev,n.m_qty,d,true)); }
    int write(const I2CNode& n, const float d)                  { return(write(n.m_bus,n.m_dev,n.m_qty,d,true)); }
    int write(const I2CNode& n, const std::string& d)           { return(write(n.m_bus,n.m_dev,n.m_qty,d,true)); }
    int write(const I2CNode& n, const std::vector<unsigned int>& d) { return(write(n.m_bus,n.m_dev,n.m_qty,d,true)); }
    int write(const I2CNode& n, const unsigned int o, const std::vector<unsigned int>& d)
                                                                { return(write(n.m_bus,n.m_dev,n.m_qty,o,d,true)); }

    bool exists(const unsigned int b) const                     { return(b<m_busses.size()); }
    bool exists(const unsigned int b, const unsigned int d) const { return((b<m_busses.size()) and (d<m_busses[b]->numI2CDevices())); }
    bool exists(const unsigned int b, const unsigned int d, const unsigned int q) const
                                                                { return((b<numI2CBusses()) and (d<numI2CDevices(b)) and (q<numI2CQuantities(b,d))); }
    unsigned int numI2CBusses() const                           { return(m_busses.size()); }
    unsigned int numI2CDevices(const unsigned int b) const      { return(m_busses[b]->numI2CDevices()); }
    unsigned int numI2CQuantities(const unsigned int b, const unsigned int d) const
                                                                {  return(m_busses[b]->getI2CDevice(d)->numI2CQuantities()); }
    I2CInterface* getI2CBus(const unsigned int) const;
    std::string getTypeI2CDevice(const unsigned int, const unsigned int) const;
    unsigned int getAddressI2CDevice(const unsigned int, const unsigned int) const;
    unsigned int getSwitchAddressI2CDevice(const unsigned int, const unsigned int) const;
    unsigned int getSwitchSelectI2CDevice(const unsigned int, const unsigned int) const;
    unsigned int getPageOffsetI2CDevice(const unsigned int, const unsigned int) const;
    unsigned int getNumberI2CQuantity(const unsigned int, const unsigned int, const unsigned int) const;
    unsigned int getNumberI2CQuantity(const I2CNode&) const;
    std::string getNameI2CQuantity(const unsigned int, const unsigned int, const unsigned int) const;
    I2CInterface::TRANSFER_TYPE getTransferTypeI2CQuantity(const unsigned int, const unsigned int, const unsigned int) const;
    DISPLAY_TYPE getDisplayTypeI2CQuantity(const unsigned int, const unsigned int, const unsigned int) const;
    int transferType(const unsigned int, const unsigned int, const unsigned int, I2CInterface::TRANSFER_TYPE&) const;
    int displayType(const unsigned int, const unsigned int, const unsigned int, DISPLAY_TYPE&) const;
    int name(const unsigned int, std::string&) const;
    int name(const unsigned int, const unsigned int, std::string&) const;
    int name(const unsigned int, const unsigned int, const unsigned int, std::string&) const;
    int readable(const unsigned int, const unsigned int, const unsigned int, bool&) const;
    int writable(const unsigned int, const unsigned int, const unsigned int, bool&) const;
    void dump() const;
    int dumpI2CQuantity(const unsigned int, const unsigned int, const unsigned int);
    int dumpI2CQuantity(const I2CNode& n)                       { return(dumpI2CQuantity(n.m_bus,n.m_dev,n.m_qty)); }
    int findI2CDevice(const unsigned int, const unsigned int, const std::string&, unsigned int&);
    int findI2CQuantity(const unsigned int, const unsigned int, const std::string&, unsigned int&);
    int getI2CNode(const std::string&, I2CNode&);
    int getI2CNode(const unsigned int, const unsigned int, const unsigned int, I2CNode&);

  private:

    // private nested class
    class I2CQuantity {
      public:
        // public methods
        I2CQuantity(const I2CInterface::TRANSFER_TYPE, const std::string&, const unsigned int, const unsigned int);
       ~I2CQuantity();
        I2CInterface::TRANSFER_TYPE getTransferType() const     { return(m_transfer_type); }
        DISPLAY_TYPE getDisplayType() const                     { return(m_display_type); }
        std::string getName() const                             { return(m_name); }
        unsigned int getOffset() const                          { return(m_offset); }
        unsigned int getNumber() const                          { return(m_number); }
        bool getReadable() const                                { return(m_readable); }
        bool getWritable() const                                { return(m_writable); }
        int readableI2CQuantity(const bool);
        int writableI2CQuantity(const bool);
        int displayI2CQuantity(const DISPLAY_TYPE);
        int endianI2CQuantity(const bool);
        int signedI2CQuantity(const bool, const bool);
        int exponentI2CQuantity(const unsigned int, const int);
        int convertI2CQuantity(const float, const float = 0.0);
        int maskI2CQuantity(const unsigned int = 0xffffffff);
        unsigned int bytes2word(const uint8_t[]);
        void word2bytes(const unsigned int, const unsigned int, uint8_t[]);
        float word2float(const unsigned int);
        unsigned int float2word(const float);
        void dump(const unsigned int) const;
      private:
        // private members
        I2CInterface::TRANSFER_TYPE     m_transfer_type;
        DISPLAY_TYPE                    m_display_type;
        std::string                     m_name;
        unsigned int                    m_offset;
        unsigned int                    m_number;
        bool                            m_readable;
        bool                            m_writable;
        bool                            m_little_endian;
        bool                            m_mantissa_signed;
        bool                            m_exponent_signed;
        unsigned int                    m_exponent_width;
        int                             m_exponent_offset;
        float                           m_convert_factor;
        float                           m_convert_offset;
        unsigned int                    m_convert_mask;
        // private methods
        int word2number(const unsigned int, const unsigned int, const bool);
        int number2word(const unsigned int, const unsigned int, const bool);
        int word2range(const unsigned int, const unsigned int, const unsigned int);
        unsigned int range2word(const unsigned int, const unsigned int, const unsigned int);
        std::string rw() const;
    };

    // private nested class
    class I2CDevice {
      public:
        // public methods
        I2CDevice(const std::string&, const unsigned int);
       ~I2CDevice();
        std::string getType() const                             { return(m_type); }
        std::string getName() const                             { return(m_name); }
        unsigned int getAddress() const                         { return(m_address); }
        unsigned int getSwitchAddress() const                   { return(m_switch_address); }
        unsigned int getSwitchSelect() const                    { return(m_switch_select); }
        bool getPageFlag() const                                { return(m_page_flag); }
        unsigned int getPageOffset() const                      { return(m_page_offset); }
        unsigned int getPageSelect() const                      { return(m_page_select); }
        bool getPmbus() const                                   { return(m_pmbus); }
        int typeI2CDevice(const std::string&);
        int switchI2CDevice(const unsigned int, const unsigned int);
        int pageI2CDevice(const unsigned int, const unsigned int);
        int pmbusI2CDevice();
        int addI2CQuantity(I2CQuantity*, unsigned int&);
        I2CQuantity* getI2CQuantity(const unsigned int q) const { return(m_quantities[q]); }
        unsigned int numI2CQuantities() const                   { return(m_quantities.size()); }
        void dump(const unsigned int) const;
      private:
        // private members
        std::string                     m_type;
        std::string                     m_name;
        unsigned int                    m_address;
        unsigned int                    m_switch_address;
        unsigned int                    m_switch_select;
        bool                            m_page_flag;
        unsigned int                    m_page_offset;
        unsigned int                    m_page_select;
        bool                            m_pmbus;
        std::vector<I2CQuantity*>       m_quantities;
    };

    // private nested class
    class I2CBus {
      public:
        // public methods
        I2CBus(const std::string&, I2CInterface*);
       ~I2CBus();
        std::string getName() const                             { return(m_name); }
        I2CInterface* getI2C() const                            { return(m_i2c); }
        int addI2CDevice(I2CDevice*, unsigned int&);
        I2CDevice* getI2CDevice(const unsigned int d) const     { return(m_devices[d]); }
        unsigned int numI2CDevices() const                      { return(m_devices.size()); }
        void dump(const unsigned int) const;
        void clear();
      private:
        // private members
        std::string                     m_name;
        I2CInterface*                   m_i2c;
        std::vector<I2CDevice*>         m_devices;
    };

    // private members
    std::vector<I2CBus*>                m_busses;

    // private methods
    int localRead(const unsigned int, const unsigned int, const unsigned int, unsigned int&, uint8_t[]);
    int localRead(const unsigned int, const unsigned int, const unsigned int, const unsigned int, unsigned int&, uint8_t[]);
    int localWrite(const unsigned int, const unsigned int, const unsigned int, unsigned int&, uint8_t[]);
    int localWrite(const unsigned int, const unsigned int, const unsigned int, const unsigned int, unsigned int&, uint8_t[]);


    static uint16_t getNextTwoByteWord(const std::vector<unsigned int>&, unsigned int&);

  public:

    int writeRegister(const unsigned int bus, const unsigned int dev, const unsigned int offs, I2CInterface::TRANSFER_TYPE ttyp, unsigned int& num, uint8_t data[]);
    int readRegister(const unsigned int bus, const unsigned int dev, const unsigned int offs, I2CInterface::TRANSFER_TYPE ttyp, unsigned int& num, uint8_t data[]);
};      // class I2CNetwork

}       // namespace LVL1

#endif  // _I2CNETWORK_H_
