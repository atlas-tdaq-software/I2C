#ifndef _LVL1_I2CDEVICE_H_
#define _LVL1_I2CDEVICE_H_

//******************************************************************************
// file: I2CDevice.h
// desc: I2C Interface based on i2c-tools (/dev/i2c-0)
// auth: 30-AUG-2016 R. Spiwoks
// auth: 16-FEB-2017 R. Spiwoks, imported from zynq_i2c
//******************************************************************************

#include "I2C/I2CInterface.h"

#include <stdint.h>
#include <unistd.h>

namespace LVL1 {

// I2CDevice controller class
class I2CDevice : public LVL1::I2CInterface {

  public:

    // public type definition
    typedef enum { NO_PEC, PEC_DEVICE, PEC_SOFT } PEC_TYPE;

    // constructor and desctructor
    I2CDevice(const unsigned int, const bool = false, const PEC_TYPE = NO_PEC, const unsigned int = 0);
    virtual ~I2CDevice();
    virtual bool remote() const                 { return(false); }

    // set/get functions
    unsigned int bus() const                    { return(m_bus); }
    bool getForce()                             { return(m_force); }
    PEC_TYPE getPmbus()                         { return(m_pmbus); }

    // high-level functions
    int Read(const unsigned int, uint8_t&);
    int Read(const unsigned int, const unsigned int, uint8_t&);
    int Read(const unsigned int, const unsigned int, uint16_t&);
    int ReadIBlock(const unsigned int, const unsigned int, unsigned int&, uint8_t[]);
    int ReadSBlock(const unsigned int, const unsigned int, unsigned int&, uint8_t[]);
    int ReadPBlock(const unsigned int, const unsigned int, unsigned int&, uint8_t[]);
    int ReadProcessCall(const unsigned int, const unsigned int, uint16_t&);
    int ReadBlockProcessCall(const unsigned int, const unsigned int, unsigned int&, uint8_t[]);
    int ReadSystemMonitor(const unsigned int, const unsigned int, uint16_t&);
    int Write(const unsigned int, const uint8_t);
    int Write(const unsigned int, const unsigned int, const uint8_t);
    int Write(const unsigned int, const unsigned int, const uint16_t);
    int WriteIBlock(const unsigned int, const unsigned int, const unsigned int, const uint8_t[]);
    int WriteSBlock(const unsigned int, const unsigned int, unsigned int&, const uint8_t[]);
    int WritePBlock(const unsigned int, const unsigned int, const unsigned int, const uint8_t[]);
    int WriteSystemMonitor(const unsigned int, const unsigned int, const uint16_t);
    int PrintFunctions();

    void lockBus();
    void unlockBus();

    // local I2C interface
    virtual int DataRead(const unsigned int, const unsigned int, unsigned int&, uint8_t[], const TRANSFER_TYPE);
    virtual int DataWrite(const unsigned int, const unsigned int, unsigned int&, const uint8_t[], const TRANSFER_TYPE);

  private:

    // private member
    unsigned int        m_bus;
    int                 m_file;
    bool                m_force;
    PEC_TYPE            m_pmbus;
    int                 m_status;

    // private methods
    static void printData(const uint8_t[], const unsigned int size);

    // private constants
    static const unsigned int   DATA_MASK       = 0x000000ff;
    static const unsigned int   DATA_SIZE       = DATA_MASK + 1;
    static const unsigned int   ADDR_LL_SHIFT   =          1;
    static const unsigned int   ADDR_LL_READ    = 0x00000001;
    static const unsigned int   PEC_TYPE_NUMBER = PEC_SOFT + 1;
    static const std::string    PEC_TYPE_NAME[PEC_TYPE_NUMBER];

};      // class I2CDevice

}       // namespace LVL1

#endif  // _LVL1_I2CDEVICE_H_
