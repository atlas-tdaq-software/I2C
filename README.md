# I2C

A C++ library for using I2C.

The I2C module datasheets are here: https://gitlab.cern.ch/atlas-tdaq-software/internal/i2c-doc
